import $ from 'jquery';
import Vue from 'vue';

var app = new Vue({
    el: "#app",
    delimiters: ['@{', '}'],
    data: {
        new_value: '',
        items: window.items || []
    },
    methods: {
        addItem: function () {
            this.items.push({'nazwa':this.new_value});
            $.ajax({
                url: "/specjalista/specjalizacja/new",
                dataType: 'json',
                type: 'POST',
                data: { 'nazwa':this.new_value},
                success: function (result) {
                    console.log(result);
                }
            });
        },
        removeItem: function (key) {
            $.ajax({
                url: "/specjalista/specjalizacja/remove",
                dataType: 'json',
                type: 'POST',
                data: {'nazwa' : this.items[key]['nazwa'] },
                success: function (result) {
                    console.log(result);
                }
            });
            this.items.splice(key,1);
        }
    }
});