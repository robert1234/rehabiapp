import $ from 'jquery';
import Vue from 'vue'
import moment from 'moment';

var app = new Vue({
    el: "#app",
    delimiters: ['@{', '}'],
    data: {
        searchByValue: '',
        items: [],
        active: false
    },
    methods: {
        getItems: function () {
            var self = this;
            $.ajax({
                url: "/osoba/specjalista/search",
                dataType: 'json',
                type: 'POST',
                data: {'searchByValue' : this.searchByValue, 'active' : this.active },
                success: function (result) {
                    self.items.splice(0,self.items.length);
                    for (var i = 0; i < result.length; i++){
                        self.items.push(result[i]);
                    }
                    console.log(result);
                }
            });
        },
        edit: function (id) {
            var url = '/osoba/specjalista/edit/'+id;
            location.href = url;
        },
        changeActive: function () {
            this.active = !this.active;
            console.log(this.active);
            if(this.active) {
                $('#active-patients-button').removeClass('button-active').addClass('button-press');
            } else {
                $('#active-patients-button').addClass('button-active').removeClass('button-press');
            }
            this.getItems();
        },
        format_date: function (value){
            if (value) {
                return moment(String(value)).format('YYYY-MM-DD');
            }
        },
    },
    mounted: function () {
        this.getItems();
    }
});
