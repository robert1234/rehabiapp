import $ from 'jquery';
import Vue from 'vue';
import moment from 'moment';

var app = new Vue({
    el: "#app",
    delimiters: ['@{', '}'],
    data: {
        item: window.item || {
            pacjent: '',
            opis: '',
            plan: ''
        },
        id: window.id || '',
        pacjenci: window.pacjenci || [],
        plany: [],
    },
    methods: {
        storeItem: function () {
            var self = this;
            $.ajax({
                url: '/turnus/new',
                type: 'POST',
                data: { 'turnus' : self.item},
                success: function (result) {
                    location.href = '/turnus';
                }
            });
        },
        getPlany: function () {
            var self = this;
            $.ajax({
                url: "/plan/search/pacjent",
                dataType: 'json',
                type: 'POST',
                data: {'pacjent_id' : self.item.pacjent },
                success: function (result) {
                    self.plany.splice(0,self.plany.length);
                    for (var i = 0; i < result.length; i++){
                        self.plany.push(result[i]);
                    }
                    console.log(result);
                }
            });
        },
        // updateItem: function () {
        //     var self = this;
        //     $.ajax({
        //         url: '/osoba/pacjent/edit/'+self.id,
        //         type: 'POST',
        //         data: { 'pacjent' : self.item},
        //         success: function (result) {
        //             location.href = '/osoba/pacjent';
        //         }
        //     })
        // },
        applyChange: function () {
            this.checkFields();

            this.storeItem();
            // if(this.id === ''){
            //     this.storeItem();
            // }else{
            //     this.updateItem();
            // }
        },
        checkFields: function () { },
        format_date: function (value){
            if (value) {
                return moment(String(value)).format('YYYY-MM-DD');
            }
        },
        sortedListOfPlany: function () {
            return this.plany.sort();
        },
    },
    computed: {
        sortedListOfPacjenci: function () {
            return this.pacjenci.sort();
        },
    },
    mounted: function () {
        this.getPlany();
    }
});
