import $ from 'jquery';

$(window).ready(function () {
    $('.hidden').hide();
    $('.hidden-box-full').hide();

    $('.qr-generate').click(function () {
        var code = $(this).attr('data-code');
        var imgHtml = $('#hidden-'+code).html();
        console.log(code);
        console.log(imgHtml);
        $('.hidden-box-full').fadeIn(400);
        $('.qr-container').html(imgHtml);
    });

    $('.hidden-box-full').click(function () {
        $('.hidden-box-full').fadeOut(400);
    });
});