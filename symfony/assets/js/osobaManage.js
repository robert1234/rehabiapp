import $ from 'jquery';
import Vue from 'vue';
import moment from 'moment';

// TODO: Zainstalować FOSJsRoutingBundle

var app = new Vue({
    el: "#app",
    delimiters: ['@{', '}'],
    data: {
        osoba: window.osoba || {
            imie: '',
            nazwisko: '',
            pesel: '',
            dataUrodzenia: '',
            plec: '',
            imieOjca: '',
            imieMatki: '',
            ulica: '',
            nrDomu: '',
            kodPocztowy: '',
            miejscowosc: '',
            dataRejestracji: '',
        },
        id: window.id || ''
    },
    methods: {
        storeOsoba: function () {
            var self = this;
            $.ajax({
                url: '/osoba/new',
                type: 'POST',
                data: { 'osoba' : self.osoba},
                success: function (result) {
                    location.href = '/osoba';
                }
            })
        },
        updateOsoba: function () {
            var self = this;
            $.ajax({
                url: '/osoba/'+self.id+"/edit",
                type: 'POST',
                data: { 'osoba' : self.osoba},
                success: function (result) {
                    location.href = '/osoba';
                }
            })
        },
        applyChange: function () {
            this.checkFields();

            if(this.id === ''){
                this.storeOsoba();
            }else{
                this.updateOsoba();
            }
        },
        checkFields: function () {
            if(isNaN(Date.parse(this.osoba.dataUrodzenia.date))){
                alert("Podaj datę w poprawnym formacie!");
                return 0;
            }
        },
        format_date: function (value){
            if (value) {
                return moment(String(value)).format('YYYY-MM-DD hh:mm:ss');
            }
        },
    },
    mounted: function () {

    }
});
