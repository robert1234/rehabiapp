import $ from 'jquery';
require('jquery-ui-bundle');
import Vue from 'vue';
import moment from 'moment';

var app = new Vue({
    el: "#app",
    delimiters: ['@{', '}'],
    data: {
        turnus: window.turnus || [],
        id: window.id || '',
        etapy: window.etapy || [],
        sale: window.sale || [],
        specjalisci: window.specjalisci || [],
        mode: '1-day', //To use in future
        day: new Date(),
        timetable: [],
        etap: {
            id: '',
            sala: '',
            specjalista: '',
            zabieg: '',
            data: '',
            turnus: window.turnus.id || '',
        },
        dayHoursMap: window.dayHourMap || [],
    },
    methods: {
        storeItem: function () {
            var self = this;
            $.ajax({
                url: '/etap/modify',
                type: 'POST',
                data: { 'etap' : self.etap },
                success: function (result) {
                    app.getTimetable();
                    if(result['result'] === "collisions_exists") {
                        alert(result['collisions']);
                    }
                }
            })
        },
        getTimetable: function() {
            var self = this;
            if (this.mode === '1-day') {
                $.ajax({
                    url: '/timetable/day',
                    type: 'POST',
                    data: {
                        'sala': self.etap.sala,
                        'specjalista': self.etap.specjalista,
                        'data': self.getSelectedDay()
                    },
                    success: function (result) {
                        self.cleanDayHoursMap();

                        for (var i = 0; i < result.length; i++) {
                            let startDate = self.to_date(result[i]['dataPrzeprowadzenia']['date']);
                            let czasTrwania = result[i]['czasTrwania'];
                            let finalDate = self.to_date(result[i]['dataPrzeprowadzenia']['date']);
                            finalDate.add(czasTrwania, 'minutes');
                            for (var dateIterator = startDate;
                                 dateIterator < finalDate;
                                 dateIterator.add(10, "minutes")) {
                                let hour = dateIterator.hour();
                                let minute = dateIterator.minute();
                                if (minute === 0) minute = '00';
                                var index = hour + ':' + minute;
                                var newArray = { 'etap' : result[i]['zabieg'], hour: index };
                                app.$set(app.dayHoursMap, index, newArray);
                            }
                        }
                        app.$set(app,'timetable',result);
                    },
                });
            }
        },
        format_date: function (value){
            if (value) {
                return moment(String(value)).format('YYYY-MM-DD');
            }
        },
        to_date: function (value){
            if (value) {
                return moment(String(value));
            }
        },
        getSelectedDay: function()
        {
            var str = this.day.getFullYear()+'-'+(this.day.getMonth()+1)+'-'+this.day.getDate();
            return this.format_date(str);
        },
        dateAdd: function () {
            if (this.mode === '1-day')
            {
                this.day = new Date(this.day.setDate(this.day.getDate() + 1));
                console.log("Select next day");
                console.log(this.getSelectedDay());
                this.getTimetable();
            }
        },
        dateSub: function () {
            if (this.mode === '1-day')
            {
                this.day = new Date(this.day.setDate(this.day.getDate() - 1));
                console.log("Select previous day");
                console.log(this.getSelectedDay());
                this.getTimetable();
            }
        },
        cleanDayHoursMap: function () {
            for (var i = 8; i < 20; i++) {
                for (var j = 0; j < 60; j = j + 10) {
                    this.dayHoursMap[i+':'+((j === 0) ? '00' : j)]['etap'] = '';
                }
            }
        },
        changeEtapId: function(id, zabieg) {
            this.etap.id = id;
            this.etap.zabieg = zabieg;
        },
    },
    computed: {
        sortedListOfSpec: function () {
            return this.specjalisci.sort();
        },
        sortedListOfSale: function () {
            return this.sale.sort();
        },
    },
    mounted: function () {
        $('.item-draggable').draggable({
            revert: true,
            start: function() {
                $(this).addClass('pointer').removeClass('item-draggable');
            },
            stop: function() {
                $(this).removeClass('pointer').addClass('item-draggable');
            }
        });
        $('.calendar-cart').droppable({
            classes: {
                "ui-droppable-active": "calendar-cart-active",
                "ui-droppable-hover": "calendar-cart-hover"
            },
            drop: function( event, ui ) {
                let time = $( this ).attr('id');
                let fullTime = app.getSelectedDay()+' '+time+':00';
                app.$set(app.etap, 'data', fullTime);
                app.storeItem();
            }
        });
    }
});
