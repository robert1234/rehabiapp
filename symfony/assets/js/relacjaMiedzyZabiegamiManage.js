import $ from 'jquery';
import Vue from 'vue';

var app = new Vue({
    el: "#app",
    delimiters: ['@{', '}'],
    data: {
        item: window.item || {
            zabiegX: '',
            zabiegY: '',
            minimalnyOdstep: '0',
        },
        zabiegi: window.zabiegi || [],
    },
    methods: {
        storeItem: function () {
            var self = this;
            $.ajax({
                url: '/zabieg/relacje/new',
                type: 'POST',
                data: { 'relacja' : self.item},
                success: function (result) {
                    location.href = '/zabieg/relacje';
                }
            })
        },
        applyChange: function () {
            if(this.checkFields() === 1) return 1;
            this.storeItem();
        },
        checkFields: function () {
            if( !Number.isInteger(parseInt(this.item.minimalnyOdstep)) )
            {
                alert("Minimalny odstęp musi być liczbą!");
                console.log(this.item.minimalnyOdstep);
                return 1;
            }

            return 0;
        }
    },
    computed: {
        sortedListOfZabiegi: function () {
            return this.zabiegi.sort();
        }
    },
    mounted: function () { }
});
