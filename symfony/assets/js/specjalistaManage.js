import $ from 'jquery';
import Vue from 'vue';
import moment from 'moment';

var app = new Vue({
    el: "#app",
    delimiters: ['@{', '}'],
    data: {
        item: window.item || {
            osoba: '',
            specjalizacja: '',
            oddzial: '',
            opisDodatkowy: '',
            dataRozpoczecia: null,
            dataZakonczenia: null
        },
        id: window.id || '',
        osoby: window.osoby || [],
        specjalizacje: window.specjalizacje || [],
        oddzialy: window.oddzialy || [],
    },
    methods: {
        storeItem: function () {
            var self = this;
            $.ajax({
                url: '/osoba/specjalista/new',
                type: 'POST',
                data: { 'specjalista' : self.item},
                success: function (result) {
                    location.href = '/osoba/specjalista';
                }
            })
        },
        updateItem: function () {
            var self = this;
            $.ajax({
                url: '/osoba/specjalista/edit/'+self.id,
                type: 'POST',
                data: { 'specjalista' : self.item},
                success: function (result) {
                    location.href = '/osoba/specjalista';
                }
            })
        },
        applyChange: function () {
            this.checkFields();

            if(this.id === ''){
                this.storeItem();
            }else{
                this.updateItem();
            }
        },
        checkFields: function () { },
        format_date: function (value){
            if (value) {
                return moment(String(value)).format('YYYY-MM-DD hh:mm:ss');
            }
        },
    },
    computed: {
        sortedListOfOsoby: function () {
            return this.osoby.sort();
        },
        sortedListOfSpec: function () {
            return this.specjalizacje.sort();
        },
        sortedListOfOddzialy: function () {
            return this.oddzialy.sort();
        }
    },
    mounted: function () { }
});
