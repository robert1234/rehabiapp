import $ from 'jquery';
import Vue from 'vue'
import moment from 'moment';

var app = new Vue({
    el: "#app",
    delimiters: ['@{', '}'],
    data: {
        searchByValue: '',
        items: [],
        active: false
    },
    methods: {
        getItems: function () {
            var self = this;
            $.ajax({
                url: "/plan/search",
                dataType: 'json',
                type: 'POST',
                data: {'searchByValue' : this.searchByValue },
                success: function (result) {
                    self.items.splice(0,self.items.length);
                    for (var i = 0; i < result.length; i++){
                        self.items.push(result[i]);
                    }
                    console.log(result);
                }
            });
        },
        edit: function (id) {
            var url = '/plan/edit/'+id;
            location.href = url;
        },
    },
    mounted: function () {
        this.getItems();
    }
});
