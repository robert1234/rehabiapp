import $ from 'jquery';
import Vue from 'vue';

var app = new Vue({
    el: "#app",
    delimiters: ['@{', '}'],
    data: {
        new_value: '',
        new_description: '',
        items: window.items || []
    },
    methods: {
        addItem: function () {
            this.items.push({'kod':this.new_value,'opis':this.new_description});
            $.ajax({
                url: "/rozpoznanie/new",
                dataType: 'json',
                type: 'POST',
                data: { 'kod':this.new_value, 'opis':this.new_description },
                success: function (result) {
                    console.log(result);
                }
            });
        },
        removeItem: function (key) {
            $.ajax({
                url: "/rozpoznanie/remove",
                dataType: 'json',
                type: 'POST',
                data: {'kod' : this.items[key]['kod'] },
                success: function (result) {
                    console.log(result);
                }
            });
            this.items.splice(key,1);
        }
    }
});