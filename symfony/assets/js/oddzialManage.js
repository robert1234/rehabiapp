import $ from 'jquery';
import Vue from 'vue';

var app = new Vue({
    el: "#app",
    delimiters: ['@{', '}'],
    data: {
        new_oddzial_nazwa: '',
        oddzialy: window.oddzialy || []
    },
    methods: {
        addOddzial: function () {
            this.oddzialy.push({'nazwa' : this.new_oddzial_nazwa});
            $.ajax({
                url: "/oddzial/new",
                dataType: 'json',
                type: 'POST',
                data: {'nazwa' : this.new_oddzial_nazwa },
                success: function (result) {
                    console.log(result);
                }
            });
        },
        removeOddzial: function (key) {
            $.ajax({
                url: "/oddzial/remove",
                dataType: 'json',
                type: 'POST',
                data: {'nazwa' : this.oddzialy[key]['nazwa'] },
                success: function (result) {
                    console.log(result);
                }
            });
            this.oddzialy.splice(key,1);
        },
        editSale: function (id) {
            const url = "/oddzial/"+id+"/sale";
            window.location.href = url;
        }
    }
});