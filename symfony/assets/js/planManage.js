import $ from 'jquery';
import Vue from 'vue';
import moment from 'moment';

var app = new Vue({
    el: "#app",
    delimiters: ['@{', '}'],
    data: {
        item: window.item || {
            rozpoznanie: '',
            opis: '',
        },
        id: window.id || '',
        rozpoznania: window.rozpoznania || [],
        zabiegi: window.zabiegi || [],
        pozycje: window.pozycje || []
    },
    methods: {
        storeItem: function () {
            var self = this;
            $.ajax({
                url: '/plan/new',
                type: 'POST',
                data: { 'plan' : self.item, 'pozycje' : self.pozycje },
                success: function (result) {
                    location.href = '/plan';
                }
            })
        },
        updateItem: function () {
            var self = this;
            $.ajax({
                url: '/plan/edit/'+self.id,
                type: 'POST',
                data: { 'plan' : self.item , 'pozycje' : self.pozycje},
                success: function (result) {
                    location.href = '/plan';
                }
            })
        },
        applyChange: function () {
            this.checkFields();

            if(this.id === ''){
                this.storeItem();
            }else{
                this.updateItem();
            }
        },
        checkFields: function () { },
        format_date: function (value){
            if (value) {
                return moment(String(value)).format('YYYY-MM-DD');
            }
        },
        isZabiegRelated: function (zabiegId) {
            const result = this.pozycje.findIndex(
                (elem) => {
                    return elem === zabiegId
                }
            );
            return result;
        },
        addZabiegToPlan: function (zabiegId) {
            this.pozycje.push(zabiegId);
        },
        removeZabiegFromPlan: function (zabiegId) {
            const zabiegIdIndex = this.isZabiegRelated(zabiegId);
            this.pozycje.splice(zabiegIdIndex,1);
        },
    },
    computed: {
        sortedListOfRozpoznania: function () {
            return this.rozpoznania.sort();
        },
    },
    mounted: function () { }
});
