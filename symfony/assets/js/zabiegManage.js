import $ from 'jquery';
import Vue from 'vue';

var app = new Vue({
    el: "#app",
    delimiters: ['@{', '}'],
    data: {
        item: window.item || {
            rodzajZabiegu: '',
            cena: '',
            czasTrwania: '',
            zalecenia: '',
            opis: ''
        },
        id: window.id || '',
        rodzajeZabiegow: window.rodzajeZabiegow || [],
    },
    methods: {
        storeItem: function () {
            var self = this;
            $.ajax({
                url: '/zabieg/new',
                type: 'POST',
                data: { 'zabieg' : self.item},
                success: function (result) {
                    location.href = '/zabieg';
                }
            })
        },
        updateItem: function () {
            var self = this;
            $.ajax({
                url: '/zabieg/edit/'+self.id,
                type: 'POST',
                data: { 'zabieg' : self.item},
                success: function (result) {
                    location.href = '/zabieg';
                }
            })
        },
        applyChange: function () {
            this.checkFields();

            if(this.id === ''){
                this.storeItem();
            }else{
                this.updateItem();
            }
        },
        checkFields: function () { }
    },
    computed: {
        sortedListOfRodzajeZabiegow: function () {
            return this.rodzajeZabiegow.sort();
        }
    },
    mounted: function () { }
});
