import $ from 'jquery';
import Vue from 'vue';

var app = new Vue({
    el: "#app",
    delimiters: ['@{', '}'],
    data: {
        searchByValue: '',
        zabiegi: []
    },
    methods: {
        getZabiegi: function () {
            var self = this;
            $.ajax({
                url: "/zabieg/search",
                dataType: 'json',
                type: 'POST',
                data: {'searchByValue' : this.searchByValue },
                success: function (result) {
                    self.zabiegi.splice(0,self.zabiegi.length);
                    for (var i = 0; i < result.length; i++){
                        self.zabiegi.push(result[i]);
                    }
                    console.log(result);
                }
            });
        },
        edit: function (id) {
            var url = '/zabieg/edit/'+id;
            location.href = url;
        }
    },
    mounted: function () {
        this.getZabiegi();
    }
});
