import $ from 'jquery';
import Vue from 'vue';

var app = new Vue({
    el: "#app",
    delimiters: ['@{', '}'],
    data: {
        uzytkownik_systemu: window.uzytkownik_systemu || {
            login: window.proponed_login || '',
            haslo: '',
            aktywny: true,
            osoba: window.user_id || '',
        },
        id: window.id || '',
        grupy: window.grupy || [],
        osoby: window.osoby || [],
        czlonkowie_grup: window.czlonkowie_grup || []
    },
    methods: {
        storeUzytkownikSystemu: function () {
            var self = this;
            $.ajax({
                url: '/uzytkownik-systemu/new',
                type: 'POST',
                data: { 'uzytkownik_systemu' : self.uzytkownik_systemu, 'czlonkowie_grup' : self.czlonkowie_grup},
                success: function (result) {
                    location.href = '/osoba';
                }
            })
        },
        updateUzytkownikSystemu: function () {
            var self = this;
            $.ajax({
                url: '/uzytkownik_systemu/'+self.id+"/edit",
                type: 'POST',
                data: { 'uzytkownik_systemu' : self.uzytkownik_systemu, 'czlonkowie_grup' : self.czlonkowie_grup},
                success: function (result) {
                    location.href = '/osoba';
                }
            })
        },
        applyChange: function () {
            if(this.checkFields()===1) return 1;

            if(this.id === ''){
                this.storeUzytkownikSystemu();
            }else{
                this.updateUzytkownikSystemu();
            }
        },
        checkFields: function () {
            if(this.czlonkowie_grup.length === 0){
                alert("Użytkownik musi być członkiem przynajmniej jednej grupy!!");
                return 1;
            }
            return 0;
        },
        changeAktywnyValue: function () {
            this.uzytkownik_systemu.aktywny = !this.uzytkownik_systemu.aktywny;
            if(this.uzytkownik_systemu.aktywny){
                $("#aktywny").removeClass('toggle-button-red').addClass('toggle-button-green');
                $("#aktywny").html('Aktywny');
            }else{
                $("#aktywny").removeClass('toggle-button-green').addClass('toggle-button-red');
                $("#aktywny").html('Nieaktywny');
            }
        },
        isInGrupyUzytkownikow: function (grupaId) {
            const result = this.czlonkowie_grup.findIndex(
                (elem) => {
                    return elem === grupaId
                }
            );
            return result;
        },
        addGrupaUzytkownika: function (grupa) {
            this.czlonkowie_grup.push(grupa);
        },
        removeGrupaUzytkownika: function (grupa) {
            const grupaIndex = this.isInGrupyUzytkownikow(grupa);
            this.czlonkowie_grup.splice(grupaIndex,1);
        },
    },
    mounted: function () {

    },
});
