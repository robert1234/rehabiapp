import $ from 'jquery';
import Vue from 'vue';

var app = new Vue({
    el: "#app",
    delimiters: ['@{', '}'],
    data: {
        searchByValue: '',
        items: []
    },
    methods: {
        getItems: function () {
            var self = this;
            $.ajax({
                url: "/turnus/search",
                dataType: 'json',
                type: 'POST',
                data: {'searchByValue' : this.searchByValue },
                success: function (result) {
                    self.items.splice(0,self.items.length);
                    for (var i = 0; i < result.length; i++){
                        self.items.push(result[i]);
                    }
                    console.log(result);
                }
            });
        },
        edit: function (id) {
            const url = '/turnus/manage/'+id;
            location.href = url;
        }
    },
    mounted: function () {
        this.getItems();
    }
});