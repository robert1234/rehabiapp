import $ from 'jquery';
import Vue from 'vue';

var app = new Vue({
    el: "#app",
    delimiters: ['@{', '}'],
    data: {
        searchByValueX: '',
        searchByValueY: '',
        relacje: []
    },
    methods: {
        getRelacje: function () {
            var self = this;
            $.ajax({
                url: "/zabieg/relacje/search",
                dataType: 'json',
                type: 'POST',
                data: {'searchByValueX' : self.searchByValueX, 'searchByValueY' : self.searchByValueY },
                success: function (result) {
                    self.relacje.splice(0,self.relacje.length);
                    for (var i = 0; i < result.length; i++){
                        self.relacje.push(result[i]);
                    }
                    console.log(result);
                }
            });
        },
        remove: function (id, key) {
            var self = this;
            $.ajax({
                url: "/zabieg/relacje/remove",
                dataType: 'json',
                type: 'POST',
                data: {'zabiegId' : id },
                success: function (result) {
                    self.relacje.splice(key,1);
                    console.log(result);
                }
            });
        }
    },
    mounted: function () {
        this.getRelacje();
    }
});
