import $ from 'jquery';
import Vue from 'vue';

var app = new Vue({
    el: "#app",
    delimiters: ['@{', '}'],
    data: {
        new_sala_nazwa: '',
        sale: window.sale || []
    },
    methods: {
        addSala: function () {
            this.sale.push({'nrSali' : this.new_sala_nazwa,'oddzial' : window.oddzial_id });
            $.ajax({
                url: "/oddzial/edit/sala/store",
                dataType: 'json',
                type: 'POST',
                data: {'nrSali' : this.new_sala_nazwa,'oddzial' : window.oddzial_id },
                success: function (result) {
                    console.log(result);
                }
            });
        },
        removeSala: function (key) {
            $.ajax({
                url: "/oddzial/edit/sala/remove",
                dataType: 'json',
                type: 'POST',
                data: { 'nrSali' : this.sale[key]['nrSali'],'oddzial' : window.oddzial_id },
                success: function (result) {
                    console.log(result);
                }
            });
            this.sale.splice(key,1);
        }
    }
});