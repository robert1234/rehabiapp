import $ from 'jquery';
import Vue from 'vue';
import moment from 'moment';

var app = new Vue({
    el: "#app",
    delimiters: ['@{', '}'],
    data: {
        item: window.item || {
            osoba: '',
            rozpoznanie: '',
            opisDodatkowy: '',
            platnik: '',
            dataRozpoczecia: null,
            dataZakonczenia: null
        },
        id: window.id || '',
        osoby: window.osoby || [],
        rozpoznania: window.rozpoznania || [],
    },
    methods: {
        storeItem: function () {
            var self = this;
            $.ajax({
                url: '/osoba/pacjent/new',
                type: 'POST',
                data: { 'pacjent' : self.item},
                success: function (result) {
                    location.href = '/osoba/pacjent';
                }
            })
        },
        updateItem: function () {
            var self = this;
            $.ajax({
                url: '/osoba/pacjent/edit/'+self.id,
                type: 'POST',
                data: { 'pacjent' : self.item},
                success: function (result) {
                    location.href = '/osoba/pacjent';
                }
            })
        },
        applyChange: function () {
            this.checkFields();

            if(this.id === ''){
                this.storeItem();
            }else{
                this.updateItem();
            }
        },
        checkFields: function () { },
        format_date: function (value){
            if (value) {
                return moment(String(value)).format('YYYY-MM-DD hh:mm:ss');
            }
        },
    },
    computed: {
        sortedListOfOsoby: function () {
            return this.osoby.sort();
        },
        sortedListOfRozpoznania: function () {
            return this.rozpoznania.sort();
        },
    },
    mounted: function () { }
});
