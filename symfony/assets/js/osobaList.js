import $ from 'jquery';
import Vue from 'vue';

var app = new Vue({
    el: "#app",
    delimiters: ['@{', '}'],
    data: {
        searchByValue: '',
        osoby: []
    },
    methods: {
        getOsoby: function () {
            var self = this;
            $.ajax({
                url: "/osoba/search",
                dataType: 'json',
                type: 'POST',
                data: {'searchByValue' : this.searchByValue },
                success: function (result) {
                    self.osoby.splice(0,self.osoby.length);
                    for (var i = 0; i < result.length; i++){
                        self.osoby.push(result[i]);
                    }
                    console.log(result);
                }
            });
        },
        edit: function (id) {
            var url = '/osoba/'+id+'/edit';
            location.href = url;
        }
    },
    mounted: function () {
        this.getOsoby();
    }
});
