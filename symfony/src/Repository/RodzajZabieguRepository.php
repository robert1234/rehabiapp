<?php

namespace App\Repository;

use App\Entity\RodzajZabiegu;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method RodzajZabiegu|null find($id, $lockMode = null, $lockVersion = null)
 * @method RodzajZabiegu|null findOneBy(array $criteria, array $orderBy = null)
 * @method RodzajZabiegu[]    findAll()
 * @method RodzajZabiegu[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RodzajZabieguRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RodzajZabiegu::class);
    }

    public function addRodzajZabiegu(RodzajZabiegu $rodzajZabiegu)
    {
        $this->_em->persist($rodzajZabiegu);
        $this->_em->flush();
    }

    public function removeRodzajZabiegu($nazwa)
    {
        $this->_em->getRepository(RodzajZabiegu::class)
            ->createQueryBuilder('rz')
            ->delete()
            ->where('rz.nazwa = :nazwa')
            ->setParameter('nazwa', $nazwa)
            ->getQuery()->execute();
    }

    public function getRodzajZabieguById($id)
    {
        return $this->_em->getRepository(RodzajZabiegu::class)
            ->createQueryBuilder('rz')
            ->select()
            ->where('rz.id = :id')
            ->setParameter('id', $id)
            ->getQuery()->getResult();
    }

    public function getAll()
    {
        return $this->_em->getRepository(RodzajZabiegu::class)
            ->createQueryBuilder('rz')
            ->select()
            ->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }
}
