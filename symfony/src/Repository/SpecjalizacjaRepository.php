<?php

namespace App\Repository;

use App\Entity\Specjalizacja;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Specjalizacja|null find($id, $lockMode = null, $lockVersion = null)
 * @method Specjalizacja|null findOneBy(array $criteria, array $orderBy = null)
 * @method Specjalizacja[]    findAll()
 * @method Specjalizacja[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SpecjalizacjaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Specjalizacja::class);
    }

    public function addSpecjalizacja(Specjalizacja $specjalizacja)
    {
        $this->_em->persist($specjalizacja);
        $this->_em->flush();
    }

    public function removeSpecjalizacja($nazwa)
    {
        $this->_em->getRepository(Specjalizacja::class)
            ->createQueryBuilder('s')
            ->delete()
            ->where('s.nazwa = :nazwa')
            ->setParameter('nazwa', $nazwa)
            ->getQuery()->execute();
    }

    public function getSpecjalizacjaById($id)
    {
        return $this->_em->getRepository(Specjalizacja::class)
            ->createQueryBuilder('s')
            ->select()
            ->where('s.id = :id')
            ->setParameter('id', $id)
            ->getQuery()->getResult();
    }

    public function getAll()
    {
        return $this->_em->getRepository(Specjalizacja::class)
            ->createQueryBuilder('s')
            ->select()
            ->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }
}
