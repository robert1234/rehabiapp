<?php

namespace App\Repository;

use App\Entity\Plan;
use App\Entity\PozycjaPlanu;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\AbstractQuery;

/**
 * @method PozycjaPlanu|null find($id, $lockMode = null, $lockVersion = null)
 * @method PozycjaPlanu|null findOneBy(array $criteria, array $orderBy = null)
 * @method PozycjaPlanu[]    findAll()
 * @method PozycjaPlanu[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PozycjaPlanuRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PozycjaPlanu::class);
    }

    public function addEntity(PozycjaPlanu $pozycjaPlanu)
    {
        $this->_em->persist($pozycjaPlanu);
        $this->_em->flush();
    }

    public function getEntityById($id)
    {
        return $this->_em->getRepository(PozycjaPlanu::class)
            ->createQueryBuilder('pp')
            ->select()
            ->where('pp.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
    }

    public function getArrayById($id)
    {
        return $this->_em->getRepository(PozycjaPlanu::class)
            ->createQueryBuilder('pp')
            ->select()
            ->where('pp.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult(AbstractQuery::HYDRATE_ARRAY);
    }

    public function getAll()
    {
        return $this->_em->getRepository(PozycjaPlanu::class)
            ->createQueryBuilder('pp')
            ->select()
            ->getQuery()
            ->getResult(AbstractQuery::HYDRATE_ARRAY);
    }

    public function findByPlan($planId)
    {
        return $this->_em->getRepository(PozycjaPlanu::class)
            ->createQueryBuilder('pp')
            ->select()
            ->where('pp.plan = :planId')
            ->setParameter('planId', $planId)
            ->getQuery()
            ->getResult();
    }

    public function removeById($id)
    {
        $this->_em->getRepository(PozycjaPlanu::class)
            ->createQueryBuilder('pp')
            ->delete()
            ->where('pp.id = :id')
            ->setParameter('id',$id)
            ->getQuery()->execute();
    }

    public function removeByPlanAndZabieg($planId, $zabiegId)
    {
        $this->_em->getRepository(PozycjaPlanu::class)
            ->createQueryBuilder('pp')
            ->delete()
            ->where('pp.plan = :planId')
            ->andWhere('pp.zabieg = :zabiegId')
            ->setParameter('planId',$planId)
            ->setParameter('zabiegId', $zabiegId)
            ->getQuery()->execute();
    }
}
