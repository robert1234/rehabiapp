<?php

namespace App\Repository;

use App\Entity\Pacjent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Pacjent|null find($id, $lockMode = null, $lockVersion = null)
 * @method Pacjent|null findOneBy(array $criteria, array $orderBy = null)
 * @method Pacjent[]    findAll()
 * @method Pacjent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PacjentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Pacjent::class);
    }

    public function add(Pacjent $pacjent)
    {
        $this->_em->persist($pacjent);
        $this->_em->flush();
    }

    public function update($id, $platnik, $rozpoznanie, $opisDodatkowy)
    {
        $this->_em->getRepository(Pacjent::class)
            ->createQueryBuilder('p')
            ->update()
            ->set('p.platnik',"'".$platnik."'")
            ->set('p.rozpoznanie',$rozpoznanie)
            ->set('p.opisDodatkowy',"'".$opisDodatkowy."'")
            ->where('p.id = :id')
            ->setParameter('id',$id)
            ->getQuery()->execute();
    }

    public function updateDataRozpoczecia($id, $startDate)
    {
        $this->_em->getRepository(Pacjent::class)
            ->createQueryBuilder('p')
            ->update()
            ->set('p.dataRozpoczecia',$startDate)
            ->where('p.id = :id')
            ->setParameter('id',$id)
            ->getQuery()->execute();
    }

    public function updateDataZakonczenia($id, $endDate)
    {
        $this->_em->getRepository(Pacjent::class)
            ->createQueryBuilder('p')
            ->update()
            ->set('p.dataZakonczenia',$endDate)
            ->where('p.id = :id')
            ->setParameter('id',$id)
            ->getQuery()->execute();
    }

    public function getEntityById($id)
    {
        return $this->_em->getRepository(Pacjent::class)
            ->createQueryBuilder('p')
            ->select()
            ->where('p.id = :id')
            ->setParameter('id',$id)
            ->getQuery()->getResult();
    }

    public function getArrayById($id)
    {
        return $this->_em->getRepository(Pacjent::class)
            ->createQueryBuilder('p')
            ->select("IDENTITY(p.osoba) AS osoba,IDENTITY(p.rozpoznanie) AS rozpoznanie,p.opisDodatkowy,p.platnik,p.dataRozpoczecia,p.dataZakonczenia")
            ->where('p.id = :id')
            ->setParameter('id',$id)
            ->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }

    public function getAll()
    {
        return $this->_em->getRepository(Pacjent::class)
            ->createQueryBuilder('p')
            ->select('p.id, p.dataRozpoczecia, p.dataZakonczenia, r.opis, o.imie, o.nazwisko, o.pesel')
            ->join('p.rozpoznanie','r')
            ->join('p.osoba', 'o')
            ->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }

    public function getCurrent()
    {
        return $this->_em->getRepository(Pacjent::class)
            ->createQueryBuilder('p')
            ->select('p.id, p.dataRozpoczecia, p.dataZakonczenia, r.opis, o.imie, o.nazwisko, o.pesel')
            ->join('p.rozpoznanie','r')
            ->join('p.osoba', 'o')
            ->where('p.dataZakonczenia IS NULL')
            ->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }

    public function getCurrentByUzytkownikSystemu($uzytkownikId)
    {
        return $this->_em->getRepository(Pacjent::class)
            ->createQueryBuilder('p')
            ->select('p.id, p.dataRozpoczecia, p.dataZakonczenia, r.opis, o.imie, o.nazwisko, o.pesel')
            ->join('p.rozpoznanie','r')
            ->join('p.osoba', 'o')
            ->join('o.uzytkownicySystemu','us')
            ->where('p.dataZakonczenia IS NULL')
            ->andWhere('us.id = :uzytkownikId')
            ->setParameter('uzytkownikId',$uzytkownikId)
            ->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }
}
