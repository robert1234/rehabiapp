<?php

namespace App\Repository;

use App\Entity\CzlonekGrupy;
use App\Entity\UzytkownikSystemu;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method UzytkownikSystemu|null find($id, $lockMode = null, $lockVersion = null)
 * @method UzytkownikSystemu|null findOneBy(array $criteria, array $orderBy = null)
 * @method UzytkownikSystemu[]    findAll()
 * @method UzytkownikSystemu[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UzytkownikSystemuRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UzytkownikSystemu::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof UzytkownikSystemu) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setHaslo($newEncodedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    public function findByLogin($login)
    {
        return $this->_em->getRepository(UzytkownikSystemu::class)->findOneBy(['login' => $login]);
    }

    public function findById($id)
    {
        return $this->_em->getRepository(UzytkownikSystemu::class)
            ->createQueryBuilder('us')
            ->select('us.login, us.aktywny, IDENTITY(us.osoba)')
            ->where('us.id = :usId')
            ->setParameter('usId',$id)
            ->getQuery()
            ->execute();
    }

    public function findByOsobaId($osobaId)
    {
        return $this->_em->getRepository(UzytkownikSystemu::class)
            ->createQueryBuilder('us')
            ->select('us.login, us.aktywny')
            ->where('us.osoba = :osobaId')
            ->setParameter('osobaId',$osobaId)
            ->getQuery()
            ->execute();
    }

    public function getEntityById($osobaId)
    {
        return $this->_em->getRepository(UzytkownikSystemu::class)
            ->createQueryBuilder('us')
            ->select()
            ->where('us.osoba = :osobaId')
            ->setParameter('osobaId',$osobaId)
            ->getQuery()
            ->getResult();
    }

    public function saveUzytkownikSystemu(UzytkownikSystemu $uzytkownikSystemu)
    {
        $this->_em->persist($uzytkownikSystemu);
        $this->_em->flush();
    }

    public function updateHaslo($uzytkownikSystemuId, $haslo)
    {
        $this->_em->getConnection()
            ->update('uzytkownik_systemu',
                ['haslo' => $haslo],
                ['id' => $uzytkownikSystemuId]);
    }

    public function updateAktywny($uzytkownikSystemuId, $aktywny)
    {
        $this->_em->getConnection()
            ->update('uzytkownik_systemu',
                ['aktywny' => $aktywny],
                ['id' => $uzytkownikSystemuId]);
    }

    public function addUzytkownikToGrupa(CzlonekGrupy $czlonekGrupy)
    {
        $this->_em->persist($czlonekGrupy);
        $this->_em->flush();
    }

    public function removeUzytkownikFromGrupa($uzytkownikId,$grupaId)
    {
        $this->_em->getRepository(CzlonekGrupy::class)
            ->createQueryBuilder('cg')
            ->delete()
            ->where('cg.uzytkownikSystemu = :uyztkownikId')
            ->andWhere('cg.grupa = :grupaId')
            ->setParameter('uzytkownikId',$uzytkownikId)
            ->setParameter('grupaId',$grupaId)
            ->getQuery()->execute();
    }
}
