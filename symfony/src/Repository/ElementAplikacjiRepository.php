<?php

namespace App\Repository;

use App\Entity\ElementAplikacji;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ElementAplikacji|null find($id, $lockMode = null, $lockVersion = null)
 * @method ElementAplikacji|null findOneBy(array $criteria, array $orderBy = null)
 * @method ElementAplikacji[]    findAll()
 * @method ElementAplikacji[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ElementAplikacjiRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ElementAplikacji::class);
    }

    // /**
    //  * @return ElementAplikacji[] Returns an array of ElementAplikacji objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ElementAplikacji
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
