<?php

namespace App\Repository;

use App\Entity\RelacjaMiedzyZabiegami;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method RelacjaMiedzyZabiegami|null find($id, $lockMode = null, $lockVersion = null)
 * @method RelacjaMiedzyZabiegami|null findOneBy(array $criteria, array $orderBy = null)
 * @method RelacjaMiedzyZabiegami[]    findAll()
 * @method RelacjaMiedzyZabiegami[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RelacjaMiedzyZabiegamiRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RelacjaMiedzyZabiegami::class);
    }

    public function addRelacjaMiedzyZabiegami(RelacjaMiedzyZabiegami $relacjaMiedzyZabiegami)
    {
        $this->_em->persist($relacjaMiedzyZabiegami);
        $this->_em->flush();
    }

    public function removeRelacjaMiedzyZabiegami($zabiegX, $zabiegY)
    {
        $this->_em->getRepository(RelacjaMiedzyZabiegami::class)
            ->createQueryBuilder('rmz')
            ->delete()
            ->where('rmz.zabiegX = :zabiegX')
            ->andWhere('rmz.zabiegY = :zabiegY')
            ->setParameter('zabiegX', $zabiegX)
            ->setParameter('zabiegY', $zabiegY)
            ->getQuery()->execute();
    }

    public function removeRelacjaMiedzyZabiegamiById($zabiegId)
    {
        $this->_em->getRepository(RelacjaMiedzyZabiegami::class)
            ->createQueryBuilder('rmz')
            ->delete()
            ->where('rmz.id = :zabiegId')
            ->setParameter('zabiegId', $zabiegId)
            ->getQuery()->execute();
    }

    public function getRelacjaMiedzyZabiegami($zabiegX, $zabiegY)
    {
        return $this->_em->getRepository(RelacjaMiedzyZabiegami::class)
            ->createQueryBuilder('rmz')
            ->select()
            ->where('rmz.zabiegX = :zabiegX')
            ->andWhere('rmz.zabiegY = :zabiegY')
            ->setParameter('zabiegX', $zabiegX)
            ->setParameter('zabiegY', $zabiegY)
            ->getQuery()->getResult();
    }

    public function getRelacjaMiedzyZabiegamiById($id)
    {
        return $this->_em->getRepository(RelacjaMiedzyZabiegami::class)
            ->createQueryBuilder('rmz')
            ->select()
            ->where('rmz.id = :id')
            ->setParameter('id', $id)
            ->getQuery()->getResult();
    }

    public function getAll()
    {
        return $this->_em->getRepository(RelacjaMiedzyZabiegami::class)
            ->createQueryBuilder('s')
            ->select()
            ->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }

    public function findByZabieg($valueOne, $valueTwo)
    {
        return $this->_em->getRepository(RelacjaMiedzyZabiegami::class)
            ->createQueryBuilder('rmz')
            ->select('rmz.id, zx.opis AS zabiegX, zy.opis AS zabiegY, rmz.minimalnyOdstep')
            ->join('rmz.zabiegX','zx')
            ->join('rmz.zabiegY','zy')
            ->where('zx.opis LIKE :opisX')
            ->orWhere('zx.opis LIKE :opisY')
            ->orWhere('zy.opis LIKE :opisX')
            ->orWhere('zy.opis LIKE :opisY')
            ->setParameter('opisX', "%".$valueOne."%")
            ->setParameter('opisY', "%".$valueTwo."%")
            ->getQuery()->getResult();
    }
}
