<?php

namespace App\Repository;

use App\Entity\Plan;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\AbstractQuery;

/**
 * @method Plan|null find($id, $lockMode = null, $lockVersion = null)
 * @method Plan|null findOneBy(array $criteria, array $orderBy = null)
 * @method Plan[]    findAll()
 * @method Plan[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlanRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Plan::class);
    }

    public function addEntity(Plan $plan)
    {
        $this->_em->persist($plan);
        $this->_em->flush();
        return $plan->getId();
    }

    public function getEntityById($id)
    {
        return $this->_em->getRepository(Plan::class)
            ->createQueryBuilder('p')
            ->select()
            ->where('p.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
    }

    public function getArrayById($id)
    {
        return $this->_em->getRepository(Plan::class)
            ->createQueryBuilder('p')
            ->select('IDENTITY(p.rozpoznanie) AS rozpoznanie, p.opis')
            ->where('p.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult(AbstractQuery::HYDRATE_ARRAY);
    }

    public function getAll()
    {
        return $this->_em->getRepository(Plan::class)
            ->createQueryBuilder('p')
            ->select()
            ->getQuery()
            ->getResult(AbstractQuery::HYDRATE_ARRAY);
    }

    public function update($id, $opis, $rozpoznanieId)
    {
        $this->_em->getRepository(Plan::class)
            ->createQueryBuilder('p')
            ->update()
            ->set('p.opis', "'".$opis."'")
            ->set('p.rozpoznanie', $rozpoznanieId)
            ->where('p.id = :id')
            ->setParameter('id',$id)
            ->getQuery()->execute();
    }

    public function findByOpisOrRozpozanie($searchByValue)
    {
        return $this->_em->getRepository(Plan::class)
            ->createQueryBuilder('p')
            ->select('p.id, p.opis, r.kod AS rozpoznanie_kod, r.opis AS rozpoznanie_opis')
            ->join('p.rozpoznanie','r')
            ->where('p.opis LIKE :value')
            ->orWhere('r.opis LIKE :value')
            ->orWhere('r.kod LIKE :value')
            ->setParameter('value',"%".$searchByValue."%")
            ->getQuery()->getResult();
    }

    public function findByRozpoznanieId($rozpoznanieId)
    {
        return $this->_em->getRepository(Plan::class)
            ->createQueryBuilder('p')
            ->select('p.id, p.opis, r.kod AS rozpoznanie_kod, r.opis AS rozpoznanie_opis')
            ->join('p.rozpoznanie','r')
            ->where('p.rozpoznanie = :rozpoznanieId')
            ->setParameter('rozpoznanieId',$rozpoznanieId)
            ->getQuery()->getResult(AbstractQuery::HYDRATE_ARRAY);
    }
}
