<?php

namespace App\Repository;

use App\Entity\Zabieg;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Zabieg|null find($id, $lockMode = null, $lockVersion = null)
 * @method Zabieg|null findOneBy(array $criteria, array $orderBy = null)
 * @method Zabieg[]    findAll()
 * @method Zabieg[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ZabiegRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Zabieg::class);
    }

    public function getEntityById($id)
    {
        return $this->_em->getRepository(Zabieg::class)
            ->createQueryBuilder('z')
            ->select()
            ->where('z.id = :id')
            ->setParameter(':id',$id)
            ->getQuery()
            ->getResult();
    }

    public function getArrayById($id)
    {
        return $this->_em->getRepository(Zabieg::class)
            ->createQueryBuilder('z')
            ->select('IDENTITY(z.rodzajZabiegu) AS rodzajZabiegu, z.opis, z.cena, z.zalecenia, z.czasTrwania')
            ->where('z.id = :id')
            ->setParameter(':id',$id)
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }

    public function addEntity(Zabieg $zabieg)
    {
        $this->_em->persist($zabieg);
        $this->_em->flush();
    }

    public function removeById($id)
    {
        $this->_em->getRepository(Zabieg::class)
            ->createQueryBuilder('z')
            ->delete()
            ->where('z.id = :id')
            ->setParameter(':id',$id)
            ->getQuery()->execute();
    }

    public function update($zabiegId, Zabieg $zabieg)
    {
        $this->_em->getRepository(Zabieg::class)
            ->createQueryBuilder('z')
            ->update()
            ->set('z.rodzajZabiegu',$zabieg->getRodzajZabiegu()->getId())
            ->set('z.cena',$zabieg->getCena())
            ->set('z.czasTrwania',$zabieg->getCzasTrwania())
            ->set('z.opis',"'".$zabieg->getOpis()."'")
            ->set('z.zalecenia',"'".$zabieg->getZalecenia()."'")
            ->where('z.id = :id')
            ->setParameter(':id',$zabiegId)
            ->getQuery()->execute();
    }

    public function findByOpisOrZalecenia($searchByValue)
    {
        return $this->_em->getRepository(Zabieg::class)
            ->createQueryBuilder('z')
            ->select('rz.nazwa as rodzajZabiegu, z.id, z.opis, z.zalecenia, z.cena, z.czasTrwania')
            ->join('z.rodzajZabiegu','rz')
            ->where('z.opis LIKE :opis')
            ->orWhere('z.zalecenia LIKE :zalecenia')
            ->setParameter('opis',"%".$searchByValue."%")
            ->setParameter('zalecenia',"%".$searchByValue."%")
            ->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }
}
