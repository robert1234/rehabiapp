<?php

namespace App\Repository;

use App\Entity\Aplikacja;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Aplikacja|null find($id, $lockMode = null, $lockVersion = null)
 * @method Aplikacja|null findOneBy(array $criteria, array $orderBy = null)
 * @method Aplikacja[]    findAll()
 * @method Aplikacja[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AplikacjaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Aplikacja::class);
    }

    // /**
    //  * @return Aplikacja[] Returns an array of Aplikacja objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Aplikacja
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
