<?php

namespace App\Repository;

use App\Entity\Pacjent;
use App\Entity\Uprawnienie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;

/**
 * @method Uprawnienie|null find($id, $lockMode = null, $lockVersion = null)
 * @method Uprawnienie|null findOneBy(array $criteria, array $orderBy = null)
 * @method Uprawnienie[]    findAll()
 * @method Uprawnienie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UprawnienieRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Pacjent::class);
    }

    public function getUprawnieniaPrzyznaneByUser($login)
    {
        $query = "SELECT CONCAT('ROLE_',UPPER(element_aplikacji),'_',UPPER(rodzaj_uprawnienia)) AS role FROM tabela_uprawnien WHERE login = :login AND status = 'przyznano'";
        $statement = $this->_em->getConnection()->prepare($query);
        $statement->bindValue('login',$login);
        $statement->execute();

        return $statement->fetchAll();
    }

    public function getUprawnieniaOdebraneByUser($login)
    {
        $query = "SELECT CONCAT('ROLE_',UPPER(element_aplikacji),'_',UPPER(rodzaj_uprawnienia)) AS role FROM tabela_uprawnien WHERE login = :login AND status = 'odebrano'";
        $statement = $this->_em->getConnection()->prepare($query);
        $statement->bindValue('login',$login);
        $statement->execute();

        return $statement->fetchAll();
    }
}
