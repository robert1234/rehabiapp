<?php

namespace App\Repository;

use App\Entity\CzlonekGrupy;
use App\Entity\Grupa;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Grupa|null find($id, $lockMode = null, $lockVersion = null)
 * @method Grupa|null findOneBy(array $criteria, array $orderBy = null)
 * @method Grupa[]    findAll()
 * @method Grupa[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GrupaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Grupa::class);
    }

    public function getGroupList()
    {
        return $this->_em->getRepository(Grupa::class)
            ->createQueryBuilder('g')
            ->select('g.id, g.nazwa')
            ->getQuery()->getResult();
    }

    public function getGrupyByUzytkownikId($uzytkownikSystemuId)
    {
        return $this->_em->getRepository(CzlonekGrupy::class)
            ->createQueryBuilder('cg')
            ->select('cg.id')
            ->where('cg.uzytkownikSystemu = :uzytkownikSystemuId')
            ->setParameter('uzytkownikSystemuId', $uzytkownikSystemuId)
            ->getQuery()
            ->getResult();
    }

    public function getById($id)
    {
        return $this->_em->getRepository(Grupa::class)
            ->createQueryBuilder('g')
            ->select()
            ->where('g.id = :grupaId')
            ->setParameter('grupaId',$id)
            ->getQuery()->getResult();
    }
}
