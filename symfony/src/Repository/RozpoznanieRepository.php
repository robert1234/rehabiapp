<?php

namespace App\Repository;

use App\Entity\Rozpoznanie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Rozpoznanie|null find($id, $lockMode = null, $lockVersion = null)
 * @method Rozpoznanie|null findOneBy(array $criteria, array $orderBy = null)
 * @method Rozpoznanie[]    findAll()
 * @method Rozpoznanie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RozpoznanieRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Rozpoznanie::class);
    }

    public function addRozpoznanie(Rozpoznanie $rozpoznanie)
    {
        $this->_em->persist($rozpoznanie);
        $this->_em->flush();
    }

    public function removeRozpoznanie($kod)
    {
        $this->_em->getRepository(Rozpoznanie::class)
            ->createQueryBuilder('r')
            ->delete()
            ->where('r.kod = :kod')
            ->setParameter('kod', $kod)
            ->getQuery()->execute();
    }

    public function getRozpoznanieById($id)
    {
        return $this->_em->getRepository(Rozpoznanie::class)
            ->createQueryBuilder('r')
            ->select()
            ->where('r.id = :id')
            ->setParameter('id', $id)
            ->getQuery()->getResult();
    }

    public function getAll()
    {
        return $this->_em->getRepository(Rozpoznanie::class)
            ->createQueryBuilder('r')
            ->select()
            ->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }

    public function getByKod($kod)
    {
        return $this->_em->getRepository(Rozpoznanie::class)
            ->createQueryBuilder('r')
            ->select()
            ->where('r.kod = :kod')
            ->setParameter('kod', $kod)
            ->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }
}
