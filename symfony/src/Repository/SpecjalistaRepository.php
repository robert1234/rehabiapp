<?php

namespace App\Repository;

use App\Entity\Specjalista;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Specjalista|null find($id, $lockMode = null, $lockVersion = null)
 * @method Specjalista|null findOneBy(array $criteria, array $orderBy = null)
 * @method Specjalista[]    findAll()
 * @method Specjalista[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SpecjalistaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Specjalista::class);
    }

    public function add(Specjalista $specjalista)
    {
        $this->_em->persist($specjalista);
        $this->_em->flush();
    }

    public function update($id, $oddzial, $specjalizacja, $opisDodatkowy)
    {
        $this->_em->getRepository(Specjalista::class)
            ->createQueryBuilder('s')
            ->update()
            ->set('s.oddzial',$oddzial)
            ->set('s.specjalizacja',$specjalizacja)
            ->set('s.opisDodatkowy',"'".$opisDodatkowy."'")
            ->where('s.id = :id')
            ->setParameter('id',$id)
            ->getQuery()->execute();
    }

    public function updateDataRozpoczecia($id, $startDate)
    {
        $this->_em->getRepository(Specjalista::class)
            ->createQueryBuilder('s')
            ->update()
            ->set('s.dataRozpoczecia',$startDate)
            ->where('s.id = :id')
            ->setParameter('id',$id)
            ->getQuery()->execute();
    }

    public function updateDataZakonczenia($id, $endDate)
    {
        $this->_em->getRepository(Specjalista::class)
            ->createQueryBuilder('s')
            ->update()
            ->set('s.dataZakonczenia',$endDate)
            ->where('s.id = :id')
            ->setParameter('id',$id)
            ->getQuery()->execute();
    }

    public function getEntityById($id)
    {
        return $this->_em->getRepository(Specjalista::class)
            ->createQueryBuilder('s')
            ->select()
            ->where('s.id = :id')
            ->setParameter('id',$id)
            ->getQuery()->getResult();
    }

    public function getArrayById($id)
    {
        return $this->_em->getRepository(Specjalista::class)
            ->createQueryBuilder('s')
            ->select("IDENTITY(s.osoba) AS osoba,IDENTITY(s.specjalizacja) AS specjalizacja,s.opisDodatkowy,IDENTITY(s.oddzial) AS oddzial,s.dataRozpoczecia,s.dataZakonczenia")
            ->where('s.id = :id')
            ->setParameter('id',$id)
            ->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }

    public function getAll()
    {
        return $this->_em->getRepository(Specjalista::class)
            ->createQueryBuilder('s')
            ->select('s.id, s.dataRozpoczecia, s.dataZakonczenia, o.imie, 
                            o.nazwisko, o.pesel, sp.nazwa AS specjalista, od.nazwa AS oddzial')
            ->join('s.specjalizacja','sp')
            ->join('s.osoba', 'o')
            ->join('s.oddzial', 'od')
            ->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }

    public function getCurrent()
    {
        return $this->_em->getRepository(Specjalista::class)
            ->createQueryBuilder('s')
            ->select('s.id, s.dataRozpoczecia, s.dataZakonczenia, o.imie, 
                            o.nazwisko, o.pesel, sp.nazwa AS specjalista, od.nazwa AS oddzial')
            ->join('s.specjalizacja','sp')
            ->join('s.osoba', 'o')
            ->join('s.oddzial', 'od')
            ->where('s.dataZakonczenia IS NULL')
            ->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }

    public function getSpecjalistaFunctionByLoggedUser($userId)
    {
        return $this->_em->getRepository(Specjalista::class)
            ->createQueryBuilder('s')
            ->select()
            ->join('s.osoba','o')
            ->join('o.uzytkownicySystemu','us')
            ->where('us.id = :userId')
            ->orderBy('s.dataRozpoczecia','DESC')
            ->setParameter('userId',$userId)
            ->getQuery()->getResult();
    }
}
