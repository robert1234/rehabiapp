<?php

namespace App\Repository;

use App\Entity\StatusWizyty;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\AbstractQuery;

/**
 * @method StatusWizyty|null find($id, $lockMode = null, $lockVersion = null)
 * @method StatusWizyty|null findOneBy(array $criteria, array $orderBy = null)
 * @method StatusWizyty[]    findAll()
 * @method StatusWizyty[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StatusWizytyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StatusWizyty::class);
    }

    public function getArrayByNazwa($nazwa)
    {
        return $this->_em->getRepository(StatusWizyty::class)
            ->createQueryBuilder('sw')
            ->select()
            ->where('sw.nazwa = :nazwa')
            ->setParameter('nazwa',$nazwa)
            ->getQuery()
            ->getResult(AbstractQuery::HYDRATE_ARRAY);
    }

    public function getArrayById($id)
    {
        return $this->_em->getRepository(StatusWizyty::class)
            ->createQueryBuilder('sw')
            ->select()
            ->where('sw.id = :id')
            ->setParameter('id',$id)
            ->getQuery()
            ->getResult(AbstractQuery::HYDRATE_ARRAY);
    }

    public function getEntityById($id)
    {
        return $this->_em->getRepository(StatusWizyty::class)
            ->findOneBy(['id' => $id]);
    }

    public function getAll()
    {
        return $this->_em->getRepository(StatusWizyty::class)
            ->createQueryBuilder('sw')
            ->select()
            ->getQuery()
            ->getResult(AbstractQuery::HYDRATE_ARRAY);
    }
}
