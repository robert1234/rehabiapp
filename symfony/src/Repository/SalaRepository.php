<?php

namespace App\Repository;

use App\Entity\Oddzial;
use App\Entity\Sala;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Sala|null find($id, $lockMode = null, $lockVersion = null)
 * @method Sala|null findOneBy(array $criteria, array $orderBy = null)
 * @method Sala[]    findAll()
 * @method Sala[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SalaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Sala::class);
    }

    public function addSala(Sala $sala)
    {
        $this->_em->persist($sala);
        $this->_em->flush();
    }

    public function removeSala($oddzialId,$nrSali)
    {
        $this->_em->getRepository(Sala::class)
            ->createQueryBuilder('s')
            ->delete()
            ->where('s.oddzial = :oddzialId')
            ->andWhere('s.nrSali = :nrSali')
            ->setParameter('nrSali', $nrSali)
            ->setParameter('oddzialId', $oddzialId)
            ->getQuery()->execute();
    }

    public function getSalaById($id)
    {
        return $this->_em->getRepository(Sala::class)
            ->createQueryBuilder('s')
            ->select()
            ->where('s.id = :id')
            ->setParameter('id', $id)
            ->getQuery()->getResult();
    }

    public function getAll()
    {
        return $this->_em->getRepository(Sala::class)
            ->createQueryBuilder('s')
            ->select()
            ->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }

    public function getByOddzial($oddzialId)
    {
        return $this->_em->getRepository(Sala::class)
            ->createQueryBuilder('s')
            ->select()
            ->where('s.oddzial = :oddzialId')
            ->setParameter('oddzialId', $oddzialId)
            ->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }
}
