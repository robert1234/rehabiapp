<?php

namespace App\Repository;

use App\Entity\RodzajUprawnienia;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method RodzajUprawnienia|null find($id, $lockMode = null, $lockVersion = null)
 * @method RodzajUprawnienia|null findOneBy(array $criteria, array $orderBy = null)
 * @method RodzajUprawnienia[]    findAll()
 * @method RodzajUprawnienia[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RodzajUprawnieniaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RodzajUprawnienia::class);
    }

    // /**
    //  * @return RodzajUprawnienia[] Returns an array of RodzajUprawnienia objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RodzajUprawnienia
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
