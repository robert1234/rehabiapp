<?php


namespace App\Repository;


use App\Entity\ElementAplikacji;
use App\Entity\Etap;
use App\Entity\StatusWizyty;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class EtapPlannerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Etap::class);
    }

    public function checkTimetableCollisions($data, $sala, $specjalista)
    {
        return $this->_em->getRepository(Etap::class)
            ->createQueryBuilder('e')
            ->select('count(e.id)')
            ->join('e.statusWizyty','sw')
            ->join('e.zabieg','z')
            ->where('sw.id IN (:statusNames)')
            ->andWhere(" DATE_ADD(e.dataPrzeprowadzenia, z.czasTrwania, 'MINUTE') > :data ")
            ->andWhere(" DATE_SUB(e.dataPrzeprowadzenia, z.czasTrwania, 'MINUTE') < :data ")
            ->andWhere(" e.sala = :sala ")
            ->andWhere(" e.specjalista = :specjalista ")
            ->setParameter('statusNames',[StatusWizyty::ZAREJESTROWANO,StatusWizyty::ZAPLANOWANO])
            ->setParameter('data', $data)
            ->setParameter('sala', $sala)
            ->setParameter('specjalista', $specjalista)
            ->getQuery()->getOneOrNullResult();
    }

    public function getFullTimetableBySpecjalistaAndSala($specjalistaId, $salaId)
    {
        return $this->_em->getRepository(Etap::class)
            ->createQueryBuilder('e')
            ->select('e.dataPrzeprowadzenia, sw.nazwa AS status, z.opis AS zabieg, z.czasTrwania')
            ->join('e.zabieg','z')
            ->join('e.statusWizyty','sw')
            ->where('e.specjalista = :spec')
            ->andWhere('e.sala = :sala')
            ->setParameter('sala',$salaId)
            ->setParameter('spec',$specjalistaId)
            ->getQuery()->getResult();
    }

    public function getCurrentTimetableBySpecjalistaAndSala($specjalistaId, $salaId)
    {
        return $this->_em->getRepository(Etap::class)
            ->createQueryBuilder('e')
            ->select('e.dataPrzeprowadzenia, sw.nazwa AS status, z.opis AS zabieg, z.czasTrwania')
            ->join('e.zabieg','z')
            ->join('e.statusWizyty','sw')
            ->where('e.specjalista = :spec')
            ->andWhere('e.sala = :sala')
            ->andWhere('e.dataPrzeprowadzenia >= :data')
            ->setParameter('sala',$salaId)
            ->setParameter('spec',$specjalistaId)
            ->setParameter('data', (new \DateTime())->format("Y-m-d")." 00:00:00" )
            ->getQuery()->getResult();
    }

    public function getDayTimetableBySpecjalistaAndSala($specjalistaId, $salaId, $date)
    {
        return $this->_em->getRepository(Etap::class)
            ->createQueryBuilder('e')
            ->select('e.dataPrzeprowadzenia, sw.nazwa AS status, z.opis AS zabieg, z.czasTrwania')
            ->join('e.zabieg','z')
            ->join('e.statusWizyty','sw')
            ->where('e.specjalista = :spec')
            ->andWhere('e.sala = :sala')
            ->andWhere('e.dataPrzeprowadzenia BETWEEN :dataStart AND :dataEnd')
            ->setParameter('sala',$salaId)
            ->setParameter('spec',$specjalistaId)
            ->setParameter('dataStart', $date." 00:00:00" )
            ->setParameter('dataEnd', $date." 23:59:59" )
            ->getQuery()->getResult();
    }
}