<?php

namespace App\Repository;

use App\Entity\CzlonekGrupy;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method CzlonekGrupy|null find($id, $lockMode = null, $lockVersion = null)
 * @method CzlonekGrupy|null findOneBy(array $criteria, array $orderBy = null)
 * @method CzlonekGrupy[]    findAll()
 * @method CzlonekGrupy[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CzlonekGrupyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CzlonekGrupy::class);
    }

    // /**
    //  * @return CzlonekGrupy[] Returns an array of CzlonekGrupy objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CzlonekGrupy
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
