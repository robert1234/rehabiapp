<?php

namespace App\Repository;

use App\Entity\Osoba;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;

/**
 * @method Osoba|null find($id, $lockMode = null, $lockVersion = null)
 * @method Osoba|null findOneBy(array $criteria, array $orderBy = null)
 * @method Osoba[]    findAll()
 * @method Osoba[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OsobaRepository extends  ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Osoba::class);
    }

    public function addOsoba(Osoba $osoba)
    {
        $this->_em->persist($osoba);
        $this->_em->flush();
    }

    public function updateOsoba(Osoba $osoba)
    {
        $this->_em->flush();
    }

    public function removeOsoba(Osoba $osoba)
    {
        $this->_em->remove();
        $this->_em->flush();
    }

    public function selectAll()
    {
        return $this->_em->getRepository(Osoba::class)->findAll();
    }

    public function findByNameOrSurnameOrPesel($searchByValue)
    {
        return $this->_em->getRepository(Osoba::class)
            ->createQueryBuilder('o')
            ->select('o.id, o.imie, o.nazwisko, o.pesel, o.ulica, o.nrDomu, o.kodPocztowy, o.miejscowosc')
            ->where('o.imie like :searchByValue')
            ->orWhere('o.nazwisko like :searchByValue')
            ->orWhere('o.pesel like :searchByValue')
            ->setParameter('searchByValue', '%'.$searchByValue.'%')
            ->getQuery()->execute();
    }

    public function findById($osobaId)
    {
        return $this->_em->getRepository(Osoba::class)
            ->createQueryBuilder('o')
            ->select()
            ->where('o.id = :osobaId')
            ->setParameter('osobaId', $osobaId)
            ->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }

    public function findEntityById($osobaId)
    {
        return $this->_em->getRepository(Osoba::class)
            ->createQueryBuilder('o')
            ->select()
            ->where('o.id = :osobaId')
            ->setParameter('osobaId', $osobaId)
            ->getQuery()->getResult();
    }

    public function updateOsobaFromJson($request, $id)
    {
        $this->_em->getConnection()->update('osoba',
            [
                'imie' => $request['imie'],
                'nazwisko' => $request['nazwisko'],
                'pesel' => $request['pesel'],
                'data_urodzenia' => $request['dataUrodzenia'],
                'imie_ojca' => $request['imieOjca'],
                'imie_matki' => $request['imieMatki'],
                'plec' => $request['plec'],
                'ulica' => $request['ulica'],
                'nr_domu' => $request['nrDomu'],
                'kod_pocztowy' => $request['kodPocztowy'],
                'miejscowosc' => $request['miejscowosc']
            ],
            [ 'id' => $id] );
    }
}
