<?php

namespace App\Repository;

use App\Entity\Etap;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\AbstractQuery;

/**
 * @method Etap|null find($id, $lockMode = null, $lockVersion = null)
 * @method Etap|null findOneBy(array $criteria, array $orderBy = null)
 * @method Etap[]    findAll()
 * @method Etap[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EtapRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Etap::class);
    }

    public function addEntity(Etap $etap)
    {
        $this->_em->persist($etap);
        $this->_em->flush();
    }

    public function getEntityById($id)
    {
        return $this->_em->getRepository(Etap::class)
            ->findOneBy(['id'=>$id]);
    }

    public function getArrayById($id)
    {
        return $this->_em->getRepository(Etap::class)
            ->createQueryBuilder('e')
            ->select()
            ->where('e.id = :id')
            ->setParameter('id',$id)
            ->getQuery()
            ->getResult(AbstractQuery::HYDRATE_ARRAY);
    }

    public function updateDataPrzeprowadzenia($id, $date)
    {
        $this->_em->getRepository(Etap::class)
            ->createQueryBuilder('e')
            ->update()
            ->set('e.dataPrzeprowadzenia',"'".$date."'")
            ->where('e.id = :id')
            ->setParameter('id',$id)
            ->getQuery()->execute();
    }

    public function updateSala($id, $salaId)
    {
        $this->_em->getRepository(Etap::class)
            ->createQueryBuilder('e')
            ->update()
            ->set('e.sala',$salaId)
            ->where('e.id = :id')
            ->setParameter('id',$id)
            ->getQuery()->execute();
    }

    public function updateSpecjalista($id, $specjalistaId)
    {
        $this->_em->getRepository(Etap::class)
            ->createQueryBuilder('e')
            ->update()
            ->set('e.specjalista',$specjalistaId)
            ->where('e.id = :id')
            ->setParameter('id',$id)
            ->getQuery()->execute();
    }

    public function updateStatusWizyty($id, $statusId)
    {
        $this->_em->getRepository(Etap::class)
            ->createQueryBuilder('e')
            ->update()
            ->set('e.statusWizyty',$statusId)
            ->where('e.id = :id')
            ->setParameter('id',$id)
            ->getQuery()->execute();
    }

    public function findEtapWithFilledDataByTurnus($turnusId)
    {
        return $this->_em->getRepository(Etap::class)
            ->createQueryBuilder('e')
            ->select()
            ->where('e.turnus = :turnusId')
            ->andWhere('e.dataPrzeprowadzenia IS NOT NULL')
            ->setParameter('turnusId',$turnusId)
            ->getQuery()->getResult();
    }

    public function findEtapByTurnus($turnusId)
    {
        return $this->_em->getRepository(Etap::class)
            ->createQueryBuilder('e')
            ->select('e.id, z.opis AS zabieg, IDENTITY(e.zabieg) AS zabieg_id, IDENTITY(e.sala) AS sala, 
                            s.nrSali, sw.nazwa AS nazwa_statusu,
                            IDENTITY(e.specjalista) AS specjalista, IDENTITY(e.turnus) AS turnus,
                            IDENTITY(e.statusWizyty) AS status, e.dataPrzeprowadzenia')
            ->leftJoin('e.zabieg','z')
            ->leftJoin('e.sala','s')
            ->leftJoin('e.statusWizyty','sw')
            ->where('e.turnus = :turnusId')
            ->setParameter('turnusId',$turnusId)
            ->getQuery()->getResult(AbstractQuery::HYDRATE_ARRAY);
    }
}
