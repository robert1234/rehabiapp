<?php

namespace App\Repository;

use App\Entity\Turnus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\AbstractQuery;

/**
 * @method Turnus|null find($id, $lockMode = null, $lockVersion = null)
 * @method Turnus|null findOneBy(array $criteria, array $orderBy = null)
 * @method Turnus[]    findAll()
 * @method Turnus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TurnusRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Turnus::class);
    }

    public function addEntity(Turnus $turnus)
    {
        $this->_em->persist($turnus);
        $this->_em->flush();
        return $turnus;
    }

    public function getEntity($id)
    {
        return $this->_em->getRepository(Turnus::class)
            ->findOneBy(['id' => $id]);
    }

    public function getArray($id)
    {
        return $this->_em->getRepository(Turnus::class)
            ->createQueryBuilder('t')
            ->select()
            ->where('t.id = :id')
            ->setParameter('id',$id)
            ->getQuery()
            ->getResult(AbstractQuery::HYDRATE_ARRAY);
    }

    public function getAll()
    {
        return $this->_em->getRepository(Turnus::class)
            ->createQueryBuilder('t')
            ->select()
            ->getQuery()
            ->getResult(AbstractQuery::HYDRATE_ARRAY);
    }

    public function findBySearchValue($searchValue)
    {
        return $this->_em->getRepository(Turnus::class)
            ->createQueryBuilder('t')
            ->select('t.id, o.imie, o.nazwisko, o.pesel, pl.opis AS plan, t.opis')
            ->join('t.plan','pl')
            ->join('t.pacjent','pc')
            ->join('pc.osoba','o')
            ->where('o.imie LIKE :searchValue')
            ->orWhere('o.nazwisko LIKE :searchValue')
            ->orWhere('o.pesel LIKE :searchValue')
            ->orWhere('pl.opis LIKE :searchValue')
            ->orWhere('t.opis LIKE :searchValue')
            ->setParameter('searchValue',"%".$searchValue."%")
            ->getQuery()->getResult();
    }

    public function findByPacjent($pacjentId)
    {
        return $this->_em->getRepository(Turnus::class)
            ->findBy(['pacjent' => $pacjentId]);
    }
}
