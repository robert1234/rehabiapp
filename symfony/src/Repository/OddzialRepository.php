<?php

namespace App\Repository;

use App\Entity\Oddzial;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Oddzial|null find($id, $lockMode = null, $lockVersion = null)
 * @method Oddzial|null findOneBy(array $criteria, array $orderBy = null)
 * @method Oddzial[]    findAll()
 * @method Oddzial[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OddzialRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Oddzial::class);
    }

    public function addOddzial(Oddzial $oddzial)
    {
        $this->_em->persist($oddzial);
        $this->_em->flush();
    }

    public function removeOddzial($nazwa)
    {
        $this->_em->getRepository(Oddzial::class)
            ->createQueryBuilder('o')
            ->delete()
            ->where('o.nazwa = :nazwa')
            ->setParameter('nazwa', $nazwa)
            ->getQuery()->execute();
    }

    public function getOddzialById($id)
    {
        return $this->_em->getRepository(Oddzial::class)
            ->createQueryBuilder('o')
            ->select()
            ->where('o.id = :id')
            ->setParameter('id', $id)
            ->getQuery()->getResult();
    }

    public function getAll()
    {
        return $this->_em->getRepository(Oddzial::class)
            ->createQueryBuilder('o')
            ->select()
            ->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }
}
