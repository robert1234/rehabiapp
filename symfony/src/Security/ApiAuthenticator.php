<?php


namespace App\Security;


use App\Service\UprawnieniaService;
use App\Service\UzytkownikSystemuService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

class ApiAuthenticator extends AbstractGuardAuthenticator
{
    /** @var UzytkownikSystemuService */
    private $uzytkownikSystemuSrv;
    /** @var UserPasswordEncoderInterface  */
    private $passwordEncoder;
    /** @var UprawnieniaService */
    private $uprawnieniaSrv;

    /**
     * ApiAuthenticator constructor.
     * @param UzytkownikSystemuService $uzytkownikSystemuSrv
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param UprawnieniaService $uprawnieniaSrv
     */
    public function __construct(UzytkownikSystemuService $uzytkownikSystemuSrv,
                                UserPasswordEncoderInterface $passwordEncoder,
                                UprawnieniaService $uprawnieniaSrv)
    {
        $this->uzytkownikSystemuSrv = $uzytkownikSystemuSrv;
        $this->passwordEncoder = $passwordEncoder;
        $this->uprawnieniaSrv = $uprawnieniaSrv;
    }

    /**
     * Called on every request to decide if this authenticator should be
     * used for the request. Returning `false` will cause this authenticator
     * to be skipped.
     * @param Request $request
     * @return bool
     */
    public function supports(Request $request)
    {
        return $request->headers->has('X-AUTH-LOGIN') && $request->headers->has('X-AUTH-PASSWORD');
    }

    /**
     * Called on every request. Return whatever credentials you want to
     * be passed to getUser() as $credentials.
     * @param Request $request
     * @return array
     */
    public function getCredentials(Request $request)
    {
        return [
            "login" => $request->headers->get('X-AUTH-LOGIN'),
            "password" => $request->headers->get("X-AUTH-PASSWORD")
        ];
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        if (null === $credentials) {
            return null;
        }
        $user = $userProvider->loadUserByUsername($credentials['login']);
        $roles = $this->uprawnieniaSrv->getRolesList($credentials['login']);
        $user->setRoles($roles);
        return $user;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        return $this->passwordEncoder->isPasswordValid($user, $credentials['password']);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $data = [
            'message' => strtr($exception->getMessageKey(), $exception->getMessageData())
        ];

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }

    /**
     * Called when authentication is needed, but it's not sent
     * @param Request $request
     * @param AuthenticationException|null $authException
     * @return JsonResponse
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        $data = [
            'message' => 'Authentication Required'
        ];

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }

    public function supportsRememberMe()
    {
        return false;
    }
}