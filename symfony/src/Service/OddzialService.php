<?php


namespace App\Service;


use App\Entity\Oddzial;
use App\Entity\Sala;
use App\Repository\OddzialRepository;
use App\Repository\SalaRepository;

class OddzialService
{
    private $oddzialRepository;
    private $salaRepository;

    /**
     * OddzialService constructor.
     * @param $oddzialRepository
     */
    public function __construct(OddzialRepository $oddzialRepository, SalaRepository $salaRepository)
    {
        $this->oddzialRepository = $oddzialRepository;
        $this->salaRepository = $salaRepository;
    }

    public function addOddzial($nazwa)
    {
        $oddzial = new Oddzial();
        $oddzial->setNazwa($nazwa);
        $this->oddzialRepository->addOddzial($oddzial);
    }

    public function removeOddzial($nazwa)
    {
        $this->oddzialRepository->removeOddzial($nazwa);
    }

    public function getOddzialById($id)
    {
        $oddzial = $this->oddzialRepository->getOddzialById($id);
        if(empty($oddzial)) {
            return null;
        }else{
            return $oddzial[0];
        }
    }

    public function getAll()
    {
        return $this->oddzialRepository->getAll();
    }

    public function addSala($oddzialId,$nrSali)
    {
        $sala = new Sala();
        $oddzial = $this->oddzialRepository->getOddzialById($oddzialId)[0];
        $sala->setOddzial($oddzial);
        $sala->setNrSali($nrSali);
        $this->salaRepository->addSala($sala);
    }

    public function removeSala($oddzialId,$nrSali)
    {
        $this->salaRepository->removeSala($oddzialId,$nrSali);
    }

    public function getSalaById($id)
    {
        $sala = $this->salaRepository->getSalaById($id);
        if(empty($oddzial)) {
            return null;
        }else{
            return $sala[0];
        }
    }

    public function getAllSale()
    {
        return $this->salaRepository->getAll();
    }

    public function getSalaByOddzial($oddzialId)
    {
        return $this->salaRepository->getByOddzial($oddzialId);
    }
}