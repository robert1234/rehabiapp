<?php


namespace App\Service;


use App\Entity\Specjalizacja;
use App\Repository\SpecjalizacjaRepository;

class SpecjalizacjaService
{
    private $specjalizacjaRepository;

    /**
     * SpecjalizacjaService constructor.
     * @param $specjalizacjaRepository
     */
    public function __construct(SpecjalizacjaRepository $specjalizacjaRepository)
    {
        $this->specjalizacjaRepository = $specjalizacjaRepository;
    }

    public function addSpecjalizacja($nazwa)
    {
        $specjalizacja = new Specjalizacja();
        $specjalizacja->setNazwa($nazwa);
        $this->specjalizacjaRepository->addSpecjalizacja($specjalizacja);
    }

    public function removeSpecjalizacja($nazwa)
    {
        $this->specjalizacjaRepository->removeSpecjalizacja($nazwa);
    }

    public function getSpecjalizacjaById($id)
    {
        $specjalizacja = $this->specjalizacjaRepository->getSpecjalizacjaById($id);
        if(empty($specjalizacja)) {
            return null;
        }else{
            return $specjalizacja[0];
        }
    }

    public function getAll()
    {
        return $this->specjalizacjaRepository->getAll();
    }

}