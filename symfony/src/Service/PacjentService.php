<?php


namespace App\Service;

use App\Entity\Pacjent;
use App\Repository\PacjentRepository;

class PacjentService
{
    private $pacjentRep;
    private $osobaSrv;
    private $rozpoznanieSrv;

    /**
     * PacjentService constructor.
     * @param PacjentRepository $pacjentRep
     * @param OsobaService $osobaSrv
     * @param RozpoznanieService $rozpoznanieSrv
     */
    public function __construct(PacjentRepository $pacjentRep, OsobaService $osobaSrv, RozpoznanieService $rozpoznanieSrv)
    {
        $this->pacjentRep = $pacjentRep;
        $this->osobaSrv = $osobaSrv;
        $this->rozpoznanieSrv = $rozpoznanieSrv;
    }

    public function add($osobaId, $rozpoznanieId, $platnik, $opisDodatkowy)
    {
        $pacjent = new Pacjent();
        $osoba = $this->osobaSrv->findEntityById($osobaId);
        $rozpoznanie = $this->rozpoznanieSrv->getRozpoznanieById($rozpoznanieId);
        $pacjent->setOsoba($osoba);
        $pacjent->setRozpoznanie($rozpoznanie);
        $pacjent->setPlatnik($platnik);
        $pacjent->setOpisDodatkowy($opisDodatkowy);
        $pacjent->setDataRozpoczecia(new \DateTime());
        $this->pacjentRep->add($pacjent);
    }

    public function update($id, $platnik, $rozpoznanie, $opisDodatkowy)
    {
        $this->pacjentRep->update($id, $platnik, $rozpoznanie, $opisDodatkowy);
    }

    public function updateDataRozpoczecia($id, $startDate)
    {
        $this->pacjentRep->updateDataRozpoczecia($id, $startDate);
    }

    public function updateDataZakonczenia($id, $endDate)
    {
        $this->pacjentRep->updateDataZakonczenia($id, $endDate);
    }

    public function getEntityById($id)
    {
        $pacjent = $this->pacjentRep->getEntityById($id);
        return empty($pacjent) ? null : $pacjent[0];
    }

    public function getArrayById($id)
    {
        $pacjent = $this->pacjentRep->getArrayById($id);
        return empty($pacjent) ? null : $pacjent[0];
    }

    public function getAll()
    {
        return $this->pacjentRep->getAll();
    }

    public function getCurrent()
    {
        return $this->pacjentRep->getCurrent();
    }

    public function getCurrentByUzytkownikSystemu($uzytkownikId)
    {
        $uzytkownikSystemu = $this->pacjentRep->getCurrentByUzytkownikSystemu($uzytkownikId);
        return empty($uzytkownikSystemu) ? null : $uzytkownikSystemu[0];
    }
}