<?php


namespace App\Service;


use App\Entity\Etap;
use App\Entity\Turnus;
use App\Repository\EtapRepository;
use Doctrine\ORM\AbstractQuery;

class EtapService
{
    private $etapRep;
    private $statusSrv;
    private $oddzialSrv;
    private $specjalistaSrv;
    private $zabiegSrv;
    private $codeGenerator;

    /**
     * EtapService constructor.
     * @param EtapRepository $etapRep
     * @param StatusWizytyService $statusSrv
     * @param OddzialService $oddzialSrv
     * @param SpecjalistaService $specjalistaSrv
     * @param ZabiegService $zabiegSrv
     * @param CodeGenerator $codeGenerator
     */
    public function __construct(EtapRepository $etapRep,
                                StatusWizytyService $statusSrv,
                                OddzialService $oddzialSrv,
                                SpecjalistaService $specjalistaSrv,
                                ZabiegService $zabiegSrv,
                                CodeGenerator $codeGenerator)
    {
        $this->etapRep = $etapRep;
        $this->statusSrv = $statusSrv;
        $this->oddzialSrv = $oddzialSrv;
        $this->specjalistaSrv = $specjalistaSrv;
        $this->zabiegSrv = $zabiegSrv;
        $this->codeGenerator = $codeGenerator;
    }

    public function addEntity($zabiegId, Turnus $turnus, $statusId)
    {
        $etap = new Etap();

        $zabieg = $this->zabiegSrv->getEntityById($zabiegId);
        $status = $this->statusSrv->getEntityById($statusId);
        $validationCode = $this->codeGenerator->generateRandomString();

        $etap->setZabieg($zabieg);
        $etap->setTurnus($turnus);
        $etap->setStatusWizyty($status);
        $etap->setKodWalidacyjny($validationCode);

        $this->etapRep->addEntity($etap);
    }

    public function getEntityById($id)
    {
        return $this->etapRep->getEntityById($id);
    }

    public function getArrayById($id)
    {
        $etap = $this->etapRep->getArrayById($id);
        return empty($etap) ? null : $etap[0];
    }

    public function updateDataPrzeprowadzenia($id, $date)
    {
        $this->etapRep->updateDataPrzeprowadzenia($id, $date);
    }

    public function updateSala($id, $salaId)
    {
        $this->etapRep->updateSala($id, $salaId);
    }

    public function updateSpecjalista($id, $specjalistaId)
    {
        $this->etapRep->updateSpecjalista($id, $specjalistaId);
    }

    public function updateStatusWizyty($id, $statusId)
    {
        $this->etapRep->updateStatusWizyty($id, $statusId);
    }

    public function findEtapWithFilledDataByTurnus($turnusId)
    {
        return $this->etapRep->findEtapWithFilledDataByTurnus($turnusId);
    }

    public function findEtapByTurnus($turnusId)
    {
        return $this->etapRep->findEtapByTurnus($turnusId);
    }

    function validateCode($etapId, $code, $login) : bool
    {
        $etap = $this->getEntityById($etapId);
        $uzytkownikSystemu = $etap->getTurnus()->getPacjent()->getOsoba()->getUzytkownikSystemu();
        $validCode = $etap->getKodWalidacyjny();

        $loginCompareResult = ( strcmp($uzytkownikSystemu->getLogin(),$login) == 0 );
        $codeCompareResult = ( strcmp($validCode,$code) == 0 );
        return ($loginCompareResult && $codeCompareResult);
    }

}