<?php


namespace App\Service;


use App\Repository\UprawnienieRepository;

class UprawnieniaService
{
    private $uprawnieniaRepository;

    /**
     * UprawnieniaService constructor.
     * @param $uprawnieniaRepository
     */
    public function __construct(UprawnienieRepository $uprawnieniaRepository)
    {
        $this->uprawnieniaRepository = $uprawnieniaRepository;
    }

    public function getRolesList($user)
    {
        $roleArray = $this->uprawnieniaRepository->getUprawnieniaPrzyznaneByUser($user);
        $rolesList = [];
        foreach ($roleArray as $role){
            array_push($rolesList,$role['role']);
        }
        return $rolesList;
    }

}