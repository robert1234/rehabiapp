<?php


namespace App\Service;


use App\Entity\PozycjaPlanu;
use App\Repository\PozycjaPlanuRepository;
use Doctrine\ORM\AbstractQuery;

class PozycjaPlanuService
{
    private $pozycjaPlanuRep;
    private $planSrv;
    private $zabiegSrv;

    /**
     * PozycjaPlanuService constructor.
     * @param $pozycjaPlanuRep
     * @param $planSrv
     * @param $zabiegSrv
     */
    public function __construct(PozycjaPlanuRepository $pozycjaPlanuRep,
                                PlanService $planSrv,
                                ZabiegService $zabiegSrv)
    {
        $this->pozycjaPlanuRep = $pozycjaPlanuRep;
        $this->planSrv = $planSrv;
        $this->zabiegSrv = $zabiegSrv;
    }


    public function addEntity($planId, $zabiegId) // $liczbaPorzadkowa - In future use
    {
        $plan = $this->planSrv->getEntityById($planId);
        $zabieg = $this->zabiegSrv->getEntityById($zabiegId);
        $pozycjaPlanu = new PozycjaPlanu();
        $pozycjaPlanu->setPlan($plan);
        $pozycjaPlanu->setZabieg($zabieg);
        $this->pozycjaPlanuRep->addEntity($pozycjaPlanu);
    }

    public function updateListByPlan($planId, $list){
        $plan = $this->planSrv->getEntityById($planId);
        $curentList = $this->getZabiegListByPlan($planId);
        $list = is_null($list) ? [] : $list;

        $listToAdd = array_diff($list,$curentList);
        $listToRemove = array_diff($curentList,$list);

        foreach ($listToRemove as $zabiegId) {
            $this->removeByPlanAndZabieg($planId,$zabiegId);
        }
        foreach ($listToAdd as $zabiegId) {
            $this->addEntity($planId, $zabiegId);
        }
    }

    public function getZabiegListByPlan($planId)
    {
        $pozycje = $this->findByPlan($planId);
        $list = [];
        foreach ($pozycje as $pozycja) {
            array_push($list,$pozycja->getZabieg()->getId());
        }
        return $list;
    }

    public function getEntityById($id)
    {
        $pozycjaPlanu = $this->pozycjaPlanuRep->getEntityById($id);
        return empty($pozycjaPlanu) ? null : $pozycjaPlanu[0];
    }

    public function getArrayById($id)
    {
        $pozycjaPlanu = $this->pozycjaPlanuRep->getArrayById($id);
        return empty($pozycjaPlanu) ? null : $pozycjaPlanu[0];
    }

    public function getAll()
    {
        return $this->pozycjaPlanuRep->getAll();
    }

    public function findByPlan($planId)
    {
        return $this->pozycjaPlanuRep->findByPlan($planId);
    }

    public function removeById($id)
    {
        $this->pozycjaPlanuRep->removeById($id);
    }

    public function removeByPlanAndZabieg($planId, $zabiegId)
    {
        $this->pozycjaPlanuRep->removeByPlanAndZabieg($planId, $zabiegId);
    }

    public function getZabiegIdListByPlan($planId)
    {
        $pozycjePlanu = $this->findByPlan($planId);
        $zabiegIdList = [];
        foreach ($pozycjePlanu as $item) {
            array_push($zabiegIdList, $item->getZabieg()->getId());
        }
        return $zabiegIdList;
    }

}