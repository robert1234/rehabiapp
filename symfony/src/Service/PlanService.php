<?php


namespace App\Service;


use App\Entity\Plan;
use App\Repository\PlanRepository;
use Doctrine\ORM\AbstractQuery;

class PlanService
{
    private $planRepository;
    private $rozpoznanieService;
    private $pacjentService;

    /**
     * PlanService constructor.
     * @param PlanRepository $planRepository
     * @param PacjentService $pacjentService
     * @param RozpoznanieService $rozpoznanieService
     */
    public function __construct(PlanRepository $planRepository,
                                PacjentService $pacjentService,
                                RozpoznanieService $rozpoznanieService)
    {
        $this->planRepository = $planRepository;
        $this->pacjentService = $pacjentService;
        $this->rozpoznanieService = $rozpoznanieService;
    }

    public function addEntity($rozpoznanieId, $opis)
    {
        $rozpoznanie = $this->rozpoznanieService->getRozpoznanieById($rozpoznanieId);
        $plan = new Plan();
        $plan->setRozpoznanie($rozpoznanie);
        $plan->setOpis($opis);
        return $this->planRepository->addEntity($plan);
    }

    public function getEntityById($id)
    {
        $plan = $this->planRepository->getEntityById($id);
        return empty($plan) ? null : $plan[0];
    }

    public function getArrayById($id)
    {
        $plan = $this->planRepository->getArrayById($id);
        return empty($plan) ? null : $plan[0];
    }

    public function getAll()
    {
        return $this->planRepository->getAll();
    }

    public function update($id, $opis, $rozpoznanieId)
    {
        $this->planRepository->update($id, $opis, $rozpoznanieId);
    }

    public function findByOpisOrRozpozanie($searchByValue)
    {
        return $this->planRepository->findByOpisOrRozpozanie($searchByValue);
    }

    public function findByRozpoznanieId($rozpoznanieId)
    {
        return $this->planRepository->findByRozpoznanieId($rozpoznanieId);
    }

    public function findPlanyForPacjent($pacjentId)
    {
        $pacjent = $this->pacjentService->getEntityById($pacjentId);
        if(is_null($pacjent)){
            return [];
        } else {
            $rozpoznanieId = $pacjent->getRozpoznanie()->getId();
            dump($rozpoznanieId);
            return $this->findByRozpoznanieId($rozpoznanieId);
        }
    }
}