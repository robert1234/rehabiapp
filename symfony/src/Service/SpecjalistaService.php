<?php


namespace App\Service;

use App\Entity\Specjalista;
use App\Repository\SpecjalistaRepository;

class SpecjalistaService
{
    private $specjalistaRep;
    private $osobaSrv;
    private $specjalizacjaSrv;
    private $odzdzialSrv;

    /**
     * PacjentService constructor.
     * @param $specjalistaRep
     * @param $osobaSrv
     * @param $specjalizacjaSrv
     * @param $odzdzialSrv
     */
    public function __construct(SpecjalistaRepository $specjalistaRep,
                                OsobaService $osobaSrv,
                                SpecjalizacjaService $specjalizacjaSrv,
                                OddzialService $odzdzialSrv)
    {
        $this->specjalistaRep = $specjalistaRep;
        $this->osobaSrv = $osobaSrv;
        $this->specjalizacjaSrv = $specjalizacjaSrv;
        $this->odzdzialSrv = $odzdzialSrv;
    }


    public function add($osobaId, $oddzial, $specjalizacja, $opisDodatkowy)
    {
        $specjalista = new Specjalista();
        $osoba = $this->osobaSrv->findEntityById($osobaId);
        $specjalizacja = $this->specjalizacjaSrv->getSpecjalizacjaById($specjalizacja);
        $oddzial = $this->odzdzialSrv->getOddzialById($oddzial);
        $specjalista->setOsoba($osoba);
        $specjalista->setOddzial($oddzial);
        $specjalista->setSpecjalizacja($specjalizacja);
        $specjalista->setOpisDodatkowy($opisDodatkowy);
        $specjalista->setDataRozpoczecia(new \DateTime());

        $this->specjalistaRep->add($specjalista);
    }

    public function update($id, $oddzial, $specjalizacja, $opisDodatkowy)
    {
        $this->specjalistaRep->update($id, $oddzial, $specjalizacja, $opisDodatkowy);
    }

    public function updateDataRozpoczecia($id, $startDate)
    {
        $this->specjalistaRep->updateDataRozpoczecia($id, $startDate);
    }

    public function updateDataZakonczenia($id, $endDate)
    {
        $this->specjalistaRep->updateDataZakonczenia($id, $endDate);
    }

    public function getEntityById($id)
    {
        $specjalista = $this->specjalistaRep->getEntityById($id);
        return empty($specjalista) ? null : $specjalista[0];
    }

    public function getArrayById($id)
    {
        $specjalista = $this->specjalistaRep->getArrayById($id);
        return empty($specjalista) ? null : $specjalista[0];
    }

    public function getAll()
    {
        return $this->specjalistaRep->getAll();
    }

    public function getCurrent()
    {
        return $this->specjalistaRep->getCurrent();
    }

    public function getSpecjalistaFunctionByLoggedUser($userId)
    {
        $spec = $this->specjalistaRep->getSpecjalistaFunctionByLoggedUser($userId);
        return empty($spec) ? null : $spec[0];
    }
}