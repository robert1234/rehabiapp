<?php


namespace App\Service;


use App\Entity\RelacjaMiedzyZabiegami;
use App\Repository\RelacjaMiedzyZabiegamiRepository;

class RelacjaMiedzyZabiegamiService
{
    private $relacjaMiedzyZabiegamiRepository;
    private $zabiegService;

    /**
     * RelacjaMiedzyZabiegamiService constructor.
     * @param RelacjaMiedzyZabiegamiRepository $relacjaMiedzyZabiegamiRepository
     * @param ZabiegService $zabiegService
     */
    public function __construct(RelacjaMiedzyZabiegamiRepository $relacjaMiedzyZabiegamiRepository,
                                ZabiegService $zabiegService)
    {
        $this->relacjaMiedzyZabiegamiRepository = $relacjaMiedzyZabiegamiRepository;
        $this->zabiegService = $zabiegService;
    }

    public function addRelacjaMiedzyZabiegami($zabiegX, $zabiegY, $minimalnyOdstep)
    {
        $zabiegXEnt = $this->zabiegService->getEntityById($zabiegX);
        $zabiegYEnt = $this->zabiegService->getEntityById($zabiegY);
        $relacjaMiedzyZabiegami = new RelacjaMiedzyZabiegami();
        $relacjaMiedzyZabiegami->setMinimalnyOdstep($minimalnyOdstep);
        $relacjaMiedzyZabiegami->setZabiegX($zabiegXEnt);
        $relacjaMiedzyZabiegami->setZabiegY($zabiegYEnt);
        $this->relacjaMiedzyZabiegamiRepository->addRelacjaMiedzyZabiegami($relacjaMiedzyZabiegami);
    }

    public function removeRelacjaMiedzyZabiegami($zabiegX, $zabiegY)
    {
        $this->relacjaMiedzyZabiegamiRepository->removeRelacjaMiedzyZabiegami($zabiegX,$zabiegY);
    }

    public function removeRelacjaMiedzyZabiegamiById($zabiegId)
    {
        $this->relacjaMiedzyZabiegamiRepository->removeRelacjaMiedzyZabiegamiById($zabiegId);
    }

    public function getRelacjaMiedzyZabiegami($zabiegX, $zabiegY)
    {
        $rel = $this->relacjaMiedzyZabiegamiRepository->getRelacjaMiedzyZabiegami($zabiegX,$zabiegY);
        return empty($rel) ? null : $rel[0];
    }

    public function getRelacjaMiedzyZabiegamiById($id)
    {
        $rel = $this->relacjaMiedzyZabiegamiRepository->getRelacjaMiedzyZabiegamiById($id);
        return empty($rel) ? null : $rel[0];
    }

    public function getAll()
    {
        return $this->relacjaMiedzyZabiegamiRepository->getAll();
    }

    public function findByZabieg($valueOne, $valueTwo)
    {
        return $this->relacjaMiedzyZabiegamiRepository->findByZabieg($valueOne, $valueTwo);
    }

    public function getRelacjaMatrixByZabiegIdList($list)
    {
        $matrix = null;
        foreach ($list as $zabiegX) {
            foreach ($list as $zabiegY) {
                $relacja = $this->getRelacjaMiedzyZabiegami($zabiegX,$zabiegY);
                if( !is_null($relacja) ) {
                    $matrix[$zabiegX][$zabiegY] = $relacja->getMinimalnyOdstep;
                }
            }
        }
        return $matrix;
    }

}