<?php


namespace App\Service;


use App\Entity\CzlonekGrupy;
use App\Entity\UzytkownikSystemu;
use App\Repository\UzytkownikSystemuRepository;

class UzytkownikSystemuService
{
    private $uzytkownikSystemuRepository;

    /**
     * UzytkownikSystemuRepository constructor.
     * @param $uzytkownikSystemuRepository
     */
    public function __construct(UzytkownikSystemuRepository $uzytkownikSystemuRepository)
    {
        $this->uzytkownikSystemuRepository = $uzytkownikSystemuRepository;
    }

    public function findByLogin($login)
    {
        return $this->uzytkownikSystemuRepository->findByLogin($login);
    }

    public function findById($id)
    {
        $uzytkownikSystemu = $this->uzytkownikSystemuRepository->findById($id);
        if(!empty($uzytkownikSystemu)){
            return $uzytkownikSystemu[0];
        }else{
            return $uzytkownikSystemu;
        }
    }

    public function findByOsobaId($osobaId)
    {
        $osoba = $this->uzytkownikSystemuRepository->findByOsobaId($osobaId);
        dump($osoba);
        if(!empty($osoba)){
            return $osoba[0];
        }else{
            return null;
        }
    }

    public function saveUzytkownikSystemu(UzytkownikSystemu $uzytkownikSystemu)
    {
        $this->uzytkownikSystemuRepository->saveUzytkownikSystemu($uzytkownikSystemu);
    }

    public function updateHaslo($uzytkownikSystemuId, $haslo)
    {
        $this->uzytkownikSystemuRepository->updateHaslo($uzytkownikSystemuId,$haslo);
    }

    public function updateAktywny($uzytkownikSystemuId, $aktywny)
    {
        $this->uzytkownikSystemuRepository->updateAktywny($uzytkownikSystemuId,$aktywny);
    }

    public function addUzytkownikToGrupa(CzlonekGrupy $czlonekGrupy)
    {
        $this->uzytkownikSystemuRepository->addUzytkownikToGrupa($czlonekGrupy);
    }

    public function removeUzytkownikFromGrupa($uzytkownikId,$grupaId)
    {
        $this->uzytkownikSystemuRepository->removeUzytkownikFromGrupa($uzytkownikId,$grupaId);
    }

    public function getEntityById($osobaId)
    {
        $uzytkownikSystemu = $this->uzytkownikSystemuRepository->getEntityById($osobaId);
        return empty($uzytkownikSystemu) ? null : $uzytkownikSystemu[0];
    }
}