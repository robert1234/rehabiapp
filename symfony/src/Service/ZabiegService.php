<?php


namespace App\Service;


use App\Entity\Zabieg;
use App\Repository\ZabiegRepository;

class ZabiegService
{
    private $zabiegRepository;

    /**
     * ZabiegService constructor.
     * @param $zabiegRepository
     */
    public function __construct(ZabiegRepository $zabiegRepository)
    {
        $this->zabiegRepository = $zabiegRepository;
    }


    public function getEntityById($id)
    {
        $zabieg = $this->zabiegRepository->getEntityById($id);
        return empty($zabieg) ? null : $zabieg[0];
    }

    public function getArrayById($id)
    {
        $zabieg = $this->zabiegRepository->getArrayById($id);
        return empty($zabieg) ? null : $zabieg[0];
    }

    public function addEntity(Zabieg $zabieg)
    {
        $this->zabiegRepository->addEntity($zabieg);
    }

    public function removeById($id)
    {
        $this->zabiegRepository->removeById($id);
    }

    public function update($zabiegId, Zabieg $zabieg)
    {
        $this->zabiegRepository->update($zabiegId, $zabieg);
    }

    public function findByOpisOrZalecenia($searchByValue)
    {
        return $this->zabiegRepository->findByOpisOrZalecenia($searchByValue);
    }
}