<?php


namespace App\Service;


use App\Entity\Rozpoznanie;
use App\Repository\RozpoznanieRepository;

class RozpoznanieService
{
    private $rozpoznanieRepository;

    /**
     * RozpoznanieService constructor.
     * @param $rozpoznanieRepository
     */
    public function __construct(RozpoznanieRepository $rozpoznanieRepository)
    {
        $this->rozpoznanieRepository = $rozpoznanieRepository;
    }

    public function addRozpoznanie($kod,$opis)
    {
        $rozpoznanie = new Rozpoznanie();
        $rozpoznanie->setKod($kod);
        $rozpoznanie->setOpis($opis);

        $this->rozpoznanieRepository->addRozpoznanie($rozpoznanie);
    }

    public function removeRozpoznanie($kod)
    {
        $this->rozpoznanieRepository->removeRozpoznanie($kod);
    }

    public function getRozpoznanieById($id)
    {
        $rozpoznanie = $this->rozpoznanieRepository->getRozpoznanieById($id);
        return empty($rozpoznanie) ? null : $rozpoznanie[0];
    }

    public function getAll()
    {
        return $this->rozpoznanieRepository->getAll();
    }

    public function getByKod($kod)
    {
        return $this->rozpoznanieRepository->getByKod($kod);
    }
}