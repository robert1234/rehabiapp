<?php


namespace App\Service;


use App\Entity\StatusWizyty;
use App\Repository\StatusWizytyRepository;
use Doctrine\ORM\AbstractQuery;

class StatusWizytyService
{
    private $statusWizytyRep;

    /**
     * StatusWizytyService constructor.
     * @param $statusWizytyRep
     */
    public function __construct(StatusWizytyRepository $statusWizytyRep)
    {
        $this->statusWizytyRep = $statusWizytyRep;
    }

    public function getArrayByNazwa($nazwa)
    {
        $status = $this->statusWizytyRep->getArrayByNazwa($nazwa);
        return empty($status) ? null : $status[0];
    }

    public function getArrayById($id)
    {
        $status = $this->statusWizytyRep->getArrayById($id);
        return empty($status) ? null : $status[0];
    }

    public function getEntityById($id)
    {
        return $this->statusWizytyRep->getEntityById($id);
    }

    public function getAll()
    {
        return $this->statusWizytyRep->getAll();
    }

}