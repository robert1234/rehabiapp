<?php


namespace App\Service;


use App\Entity\Etap;
use App\Repository\EtapPlannerRepository;

class PlannerService
{
    /** @var RelacjaMiedzyZabiegamiService  */
    private $relacjaMiedzyZabiegamiSrv;

    /** @var PozycjaPlanuService */
    private $pozycjaPlanuSrv;

    /** @var EtapService */
    private $etapSrv;

    /** @var EtapPlannerRepository */
    private $etapPlannerRep;

    /**
     * PlannerService constructor.
     * @param RelacjaMiedzyZabiegamiService $relacjaMiedzyZabiegamiSrv
     * @param PozycjaPlanuService $pozycjaPlanuSrv
     * @param EtapService $etapSrv
     * @param EtapPlannerRepository $etapPlannerRep
     */
    public function __construct(RelacjaMiedzyZabiegamiService $relacjaMiedzyZabiegamiSrv, PozycjaPlanuService $pozycjaPlanuSrv, EtapService $etapSrv, EtapPlannerRepository $etapPlannerRep)
    {
        $this->relacjaMiedzyZabiegamiSrv = $relacjaMiedzyZabiegamiSrv;
        $this->pozycjaPlanuSrv = $pozycjaPlanuSrv;
        $this->etapSrv = $etapSrv;
        $this->etapPlannerRep = $etapPlannerRep;
    }

    public function modifyEtap(array $etap)
    {
        $collisions = $this->checkZabiegCollisions($etap);
        if( empty($collisions['intervals']) && $collisions['timetable'] == 0 ) {
            $etapId = $etap['id'];
            $sala = $etap['sala'];
            $data = $etap['data'];
            $specjalista = $etap['specjalista'];
            $this->etapSrv->updateSala($etapId, $sala);
            $this->etapSrv->updateDataPrzeprowadzenia($etapId, $data);
            $this->etapSrv->updateSpecjalista($etapId, $specjalista);
            return ["result" => "success"];
        }
        return ["result" => "collisions_exists", "collisions" => $collisions];
    }

    public function checkZabiegCollisions($etapY) : array
    {
        $etapyZaplanowane = $this->etapSrv->findEtapWithFilledDataByTurnus($etapY['turnus']);
        dump($etapyZaplanowane);
        $intervals = $this->checkInterval($etapyZaplanowane,$etapY);
        $timetable = $this->checkTimetableCollision($etapY);
        return ['intervals' => $intervals, 'timetable' => $timetable];
    }

    public function checkInterval($previousEtapy, $etapY)
    {
        $result = [];

        $zabiegYId = (int)$etapY['zabieg'];
        foreach ($previousEtapy as $etapX) {
            $zabiegXId = $etapX->getZabieg()->getId();
            if($zabiegYId==$zabiegXId) {
                continue;
            }
            dump($zabiegYId);
            dump($zabiegXId);
            $relacja = $this->relacjaMiedzyZabiegamiSrv
                ->getRelacjaMiedzyZabiegami($zabiegXId,$zabiegYId);
            dump($relacja);
            $minimalInterval = is_null($relacja) ? 0 : $relacja->getMinimalnyOdstep();
            dump($minimalInterval);

            $dateFrom = strtotime($etapX->getDataPrzeprowadzenia()->format("Y-m-d h:i"));
            $dateTo = strtotime((new \DateTime($etapY['data']))->format("Y-m-d h:i"));

            $currentInterval = round(abs($dateTo - $dateFrom) / 60,2);
            dump($currentInterval);
            dump(($currentInterval - $minimalInterval));

            if( ($currentInterval - $minimalInterval) < 0) {
                array_push($result, [
                    'etapX' => $etapX->getId(),
                    'etapY' => $etapY['id'],
                    'status' => $minimalInterval
                ]);
            }
        }

        return $result;
    }

    public function checkTimetableCollision($etap)
    {
        return $this->etapPlannerRep->checkTimetableCollisions($etap['data'],$etap['sala'],$etap['specjalista'])[1];
    }

    public function getCurrentTimetableBySpecjalistaAndSala($specjalistaId, $salaId)
    {
        return $this->etapPlannerRep->getCurrentTimetableBySpecjalistaAndSala($specjalistaId, $salaId);
    }

    public function getFullTimetableBySpecjalistaAndSala($specjalistaId, $salaId)
    {
        return $this->etapPlannerRep->getFullTimetableBySpecjalistaAndSala($specjalistaId, $salaId);
    }

    public function getDayTimetableBySpecjalistaAndSala($specjalistaId, $salaId, $date)
    {
        return $this->etapPlannerRep->getDayTimetableBySpecjalistaAndSala($specjalistaId, $salaId, $date);
    }

    public function generateDayHourMap(){
        for ($i = 8; $i < 20; $i++) {
            for ($j = 0; $j < 6; $j++) {
                $result[$i.':'.$j.'0'] = ['etap' => '', 'hour' => $i.':'.$j.'0'];
            }
        }
        return $result;
    }
}