<?php


namespace App\Service;


use App\Entity\Osoba;

class OsobaService
{
    private $osobaRepository;

    /**
     * OsobaRepository constructor.
     * @param $osobaRepository
     */
    public function __construct(\App\Repository\OsobaRepository $osobaRepository)
    {
        $this->osobaRepository = $osobaRepository;
    }

    public function addOsoba(Osoba $osoba)
    {
        $this->osobaRepository->addOsoba($osoba);
    }

    public function updateOsoba(Osoba $osoba)
    {
        $this->osobaRepository->updateOsoba($osoba);
    }

    public function removeOsoba(Osoba $osoba)
    {
        $this->osobaRepository->removeOsoba($osoba);
    }

    public function selectAll()
    {
        return $this->osobaRepository->selectAll();
    }

    public function findByNameOrSurnameOrPesel($searchByValue)
    {
        return $this->osobaRepository->findByNameOrSurnameOrPesel($searchByValue);
    }

    public function findById($userId)
    {
        $osoba = $this->osobaRepository->findById($userId)[0];
        dump($osoba);
        $osoba['dataUrodzenia'] = $osoba['dataUrodzenia']->format("Y-m-d");
        $osoba['dataRejestracji'] = $osoba['dataRejestracji']->format("Y-m-d h:i:s");
        return $osoba;
    }

    public function findEntityById($userId)
    {
        return $this->osobaRepository->findEntityById($userId)[0];
    }

    public function updateOsobaFromJson($request, $id)
    {
        $this->osobaRepository->updateOsobaFromJson($request ,$id);
    }

    public function storeOsobaFromJson($request)
    {
        $osoba = $this->mapJsonToOsoba($request);
        $osoba->setDataRejestracji(new \DateTime());

        $this->addOsoba($osoba);
    }

    public function mapJsonToOsoba($request)
    {
        $osoba = new Osoba();
        $osoba->setImie($request['imie']);
        $osoba->setNazwisko($request['nazwisko']);
        $osoba->setPesel($request['pesel']);
        $osoba->setDataUrodzenia(new \DateTime($request['dataUrodzenia']));
        $osoba->setPlec($request['plec']);
        $osoba->setImieMatki($request['imieMatki']);
        $osoba->setImieOjca($request['imieOjca']);
        $osoba->setUlica($request['ulica']);
        $osoba->setNrDomu($request['nrDomu']);
        $osoba->setKodPocztowy($request['kodPocztowy']);
        $osoba->setMiejscowosc($request['miejscowosc']);
        return $osoba;
    }

}