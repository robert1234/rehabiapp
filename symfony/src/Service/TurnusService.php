<?php


namespace App\Service;


use App\Entity\StatusWizyty;
use App\Entity\Turnus;
use App\Repository\TurnusRepository;
use Doctrine\ORM\AbstractQuery;

class TurnusService
{
    private $turnusRep;
    private $planSrv;
    private $pacjentSrv;
    private $etapSrv;
    private $pozycjaPlanuSrv;

    /**
     * TurnusService constructor.
     * @param TurnusRepository $turnusRep
     * @param PlanService $planSrv
     * @param EtapService $etapSrv
     * @param PozycjaPlanuService $pozycjaPlanuSrv
     * @param PacjentService $pacjentSrv
     */
    public function __construct(TurnusRepository $turnusRep,
                                PlanService $planSrv,
                                EtapService $etapSrv,
                                PozycjaPlanuService $pozycjaPlanuSrv,
                                PacjentService $pacjentSrv)
    {
        $this->turnusRep = $turnusRep;
        $this->planSrv = $planSrv;
        $this->etapSrv = $etapSrv;
        $this->pozycjaPlanuSrv = $pozycjaPlanuSrv;
        $this->pacjentSrv = $pacjentSrv;
    }


    public function addEntity($pacjentId, $opis, $planId)
    {
        $turnus = new Turnus();
        $pacjent = $this->pacjentSrv->getEntityById($pacjentId);
        $plan = $this->planSrv->getEntityById($planId);
        $turnus->setPacjent($pacjent);
        $turnus->setOpis($opis);
        $turnus->setPlan($plan);
        return $this->turnusRep->addEntity($turnus);
    }

    public function getEntity($id)
    {
        $turnus = $this->turnusRep->getEntity($id);
        return empty($turnus) ? null : $turnus[0];
    }

    public function getArray($id)
    {
        $turnus = $this->turnusRep->getArray($id);
        return empty($turnus) ? null : $turnus[0];
    }

    public function getAll()
    {
        return $this->turnusRep->getAll();
    }

    public function initializeTurnus($pacjentId, $opis, $planId)
    {
        $turnus = $this->addEntity($pacjentId, $opis, $planId);
        $pozycjePlanu = $this->pozycjaPlanuSrv->findByPlan($planId);
        foreach ($pozycjePlanu as $pozycja) {
            $zabiegId = $pozycja->getZabieg()->getId();
            $this->etapSrv->addEntity($zabiegId,$turnus,StatusWizyty::ZAREJESTROWANO);
        }
    }

    public function findBySearchValue($searchValue)
    {
        return $this->turnusRep->findBySearchValue($searchValue);
    }

    public function findByPacjent($pacjentId)
    {
        return $this->turnusRep->findByPacjent($pacjentId);
    }

}