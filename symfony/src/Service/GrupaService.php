<?php


namespace App\Service;


use App\Repository\GrupaRepository;

class GrupaService
{
    private $grupaRepository;

    /**
     * GrupaService constructor.
     * @param $grupaRepository
     */
    public function __construct(GrupaRepository $grupaRepository)
    {
        $this->grupaRepository = $grupaRepository;
    }

    public function getGroupList()
    {
        return $this->grupaRepository->getGroupList();
    }

    public function getGrupyByUzytkownikId($uzytkownikSystemuId)
    {
        return $this->grupaRepository->getGrupyByUzytkownikId($uzytkownikSystemuId);
    }

    public function getGrupaById($id)
    {
        return $this->grupaRepository->getById($id)[0];
    }
}