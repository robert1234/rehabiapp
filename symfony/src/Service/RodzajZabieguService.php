<?php


namespace App\Service;


use App\Entity\RodzajZabiegu;
use App\Repository\RodzajZabieguRepository;

class RodzajZabieguService
{
    private $rodzajZabieguRepository;

    /**
     * RodzajZabieguService constructor.
     * @param $rodzajZabieguRepository
     */
    public function __construct(RodzajZabieguRepository $rodzajZabieguRepository)
    {
        $this->rodzajZabieguRepository = $rodzajZabieguRepository;
    }

    public function addRodzajZabiegu($nazwa)
    {
        $rodzajZabiegu = new RodzajZabiegu();
        $rodzajZabiegu->setNazwa($nazwa);
        $this->rodzajZabieguRepository->addRodzajZabiegu($rodzajZabiegu);
    }

    public function removeRodzajZabiegu($nazwa)
    {
        $this->rodzajZabieguRepository->removeRodzajZabiegu($nazwa);
    }

    public function getRodzajZabieguById($id)
    {
        $rodzajZabiegu = $this->rodzajZabieguRepository->getRodzajZabieguById($id);
        if(empty($rodzajZabiegu)) {
            return null;
        }else{
            return $rodzajZabiegu[0];
        }
    }

    public function getAll()
    {
        return $this->rodzajZabieguRepository->getAll();
    }

}