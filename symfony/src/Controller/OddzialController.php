<?php

namespace App\Controller;

use App\Service\OddzialService;
use App\Service\OsobaService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class OddzialController extends AbstractController
{
    private $oddzialService;

    /**
     * OddzialController constructor.
     * @param $oddzialService
     */
    public function __construct(OddzialService $oddzialService)
    {
        $this->oddzialService = $oddzialService;
    }

    /**
     * @Route("/oddzial", name="oddzial")
     * @IsGranted("ROLE_ODDZIALY_ODCZYT")
     */
    public function index()
    {
        $oddzialy = json_encode($this->oddzialService->getAll());
        return $this->render('oddzial/index.html.twig', [
            'oddzialy' => $oddzialy
        ]);
    }

    /**
     * @Route("/oddzial/new", name="oddzial-store", methods={"POST"})
     * @IsGranted("ROLE_ODDZIALY_ZAPIS")
     */
    public function storeOddzial(Request $request)
    {
        $oddzial = $request->request->get('nazwa');
        $this->oddzialService->addOddzial($oddzial);
        return new JsonResponse(['status' => 'success']);
    }

    /**
     * @Route("/oddzial/remove", name="oddzial-remove", methods={"POST"})
     * @IsGranted("ROLE_ODDZIALY_ZAPIS")
     */
    public function removeOddzial(Request $request)
    {
        $oddzial = $request->request->get('nazwa');
        $this->oddzialService->removeOddzial($oddzial);
        return new JsonResponse(['status' => 'success']);
    }

    /**
     * @Route("/oddzial/{oddzialId}/sale", name="sale")
     * @IsGranted("ROLE_SALE_ODCZYT")
     */
    public function getSale($oddzialId)
    {
        $sale = json_encode($this->oddzialService->getSalaByOddzial($oddzialId));
        $oddzialEntity = $this->oddzialService->getOddzialById($oddzialId);
        return $this->render('oddzial/sale.html.twig', [
            'sale' => $sale,
            'oddzial_id' => $oddzialId,
            'oddzial' => $oddzialEntity
        ]);
    }

    /**
     * @Route("/oddzial/edit/sala/store", name="sala-store", methods={"POST"})
     * @IsGranted("ROLE_SALE_ZAPIS")
     */
    public function storeSala(Request $request)
    {
        $nrSali = $request->request->get('nrSali');
        $oddzial = $request->request->get('oddzial');
        $this->oddzialService->addSala($oddzial,$nrSali);
        return new JsonResponse(['status' => 'success']);
    }

    /**
     * @Route("/oddzial/edit/sala/remove", name="sala-remove", methods={"POST"})
     * @IsGranted("ROLE_SALE_ZAPIS")
     */
    public function removeSala(Request $request)
    {
        $nrSali = $request->request->get('nrSali');
        $oddzial = $request->request->get('oddzial');
        $this->oddzialService->removeSala($oddzial,$nrSali);
        return new JsonResponse(['status' => 'success']);
    }
}
