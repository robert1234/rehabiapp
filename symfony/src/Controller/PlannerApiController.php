<?php


namespace App\Controller;

use App\Entity\StatusWizyty;
use App\Service\EtapService;
use App\Service\PacjentService;
use App\Service\TurnusService;
use App\Service\UzytkownikSystemuService;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PlannerApiController extends AbstractController
{
    /** @var EtapService */
    private $etapSrv;

    /** @var PacjentService */
    private $pacjentSrv;

    /** @var TurnusService */
    private $turnusySrv;

    /** @var UzytkownikSystemuService */
    private $uzytkownikSystemuSrv;

    /**
     * PlannerApiController constructor.
     * @param EtapService $etapSrv
     * @param PacjentService $pacjentSrv
     * @param TurnusService $turnusySrv
     * @param UzytkownikSystemuService $uzytkownikSystemuSrv
     */
    public function __construct(EtapService $etapSrv, PacjentService $pacjentSrv, TurnusService $turnusySrv, UzytkownikSystemuService $uzytkownikSystemuSrv)
    {
        $this->etapSrv = $etapSrv;
        $this->pacjentSrv = $pacjentSrv;
        $this->turnusySrv = $turnusySrv;
        $this->uzytkownikSystemuSrv = $uzytkownikSystemuSrv;
    }

    /**
     * @Route("/api/etap/modify/qrcode", name="api-modify-etap-qrcode", methods={"POST"})
     * @IsGranted("ROLE_ETAPY_ZAPIS")
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function modifyEtapByQrCode(Request $request)
    {
        $login = $request->headers->get('X-AUTH-LOGIN');
        $etapId = $request->request->get('etap_id');
        $validationCode = $request->request->get('validation_code');

        if($this->etapSrv->validateCode($etapId,$validationCode,$login)) {
            $this->etapSrv->updateDataPrzeprowadzenia($etapId,(new \DateTime())->format("Y-m-d H:i:s"));
            $this->etapSrv->updateStatusWizyty($etapId,StatusWizyty::ODBYTO);
            return new JsonResponse(["message"=>"success"],200);
        } else {
            return new JsonResponse(["message"=>"bad_code_or_user"],200);
        }
    }

    /**
     * @Route("/api/etap/list", name="api-planner-etap-list", methods={"GET"})
     * @IsGranted("ROLE_ETAPY_ODCZYT")
     * @param Request $request
     * @return JsonResponse
     */
    public function listEtap(Request $request)
    {
        $login = $request->headers->get('X-AUTH-LOGIN');
        $uzytkownikSystemu = $this->uzytkownikSystemuSrv->findByLogin($login);
        $currentPacjentRole = $this->pacjentSrv->getCurrentByUzytkownikSystemu($uzytkownikSystemu->getId());
        $turnusy = $this->turnusySrv->findByPacjent($currentPacjentRole['id']);
        foreach ($turnusy as $turnus)
        {
            $etapy = $this->etapSrv->findEtapByTurnus($turnus->getId());
            return new JsonResponse(["message"=>"success","data"=>$etapy],200);
        }
    }
}