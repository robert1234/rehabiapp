<?php

namespace App\Controller;

use App\Repository\UzytkownikSystemuRepository;
use App\Service\OsobaService;
use App\Service\UzytkownikSystemuService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class OsobaController extends AbstractController
{
    private $osobaService;
    private $uzytkownikSystemuService;

    /**
     * OsobaController constructor.
     * @param OsobaService $osobaService
     * @param UzytkownikSystemuService $uzytkownikSystemuService
     */
    public function __construct(OsobaService $osobaService, UzytkownikSystemuService $uzytkownikSystemuService)
    {
        $this->osobaService = $osobaService;
        $this->uzytkownikSystemuService = $uzytkownikSystemuService;
    }

    /**
     * @IsGranted("ROLE_OSOBY_ODCZYT")
     * @Route("/osoba/search", name="osoba-search", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function osobaSearch(Request $request)
    {
        $searchByValue = $request->request->get('searchByValue');
        $osoby = $this->osobaService->findByNameOrSurnameOrPesel($searchByValue);
        dump($osoby);
        return new JsonResponse($osoby);
    }

    /**
     * @Route("/osoba", name="osoba", methods={"GET"})
     * @IsGranted("ROLE_OSOBY_ODCZYT")
     */
    public function osobaList()
    {
        return $this->render('osoba/osoba.html.twig', []);
    }

    /**
     * @Route("/osoba/{id}/edit", name="osoba-edit", methods={"GET"})
     * @IsGranted("ROLE_OSOBY_ZAPIS")
     */
    public function osobaEdit($id)
    {
        $osoba = $this->osobaService->findById($id);
        $osobaJson = json_encode($osoba, true);
        $uzytkownikSystemu = $this->uzytkownikSystemuService->getEntityById($id);
        dump($uzytkownikSystemu);
        return $this->render('osoba/osoba.manage.html.twig', [
            'osoba' => $osobaJson,
            'id' => $id,
            'uzytkownik_systemu' => $uzytkownikSystemu,
            'title' => 'Edycja osoby'
        ]);
    }

    /**
     * @Route("/osoba/new", name="osoba-new", methods={"GET"})
     * @IsGranted("ROLE_OSOBY_ZAPIS")
     */
    public function osobaNew()
    {
        return $this->render('osoba/osoba.manage.html.twig', [
            'title' => 'Nowa osoba'
        ]);
    }

    /**
     * @Route("/osoba/new", name="osoba-new-store", methods={"POST"})
     * @IsGranted("ROLE_OSOBY_ZAPIS")
     */
    public function osobaNewStore(Request $request)
    {
        $this->osobaService->storeOsobaFromJson($request->request->get('osoba'));
        return new JsonResponse();
    }

    /**
     * @IsGranted("ROLE_OSOBY_ZAPIS")
     * @Route("/osoba/{id}/edit", name="osoba-update", methods={"POST"})
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function osobaUpdate(Request $request, $id)
    {
        $this->osobaService->updateOsobaFromJson($request->request->get('osoba'), $id);
        return new JsonResponse();
    }
}
