<?php

namespace App\Controller;

use App\Entity\RodzajZabiegu;
use App\Service\RodzajZabieguService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class RodzajZabieguController extends AbstractController
{
    private $rodzajZabieguService;

    /**
     * RodzajZabieguController constructor.
     * @param $rodzajZabieguService
     */
    public function __construct(RodzajZabieguService $rodzajZabieguService)
    {
        $this->rodzajZabieguService = $rodzajZabieguService;
    }


    /**
     * @IsGranted("ROLE_ZABIEGI_ODCZYT")
     * @Route("/zabieg/rodzaj", name="zabieg-rodzaj")
     */
    public function index()
    {
        $rodzajZabiegu = json_encode($this->rodzajZabieguService->getAll());
        return $this->render('rodzaj_zabiegu/index.html.twig', [
            'items' => $rodzajZabiegu
        ]);
    }

    /**
     * @IsGranted("ROLE_ZABIEGI_ZAPIS")
     * @Route("/zabieg/rodzaj/new", name="zabieg-rodzaj-store", methods={"POST"})
     */
    public function storeRodzajZabiegu(Request $request)
    {
        $nazwa = $request->request->get('nazwa');
        $this->rodzajZabieguService->addRodzajZabiegu($nazwa);
        return new JsonResponse(['status' => 'success']);
    }

    /**
     * @IsGranted("ROLE_ZABIEGI_ZAPIS")
     * @Route("/zabieg/rodzaj/remove", name="zabieg-rodzaj-remove", methods={"POST"})
     */
    public function removeRodzajZabiegu(Request $request)
    {
        $nazwa = $request->request->get('nazwa');
        $this->rodzajZabieguService->removeRodzajZabiegu($nazwa);
        return new JsonResponse(['status' => 'success']);
    }
}
