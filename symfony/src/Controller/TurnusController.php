<?php

namespace App\Controller;

use App\Service\EtapService;
use App\Service\PacjentService;
use App\Service\PlanService;
use App\Service\TurnusService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class TurnusController extends AbstractController
{
    /**
     * @var TurnusService
     */
    private $turnusSrv;

    /**
     * @var EtapService
     */
    private $etapSrv;

    /**
     * @var PacjentService
     */
    private $pacjentSrv;

    /**
     * @var PlanService
     */
    private $planSrv;

    /**
     * TurnusController constructor.
     * @param TurnusService $turnusSrv
     * @param EtapService $etapSrv
     * @param PacjentService $pacjentSrv
     * @param PlanService $planSrv
     */
    public function __construct(TurnusService $turnusSrv, EtapService $etapSrv, PacjentService $pacjentSrv, PlanService $planSrv)
    {
        $this->turnusSrv = $turnusSrv;
        $this->etapSrv = $etapSrv;
        $this->pacjentSrv = $pacjentSrv;
        $this->planSrv = $planSrv;
    }

    /**
     * @IsGranted("ROLE_TURNUSY_ODCZYT")
     * @Route("/turnus/search", name="turnus-search", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function search(Request $request)
    {
        $searchValue = $request->request->get('searchByValue');
        $turnusy = $this->turnusSrv->findBySearchValue($searchValue);
        return new JsonResponse($turnusy);
    }

    /**
     * @IsGranted("ROLE_TURNUSY_ODCZYT")
     * @Route("/turnus", name="turnus")
     */
    public function index()
    {
        return $this->render('turnus/list.html.twig', []);
    }

    /**
     * @IsGranted("ROLE_TURNUSY_ZAPIS")
     * @Route("/turnus/new", name="turnus-new", methods={"GET"})
     */
    public function createTurnus()
    {
        $pacjenci = $this->pacjentSrv->getAll();
        return $this->render('turnus/turnus.manage.html.twig', [
            'pacjenci' => json_encode($pacjenci)
        ]);
    }

    /**
     * @IsGranted("ROLE_TURNUSY_ZAPIS")
     * @Route("/turnus/new", name="turnus-new-store", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function createTurnusStore(Request $request)
    {
        $turnus = $request->request->get('turnus');
        $this->turnusSrv->initializeTurnus($turnus['pacjent'],$turnus['opis'],$turnus['plan']);
        return new JsonResponse(['status'=>'success']);
    }

}
