<?php

namespace App\Controller;

use App\Service\OsobaService;
use App\Service\PacjentService;
use App\Service\RozpoznanieService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PacjentController extends AbstractController
{
    private  $osobaSrv;
    private $pacjentSrv;
    private $rozpozaniaSrv;

    /**
     * PacjentController constructor.
     * @param $osobaSrv
     * @param $pacjentSrv
     * @param $rozpozaniaSrv
     */
    public function __construct(OsobaService $osobaSrv,
                                PacjentService $pacjentSrv,
                                RozpoznanieService $rozpozaniaSrv)
    {
        $this->osobaSrv = $osobaSrv;
        $this->pacjentSrv = $pacjentSrv;
        $this->rozpozaniaSrv = $rozpozaniaSrv;
    }


    /**
     * @IsGranted("ROLE_PACJENCI_ODCZYT")
     * @Route("/osoba/pacjent/search", name="osoba-pacjent-search")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function search(Request $request)
    {
        $active = $request->request->get("active");
        dump($active);
        // $searchByValue = $request->request->get("searchByValue"); //In future use only
        // $items = ($active == true) ? $this->pacjentSrv->getCurrent() : $this->pacjentSrv->getAll(); //Not working
        if($active == "true") {
            $items = $this->pacjentSrv->getCurrent();
        } else {
            $items = $this->pacjentSrv->getAll();
        }
        dump($items);
        return new JsonResponse($items);
    }

    /**
     * @IsGranted("ROLE_PACJENCI_ODCZYT")
     * @Route("/osoba/pacjent", name="osoba-pacjent")
     */
    public function index()
    {
        return $this->render('pacjent/list.html.twig', []);
    }

    /**
     * @IsGranted("ROLE_PACJENCI_ZAPIS")
     * @Route("/osoba/pacjent/new", name="osoba-pacjent-new", methods={"GET"})
     */
    public function pacjentNew()
    {
        $osoby = $this->osobaSrv->findByNameOrSurnameOrPesel("");
        $rozpoznania = $this->rozpozaniaSrv->getAll();
        return $this->render('pacjent/manage.html.twig', [
            'osoby' => json_encode($osoby),
            'rozpoznania' => json_encode($rozpoznania),
            'title' => 'Nowy pacjent',
        ]);
    }

    /**
     * @IsGranted("ROLE_PACJENCI_ZAPIS")
     * @Route("/osoba/pacjent/edit/{pacjentId}", name="osoba-pacjent-edit", methods={"GET"})
     */
    public function pacjentEdit($pacjentId)
    {
        $osoby = $this->osobaSrv->findByNameOrSurnameOrPesel("");
        $rozpoznania = $this->rozpozaniaSrv->getAll();
        $pacjent = $this->pacjentSrv->getArrayById($pacjentId);
        dump($pacjent);
        return $this->render('pacjent/manage.html.twig', [
            'osoby' => json_encode($osoby),
            'rozpoznania' => json_encode($rozpoznania),
            'item' => json_encode($pacjent),
            'id' => $pacjentId,
            'title' => 'Edycja danych pacjenta',
        ]);
    }

    /**
     * @IsGranted("ROLE_PACJENCI_ZAPIS")
     * @Route("/osoba/pacjent/new", name="osoba-pacjent-new-store", methods={"POST"})
     */
    public function pacjentEditStore(Request $request)
    {
        $pacjent = $request->request->get("pacjent");
        $this->pacjentSrv->add($pacjent['osoba'],
            $pacjent['rozpoznanie'],
            $pacjent['platnik'],
            $pacjent['opisDodatkowy']);
        return new JsonResponse(['status' => 'succes']);
    }

    /**
     * @IsGranted("ROLE_PACJENCI_ZAPIS")
     * @Route("/osoba/pacjent/edit/{pacjentId}", name="osoba-pacjent-edit-store", methods={"POST"})
     */
    public function pacjentEditStoreSave(Request $request, $pacjentId)
    {
        $pacjent = $request->request->get("pacjent");
        $this->pacjentSrv->update($pacjentId,
            $pacjent['platnik'],
            $pacjent['rozpoznanie'],
            $pacjent['opisDodatkowy']);
        return new JsonResponse(['status' => 'succes']);
    }
}
