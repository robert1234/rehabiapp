<?php

namespace App\Controller;

use App\Entity\CzlonekGrupy;
use App\Entity\UzytkownikSystemu;
use App\Service\GrupaService;
use App\Service\OsobaService;
use App\Service\UzytkownikSystemuService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UzytkownikSystemuController extends AbstractController
{
    private $osobaService;
    private $uzytkownikSystemuService;
    private $grupaService;

    /**
     * UzytkownikSystemuController constructor.
     * @param OsobaService $osobaService
     * @param UzytkownikSystemuService $uzytkownikSystemuService
     * @param GrupaService $grupaService
     */
    public function __construct(OsobaService $osobaService, UzytkownikSystemuService $uzytkownikSystemuService, GrupaService $grupaService)
    {
        $this->osobaService = $osobaService;
        $this->uzytkownikSystemuService = $uzytkownikSystemuService;
        $this->grupaService = $grupaService;
    }

    /**
     * @IsGranted("ROLE_UZYTKOWNICY_SYSTEMU_ODCZYT")
     * @Route("/uzytkownik-systemu", name="uzytkownik-systemu", methods={"GET"})
     */
    public function uzytkownikSystemuList()
    {
        return new JsonResponse('');
    }

    /**
     * @IsGranted("ROLE_UZYTKOWNICY_SYSTEMU_ZAPIS")
     * @Route("/uzytkownik-systemu/new/user/{userId}", name="uzytkownik-systemu-new", methods={"GET"})
     * @param int $userId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newUzytkownikSystemu($userId = null)
    {
        $osoby = json_encode($this->osobaService->findByNameOrSurnameOrPesel(''));
        $grupy = json_encode($this->grupaService->getGroupList());
        $osoba = $this->osobaService->findById($userId);
        $proponedLogin = strtolower($osoba['imie'].'.'.$osoba['nazwisko']);
        return $this->render('uzytkownik_systemu/uzytkownik.systemu.manage.html.twig', [
            'proponed_login' => $proponedLogin,
            'is_update' => false,
            'user_id' => $userId,
            'osoby' => $osoby,
            'grupy' => $grupy,
            'title' => 'Nowy Użytkownik'
        ]);
    }

    /**
     * @IsGranted("ROLE_UZYTKOWNICY_SYSTEMU_ZAPIS")
     * @Route("/uzytkownik-systemu/new", name="uzytkownik-systemu-new-store,", methods={"POST"})
     */
    public function newUzytkownikSystemuSave(Request $request)
    {
        $uzytkownikSystemu = $request->request->get('uzytkownik_systemu');
        $listOfGrupy = $request->request->get('czlonkowie_grup');

        $osoba = $this->osobaService->findEntityById($uzytkownikSystemu['osoba']);
        dump($osoba);

        $uzytkownikSystemuEntity = new UzytkownikSystemu();
        $uzytkownikSystemuEntity->setLogin($uzytkownikSystemu['login']);
        $uzytkownikSystemuEntity->setHaslo(password_hash($uzytkownikSystemu['haslo'],PASSWORD_BCRYPT));
        $uzytkownikSystemuEntity->setAktywny(true);
        $uzytkownikSystemuEntity->setOsoba($osoba);

        $this->uzytkownikSystemuService->saveUzytkownikSystemu($uzytkownikSystemuEntity);
        dump($uzytkownikSystemu);
        foreach($listOfGrupy as $grupaId) {
            $grupa = $this->grupaService->getGrupaById($grupaId);
            $czlonekGrupyEntity = new CzlonekGrupy();
            $czlonekGrupyEntity->setGrupa($grupa);
            $czlonekGrupyEntity->setUzytkownikSystemu($uzytkownikSystemuEntity);
            dump($czlonekGrupyEntity);
            $this->uzytkownikSystemuService->addUzytkownikToGrupa($czlonekGrupyEntity);
        }
        return new JsonResponse(['status' => 'success']);
    }

    /**
     * @IsGranted("ROLE_UZYTKOWNICY_SYSTEMU_ZAPIS")
     * @Route("/uzytkownik-systemu/{uzytkownikSystemuId}/edit", name="uzytkownik-systemu-edit", methods={"GET"})
     * @param $uzytkownikSystemuId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editUzytkownikSystemu($uzytkownikSystemuId)
    {
        $osoby = json_encode($this->osobaService->findByNameOrSurnameOrPesel(''));
        $uzytkownikSystemu = json_encode($this->uzytkownikSystemuService->findById($uzytkownikSystemuId));
        $grupy = json_encode($this->grupaService->getGroupList());
        $grupyUzytkownika = json_encode($this->grupaService->getGrupyByUzytkownikId($uzytkownikSystemuId));
        return $this->render('uzytkownik_systemu/uzytkownik.systemu.manage.html.twig', [
            'is_update' => true,
            'osoby' => $osoby,
            'grupy' => $grupy,
            'uzytkownik_systemu' => $uzytkownikSystemu,
            'grupy_uzytkownika' => $grupyUzytkownika,
            'id' => $uzytkownikSystemuId,
            'title' => 'Edycja Użytkownika'
        ]);
    }

    // TODO: Dodać kontroller dla ustawień konta, a także voter do niego.
}
