<?php


namespace App\Controller;

use App\Service\EtapService;
use App\Service\OddzialService;
use App\Service\PlannerService;
use App\Service\PozycjaPlanuService;
use App\Service\RelacjaMiedzyZabiegamiService;
use App\Service\SpecjalistaService;
use App\Service\TurnusService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PlannerController extends AbstractController
{
    /** @var RelacjaMiedzyZabiegamiService  */
    private $relacjaMiedzyZabiegamiSrv;

    /** @var PozycjaPlanuService */
    private $pozycjaPlanuSrv;

    /** @var EtapService */
    private $etapSrv;

    /** @var PlannerService */
    private $plannerSrv;

    /** @var TurnusService */
    private $turnusSrv;

    /** @var SpecjalistaService */
    private $specjalistaSrv;

    /** @var OddzialService */
    private $oddzialSrv;

    /**
     * PlannerController constructor.
     * @param RelacjaMiedzyZabiegamiService $relacjaMiedzyZabiegamiSrv
     * @param PozycjaPlanuService $pozycjaPlanuSrv
     * @param EtapService $etapSrv
     * @param PlannerService $plannerSrv
     * @param TurnusService $turnusSrv
     * @param SpecjalistaService $specjalistaSrv
     * @param OddzialService $oddzialSrv
     */
    public function __construct(RelacjaMiedzyZabiegamiService $relacjaMiedzyZabiegamiSrv,
                                PozycjaPlanuService $pozycjaPlanuSrv,
                                EtapService $etapSrv,
                                PlannerService $plannerSrv,
                                TurnusService $turnusSrv,
                                SpecjalistaService $specjalistaSrv,
                                OddzialService $oddzialSrv)
    {
        $this->relacjaMiedzyZabiegamiSrv = $relacjaMiedzyZabiegamiSrv;
        $this->pozycjaPlanuSrv = $pozycjaPlanuSrv;
        $this->etapSrv = $etapSrv;
        $this->plannerSrv = $plannerSrv;
        $this->turnusSrv = $turnusSrv;
        $this->specjalistaSrv = $specjalistaSrv;
        $this->oddzialSrv = $oddzialSrv;
    }

    /**
     * @IsGranted("ROLE_ETAPY_ZAPIS")
     * @Route("/etap/modify", name="etap-modify", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function modifyEtap(Request $request)
    {
        $etap = $request->request->get('etap');
        $result = $this->plannerSrv->modifyEtap($etap);
        return new JsonResponse($result);
    }

    /**
     * @IsGranted("ROLE_ETAPY_ODCZYT")
     * @Route("/etap/get", name="etap-modify-data", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function loadEtap(Request $request)
    {
        $etapId = $request->request->get('etap');
        $etap = $this->etapSrv->getArrayById($etapId);
        return new JsonResponse($etap);
    }

    /**
     * @IsGranted("ROLE_ETAPY_ODCZYT")
     * @Route("/timetable/full", name="etap-timetable-full", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function loadTimetableFull(Request $request)
    {
        $sala = $request->request->get('sala');
        $spec = $request->request->get('specjalista');
        $timetable = $this->plannerSrv->getFullTimetableBySpecjalistaAndSala($spec, $sala);
        return new JsonResponse($timetable);
    }

    /**
     * @IsGranted("ROLE_ETAPY_ODCZYT")
     * @Route("/timetable/current", name="etap-timetable-current", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function loadTimetable(Request $request)
    {
        $sala = $request->request->get('sala');
        $spec = $request->request->get('specjalista');
        $timetable = $this->plannerSrv->getCurrentTimetableBySpecjalistaAndSala($spec, $sala);
        return new JsonResponse($timetable);
    }

    /**
     * @IsGranted("ROLE_ETAPY_ODCZYT")
     * @Route("/timetable/day", name="etap-timetable-day", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function loadTimetableDay(Request $request)
    {
        $sala = $request->request->get('sala');
        $spec = $request->request->get('specjalista');
        $data = $request->request->get('data');
        $timetable = $this->plannerSrv->getDayTimetableBySpecjalistaAndSala($spec, $sala, $data);
        return new JsonResponse($timetable);
    }

    /**
     * @IsGranted("ROLE_TURNUSY_ZAPIS")
     * @Route("/turnus/manage/{turnusId}", name="turnus-manage", methods={"GET"})
     */
    public function index($turnusId)
    {
        $turnus = $this->turnusSrv->getArray($turnusId);
        $etapy = $this->etapSrv->findEtapByTurnus($turnusId);
        $specjalisci = $this->specjalistaSrv->getAll();
        $sale = $this->oddzialSrv->getAllSale();
        dump($etapy);
        return $this->render('planner/manage.html.twig', [
            'id' => $turnusId,
            'turnus' =>  json_encode($turnus),
            'etapy' => json_encode($etapy),
            'specjalisci' => json_encode($specjalisci),
            'sale' => json_encode($sale),
            'dayHourMap' => json_encode($this->plannerSrv->generateDayHourMap()),
        ]);
    }

}