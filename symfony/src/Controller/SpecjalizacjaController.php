<?php

namespace App\Controller;

use App\Entity\Specjalizacja;
use App\Service\SpecjalizacjaService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SpecjalizacjaController extends AbstractController
{
    private $specjalizacjaService;

    /**
     * SpecjalizacjaController constructor.
     * @param $specjalizacjaService
     */
    public function __construct(SpecjalizacjaService $specjalizacjaService)
    {
        $this->specjalizacjaService = $specjalizacjaService;
    }


    /**
     * @IsGranted("ROLE_SPECJALIZACJE_ODCZYT")
     * @Route("/specjalista/specjalizacja", name="specjalista-specjalizacja")
     */
    public function index()
    {
        $specjalizacja = json_encode($this->specjalizacjaService->getAll());
        return $this->render('specjalizacja/index.html.twig', [
            'items' => $specjalizacja
        ]);
    }

    /**
     * @IsGranted("ROLE_SPECJALIZACJE_ZAPIS")
     * @Route("/specjalista/specjalizacja/new", name="specjalista-specjalizacja-store", methods={"POST"})
     */
    public function storeSpecjalizacja(Request $request)
    {
        $nazwa = $request->request->get('nazwa');
        $this->specjalizacjaService->addSpecjalizacja($nazwa);
        return new JsonResponse(['status' => 'success']);
    }

    /**
     * @IsGranted("ROLE_SPECJALIZACJE_ZAPIS")
     * @Route("/specjalista/specjalizacja/remove", name="specjalista-specjalizacja-remove", methods={"POST"})
     */
    public function removeSpecjalizacja(Request $request)
    {
        $nazwa = $request->request->get('nazwa');
        $this->specjalizacjaService->removeSpecjalizacja($nazwa);
        return new JsonResponse(['status' => 'success']);
    }
}
