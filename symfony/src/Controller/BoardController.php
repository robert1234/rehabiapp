<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class BoardController extends AbstractController
{
    /**
     * @Route("/", name="board")
     * @IsGranted("IS_AUTHENTICATED_REMEMBERED")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function index()
    {
        return $this->render('board/index.html.twig', [
            'controller_name' => 'BoardController',
        ]);
    }

    /**
     * @Route("/api/check", name="api-check")
     * @IsGranted("IS_AUTHENTICATED_REMEMBERED")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     * @return JsonResponse
     */
    public function helloSignal()
    {
        return new JsonResponse(["message"=>"hello"],200);
    }
}
