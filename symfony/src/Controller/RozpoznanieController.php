<?php

namespace App\Controller;

use App\Service\OddzialService;
use App\Service\RozpoznanieService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class RozpoznanieController extends AbstractController
{
    private $rozpoznanieService;

    /**
     * RozpoznanieController constructor.
     * @param $rozpoznanieService
     */
    public function __construct(RozpoznanieService $rozpoznanieService)
    {
        $this->rozpoznanieService = $rozpoznanieService;
    }

    /**
     * @IsGranted("ROLE_ROZPOZNANIA_ODCZYT")
     * @Route("/rozpoznanie", name="rozpoznanie")
     */
    public function index()
    {
        $rozpoznania = json_encode($this->rozpoznanieService->getAll());
        return $this->render('rozpoznanie/index.html.twig', [
            'items' => $rozpoznania
        ]);
    }

    /**
     * @IsGranted("ROLE_ROZPOZNANIA_ZAPIS")
     * @Route("/rozpoznanie/new", name="rozpoznanie-store", methods={"POST"})
     */
    public function storeRozpoznanie(Request $request)
    {
        $kod = $request->request->get('kod');
        $opis = $request->request->get('opis');
        $this->rozpoznanieService->addRozpoznanie($kod,$opis);
        return new JsonResponse(['status' => 'success']);
    }

    /**
     * @IsGranted("ROLE_ROZPOZNANIA_ZAPIS")
     * @Route("/rozpoznanie/remove", name="rozpoznanie-remove", methods={"POST"})
     */
    public function removeRozpoznanie(Request $request)
    {
        $kod = $request->request->get('kod');
        $this->rozpoznanieService->removeRozpoznanie($kod);
        return new JsonResponse(['status' => 'success']);
    }
}
