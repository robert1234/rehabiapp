<?php

namespace App\Controller;

use App\Service\OddzialService;
use App\Service\OsobaService;
use App\Service\SpecjalistaService;
use App\Service\SpecjalizacjaService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SpecjalistaController extends AbstractController
{
    private $osobaSrv;
    private $specjalistaSrv;
    private $specjalizacjaSrv;
    private $oddzialSrv;

    /**
     * SpecjalistaController constructor.
     * @param OsobaService $osobaSrv
     * @param SpecjalizacjaService $specjalizacjaSrv
     * @param OddzialService $oddzialSrv
     * @param SpecjalistaService $specjalistaSrv
     */
    public function __construct(OsobaService $osobaSrv,
                                SpecjalizacjaService $specjalizacjaSrv,
                                OddzialService $oddzialSrv,
                                SpecjalistaService $specjalistaSrv )
    {
        $this->osobaSrv = $osobaSrv;
        $this->specjalizacjaSrv = $specjalizacjaSrv;
        $this->oddzialSrv = $oddzialSrv;
        $this->specjalistaSrv = $specjalistaSrv;
    }

    /**
     * @IsGranted("ROLE_SPECJALISCI_ODCZYT")
     * @Route("/osoba/specjalista/search", name="osoba-specjalista-search")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function search(Request $request)
    {
        $active = $request->request->get("active");
        dump($active);
        // $searchByValue = $request->request->get("searchByValue"); //In future use only
        // $items = ($active == true) ? $this->specjalistaSrv->getCurrent() : $this->specjalistaSrv->getAll(); //Not working
        if($active == "true") {
            $items = $this->specjalistaSrv->getCurrent();
        } else {
            $items = $this->specjalistaSrv->getAll();
        }
        dump($items);
        return new JsonResponse($items);
    }

    /**
     * @IsGranted("ROLE_SPECJALISCI_ODCZYT")
     * @Route("/osoba/specjalista", name="osoba-specjalista")
     */
    public function index()
    {
        return $this->render('specjalista/list.html.twig', []);
    }

    /**
     * @IsGranted("ROLE_SPECJALISCI_ZAPIS")
     * @Route("/osoba/specjalista/new", name="osoba-specjalista-new", methods={"GET"})
     */
    public function specjalistaNew()
    {
        $osoby = $this->osobaSrv->findByNameOrSurnameOrPesel("");
        $specjalizacje = $this->specjalizacjaSrv->getAll();
        $oddzialy = $this->oddzialSrv->getAll();
        return $this->render('specjalista/manage.html.twig', [
            'osoby' => json_encode($osoby),
            'specjalizacje' => json_encode($specjalizacje),
            'oddzialy' => json_encode($oddzialy),
            'title' => 'Nowy specjalista'
        ]);
    }

    /**
     * @IsGranted("ROLE_SPECJALISCI_ZAPIS")
     * @Route("/osoba/specjalista/edit/{specjalistaId}", name="osoba-specjalista-edit", methods={"GET"})
     */
    public function specjalistaEdit($specjalistaId)
    {
        $osoby = $this->osobaSrv->findByNameOrSurnameOrPesel("");
        $specjalizacje = $this->specjalizacjaSrv->getAll();
        $oddzialy = $this->oddzialSrv->getAll();
        $specjalista = $this->specjalistaSrv->getArrayById($specjalistaId);
        dump($specjalista);
        return $this->render('specjalista/manage.html.twig', [
            'osoby' => json_encode($osoby),
            'specjalizacje' => json_encode($specjalizacje),
            'oddzialy' => json_encode($oddzialy),
            'item' => json_encode($specjalista),
            'id' => $specjalistaId,
            'title' => 'Edycja danych specjalisty'
        ]);
    }

    /**
     * @IsGranted("ROLE_SPECJALISCI_ZAPIS")
     * @Route("/osoba/specjalista/new", name="osoba-specjalista-new-store", methods={"POST"})
     */
    public function specjalistaEditStore(Request $request)
    {
        $specjalista = $request->request->get("specjalista");
        dump($specjalista);
        $this->specjalistaSrv->add($specjalista['osoba'],
            $specjalista['oddzial'],
            $specjalista['specjalizacja'],
            $specjalista['opisDodatkowy']);
        return new JsonResponse(['status' => 'succes']);
    }

    /**
     * @IsGranted("ROLE_SPECJALISCI_ZAPIS")
     * @Route("/osoba/specjalista/edit/{specjalistaId}", name="osoba-specjalista-edit-store", methods={"POST"})
     */
    public function specjalistaEditStoreSave(Request $request, $specjalistaId)
    {
        $specjalista = $request->request->get("specjalista");
        $this->specjalistaSrv->update($specjalistaId,
            $specjalista['oddzial'],
            $specjalista['specjalizacja'],
            $specjalista['opisDodatkowy']);
        return new JsonResponse(['status' => 'succes']);
    }

}
