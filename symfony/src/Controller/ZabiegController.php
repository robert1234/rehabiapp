<?php

namespace App\Controller;

use App\Entity\Zabieg;
use App\Service\RelacjaMiedzyZabiegamiService;
use App\Service\RodzajZabieguService;
use App\Service\ZabiegService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncode;

class ZabiegController extends AbstractController
{
    private $zabiegService;
    private $rodzajZabieguService;
    private $relacjaMiedzyZabiegamiService;

    /**
     * ZabiegController constructor.
     * @param ZabiegService $zabiegService
     * @param RodzajZabieguService $rodzajZabieguService
     * @param RelacjaMiedzyZabiegamiService $relacjaMiedzyZabiegamiService
     */
    public function __construct(ZabiegService $zabiegService,
                                RodzajZabieguService $rodzajZabieguService,
                                RelacjaMiedzyZabiegamiService $relacjaMiedzyZabiegamiService)
    {
        $this->zabiegService = $zabiegService;
        $this->rodzajZabieguService = $rodzajZabieguService;
        $this->relacjaMiedzyZabiegamiService = $relacjaMiedzyZabiegamiService;
    }

    /**
     * @IsGranted("ROLE_ZABIEGI_ODCZYT")
     * @Route("/zabieg/search", name="zabieg-search", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function zabiegSearch(Request $request)
    {
        $searchByValue = $request->request->get('searchByValue');
        $zabiegi = $this->zabiegService->findByOpisOrZalecenia($searchByValue);
        dump($zabiegi);
        return new JsonResponse($zabiegi);
    }

    /**
     * @Route("/zabieg", name="zabieg")
     * @IsGranted("ROLE_ZABIEGI_ODCZYT")
     */
    public function index()
    {
        return $this->render('zabieg/zabieg.html.twig', []);
    }

    /**
     * @IsGranted("ROLE_ZABIEGI_ZAPIS")
     * @Route("/zabieg/new", name="zabieg-new", methods={"GET"})
     * @param int $userId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newZabieg()
    {
        $rodzajeZabiegow = $this->rodzajZabieguService->getAll();
        return $this->render('zabieg/zabieg.manage.html.twig', [
            'rodzajeZabiegow' => json_encode($rodzajeZabiegow),
            'title' => 'Nowy zabieg'
        ]);
    }

    /**
     * @IsGranted("ROLE_ZABIEGI_ZAPIS")
     * @Route("/zabieg/edit/{zabiegId}", name="zabieg-edit", methods={"GET"})
     * @param int $userId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editZabieg($zabiegId)
    {
        $rodzajeZabiegow = $this->rodzajZabieguService->getAll();
        $item = $this->zabiegService->getArrayById($zabiegId);
        return $this->render('zabieg/zabieg.manage.html.twig', [
            'rodzajeZabiegow' => json_encode($rodzajeZabiegow),
            'item' => json_encode($item),
            'id' => $zabiegId,
            'title' => 'Edycja zabiegu'
        ]);
    }

    /**
     * @IsGranted("ROLE_ZABIEGI_ZAPIS")
     * @Route("/zabieg/new", name="zabieg-new-store,", methods={"POST"})
     */
    public function newZabiegSave(Request $request)
    {
        $zabieg = $request->request->get('zabieg');
        $rodzajZabiegu = $this->rodzajZabieguService->getRodzajZabieguById($zabieg['rodzajZabiegu']);
        $zabiegEntity = new Zabieg();
        $zabiegEntity->setOpis($zabieg['opis']);
        $zabiegEntity->setZalecenia($zabieg['zalecenia']);
        $zabiegEntity->setCzasTrwania($zabieg['czasTrwania']);
        $zabiegEntity->setCena($zabieg['cena']);
        $zabiegEntity->setRodzajZabiegu($rodzajZabiegu);

        $this->zabiegService->addEntity($zabiegEntity);
        return new JsonResponse(['status' => 'success']);
    }

    /**
     * @IsGranted("ROLE_ZABIEGI_ZAPIS")
     * @Route("/zabieg/edit/{zabiegId}", name="zabieg-edit-store,", methods={"POST"})
     */
    public function editZabiegSave(Request $request, $zabiegId)
    {
        $zabieg = $request->request->get('zabieg');
        $rodzajZabiegu = $this->rodzajZabieguService->getRodzajZabieguById($zabieg['rodzajZabiegu']);
        $zabiegEntity = new Zabieg();
        $zabiegEntity->setOpis($zabieg['opis']);
        $zabiegEntity->setZalecenia($zabieg['zalecenia']);
        $zabiegEntity->setCzasTrwania($zabieg['czasTrwania']);
        $zabiegEntity->setCena($zabieg['cena']);
        $zabiegEntity->setRodzajZabiegu($rodzajZabiegu);
        $this->zabiegService->update($zabiegId,$zabiegEntity);
        return new JsonResponse(['status' => 'success']);
    }

    /**
     * @IsGranted("ROLE_ZABIEGI_ZAPIS")
     * @Route("/zabieg/relacje/new", name="zabieg-relacja-new", methods={"GET"})
     * @param int $userId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newRelacja()
    {
        $zabiegi = $this->zabiegService->findByOpisOrZalecenia("");
        return $this->render('zabieg/relacja.manage.html.twig', [
            'zabiegi' => json_encode($zabiegi)
        ]);
    }

    /**
     * @IsGranted("ROLE_ZABIEGI_ODCZYT")
     * @Route("/zabieg/relacje/search", name="zabieg-relacje-search", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function relacjaSearch(Request $request)
    {
        $searchByValueX = $request->request->get('searchByValueX');
        $searchByValueY = $request->request->get('searchByValueY');
        $relacje = $this->relacjaMiedzyZabiegamiService->findByZabieg($searchByValueX, $searchByValueY);
        dump($relacje);
        return new JsonResponse($relacje);
    }

    /**
     * @IsGranted("ROLE_ZABIEGI_ODCZYT")
     * @Route("/zabieg/relacje", name="zabieg-relacje", methods={"GET"})
     * @param int $userId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getRelacje()
    {
        return $this->render('zabieg/relacja.list.html.twig', []);
    }

    /**
     * @IsGranted("ROLE_ZABIEGI_ZAPIS")
     * @Route("/zabieg/relacje/new", name="zabieg-relacja-new-store", methods={"POST"})
     * @param int $userId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newRelacjaSave(Request $request)
    {
        $zabiegX = $request->request->get('relacja')['zabiegX'];
        $zabiegY = $request->request->get('relacja')['zabiegY'];
        $minOdstep = $request->request->get('relacja')['minimalnyOdstep'];
        $this->relacjaMiedzyZabiegamiService->addRelacjaMiedzyZabiegami($zabiegX,$zabiegY,$minOdstep);
        return new JsonResponse(['status' => 'success']);
    }

    /**
     * @IsGranted("ROLE_ZABIEGI_ZAPIS")
     * @Route("/zabieg/relacje/remove", name="zabieg-relacja-remove", methods={"POST"})
     * @param int $userId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function removeRelacja(Request $request)
    {
        $zabiegId = $request->request->get("zabiegId");
        $this->relacjaMiedzyZabiegamiService->removeRelacjaMiedzyZabiegamiById($zabiegId);
        return new JsonResponse(['status' => 'success']);
    }

}
