<?php

namespace App\Controller;

use App\Service\EtapService;
use App\Service\OsobaService;
use App\Service\SpecjalistaService;
use App\Service\TurnusService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PlannerViewController extends AbstractController
{
    /** @var EtapService */
    private $etapSrv;

    /** @var TurnusService */
    private $turnusSrv;

    /** @var SpecjalistaService */
    private $specjalistaSrv;

    /**
     * PlannerViewController constructor.
     * @param EtapService $etapSrv
     * @param TurnusService $turnusSrv
     * @param SpecjalistaService $specjalistaSrv
     */
    public function __construct(EtapService $etapSrv, TurnusService $turnusSrv, SpecjalistaService $specjalistaSrv)
    {
        $this->etapSrv = $etapSrv;
        $this->turnusSrv = $turnusSrv;
        $this->specjalistaSrv = $specjalistaSrv;
    }

    /**
     * @IsGranted("ROLE_SPECJALISCI_GRUPA")
     * @IsGranted("ROLE_PACJENCI_GRUPA")
     * @Route("/planner/view/", name="planner_view")
     */
    public function indexAction()
    {
        return $this->render('planner_view/index.html.twig', []);
    }

    /**
     * @IsGranted("ROLE_PACJENCI_GRUPA")
     * @Route("/planner/view/pacjent", name="planner_view_pacjent")
     */
    public function viewPlanPacjentAction()
    {
        return $this->render('planner_view/pacjent.html.twig', []);
    }

    /**
     * @IsGranted("ROLE_SPECJALISCI_GRUPA")
     * @Route("/planner/view/specjalista", name="planner_view_specjalista")
     */
    public function viewPlanSpecjalistaAction()
    {
        $loggedUserId = $this->getUser()->getId();
        $specjalista = $this->specjalistaSrv->getSpecjalistaFunctionByLoggedUser($loggedUserId);

        return $this->render('planner_view/specjalista.html.twig', [
            'specjalista' => $specjalista,
        ]);
    }

}
