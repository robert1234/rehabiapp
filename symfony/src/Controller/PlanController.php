<?php

namespace App\Controller;

use App\Service\PlanService;
use App\Service\PozycjaPlanuService;
use App\Service\RozpoznanieService;
use App\Service\ZabiegService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PlanController extends AbstractController
{
    private $planSrv;
    private $pozycjaPlanuSrv;
    private $zabiegSrv;
    private $rozpoznanieSrv;

    /**
     * PlanController constructor.
     * @param $planSrv
     * @param $pozycjaPlanuSrv
     * @param $zabiegSrv
     * @param $rozpoznanieSrv
     */
    public function __construct(PlanService $planSrv,
                                PozycjaPlanuService $pozycjaPlanuSrv,
                                ZabiegService $zabiegSrv,
                                RozpoznanieService $rozpoznanieSrv)
    {
        $this->planSrv = $planSrv;
        $this->pozycjaPlanuSrv = $pozycjaPlanuSrv;
        $this->zabiegSrv = $zabiegSrv;
        $this->rozpoznanieSrv = $rozpoznanieSrv;
    }

    /**
     * @IsGranted("ROLE_PLANY_ODCZYT")
     * @Route("/plan/search", name="plan-search", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function planSearch(Request $request)
    {
        $searchByValue = $request->request->get('searchByValue');
        $plany = $this->planSrv->findByOpisOrRozpozanie($searchByValue);
        return new JsonResponse($plany);
    }

    /**
     * @IsGranted("ROLE_PLANY_ODCZYT")
     * @Route("/plan/search/pacjent", name="plan-search-pacjent", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function planSearchByPacjent(Request $request)
    {
        $pacjentId = $request->request->get('pacjent_id');
        dump($pacjentId);
        $plany = $this->planSrv->findPlanyForPacjent($pacjentId);
        return new JsonResponse($plany);
    }

    /**
     * @IsGranted("ROLE_PLANY_ODCZYT")
     * @Route("/plan", name="plan")
     */
    public function index()
    {
        return $this->render('plan/list.html.twig', []);
    }

    /**
     * @IsGranted("ROLE_PLANY_ZAPIS")
     * @Route("/plan/new", name="plan-new", methods={"GET"})
     */
    public function planNew()
    {
        $rozpoznania = $this->rozpoznanieSrv->getAll();
        $zabiegi = $this->zabiegSrv->findByOpisOrZalecenia('');
        return $this->render('plan/manage.html.twig', [
            'rozpoznania' => json_encode($rozpoznania),
            'zabiegi' => json_encode($zabiegi),
            'title' => 'Nowy Plan',
        ]);
    }

    /**
     * @IsGranted("ROLE_PLANY_ZAPIS")
     * @Route("/plan/new", name="plan-new-store", methods={"POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function planNewStore(Request $request)
    {
        $plan = $request->request->get('plan');
        $pozycje = $request->request->get('pozycje');

        $planId = $this->planSrv->addEntity($plan['rozpoznanie'], $plan['opis']);
        $this->pozycjaPlanuSrv->updateListByPlan($planId, $pozycje);

        return new JsonResponse(['status' => 'success']);
    }

    /**
     * @IsGranted("ROLE_PLANY_ZAPIS")
     * @Route("/plan/edit/{planId}", name="plan-edit", methods={"GET"})
     */
    public function planEdit($planId)
    {
        $plan = $this->planSrv->getArrayById($planId);
        dump($plan);
        $pozycje = $this->pozycjaPlanuSrv->getZabiegListByPlan($planId);
        $rozpoznania = $this->rozpoznanieSrv->getAll();
        $zabiegi = $this->zabiegSrv->findByOpisOrZalecenia('');
        return $this->render('plan/manage.html.twig', [
            'rozpoznania' => json_encode($rozpoznania),
            'zabiegi' => json_encode($zabiegi),
            'item' => json_encode($plan),
            'pozycje' => json_encode($pozycje),
            'id' => $planId,
            'title' => 'Edycja Planu',
        ]);
    }

    /**
     * @IsGranted("ROLE_PLANY_ZAPIS")
     * @Route("/plan/edit/{planId}", name="plan-edit-store", methods={"POST"})
     */
    public function planEditStore(Request $request, $planId)
    {
        $plan = $request->request->get('plan');
        $pozycje = $request->request->get('pozycje');

        $this->planSrv->update($planId, $plan['opis'], $plan['rozpoznanie']);
        $this->pozycjaPlanuSrv->updateListByPlan($planId, $pozycje);

        return new JsonResponse(['status' => 'success']);
    }

}
