<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CzlonekGrupyRepository")
 */
class CzlonekGrupy
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UzytkownikSystemu", inversedBy="czlonkowieGrupy")
     * @ORM\JoinColumn(nullable=false)
     */
    private $uzytkownikSystemu;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Grupa", inversedBy="czlonkowieGrupy")
     * @ORM\JoinColumn(nullable=false)
     */
    private $grupa;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUzytkownikSystemu(): ?UzytkownikSystemu
    {
        return $this->uzytkownikSystemu;
    }

    public function setUzytkownikSystemu(?UzytkownikSystemu $uzytkownikSystemu): self
    {
        $this->uzytkownikSystemu = $uzytkownikSystemu;

        return $this;
    }

    public function getGrupa(): ?Grupa
    {
        return $this->grupa;
    }

    public function setGrupa(?Grupa $grupa): self
    {
        $this->grupa = $grupa;

        return $this;
    }
}
