<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OddzialRepository")
 */
class Oddzial
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $nazwa;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Sala", mappedBy="oddzial")
     */
    private $sale;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Specjalista", mappedBy="oddzial")
     */
    private $specjalisci;

    public function __construct()
    {
        $this->sale = new ArrayCollection();
        $this->specjalisci = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNazwa(): ?string
    {
        return $this->nazwa;
    }

    public function setNazwa(string $nazwa): self
    {
        $this->nazwa = $nazwa;

        return $this;
    }

    /**
     * @return Collection|Sala[]
     */
    public function getSale(): Collection
    {
        return $this->sale;
    }

    public function addSale(Sala $sale): self
    {
        if (!$this->sale->contains($sale)) {
            $this->sale[] = $sale;
            $sale->setOddzial($this);
        }

        return $this;
    }

    public function removeSale(Sala $sale): self
    {
        if ($this->sale->contains($sale)) {
            $this->sale->removeElement($sale);
            // set the owning side to null (unless already changed)
            if ($sale->getOddzial() === $this) {
                $sale->setOddzial(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Specjalista[]
     */
    public function getSpecjalisci(): Collection
    {
        return $this->specjalisci;
    }

    public function addSpecjalisci(Specjalista $specjalisci): self
    {
        if (!$this->specjalisci->contains($specjalisci)) {
            $this->specjalisci[] = $specjalisci;
            $specjalisci->setOddzial($this);
        }

        return $this;
    }

    public function removeSpecjalisci(Specjalista $specjalisci): self
    {
        if ($this->specjalisci->contains($specjalisci)) {
            $this->specjalisci->removeElement($specjalisci);
            // set the owning side to null (unless already changed)
            if ($specjalisci->getOddzial() === $this) {
                $specjalisci->setOddzial(null);
            }
        }

        return $this;
    }
}
