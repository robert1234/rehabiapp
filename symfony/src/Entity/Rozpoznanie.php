<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RozpoznanieRepository")
 */
class Rozpoznanie
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=6)
     */
    private $kod;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $opis;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Rozpoznanie", inversedBy="rozpoznaniaPodrzedne")
     * @ORM\JoinColumn(nullable=false)
     */
    private $rozpoznanieNadrzedne;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Pacjent", mappedBy="rozpoznanie")
     */
    private $pacjenci;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Plan", mappedBy="rozpoznanie")
     */
    private $plany;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Plan", mappedBy="rozpoznanieNadrzedne")
     */
    private $rozpoznaniaPodrzedne;

    public function __construct()
    {
        $this->pacjenci = new ArrayCollection();
        $this->plany = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getKod(): ?string
    {
        return $this->kod;
    }

    public function setKod(string $kod): self
    {
        $this->kod = $kod;

        return $this;
    }

    public function getOpis(): ?string
    {
        return $this->opis;
    }

    public function setOpis(?string $opis): self
    {
        $this->opis = $opis;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRozpoznanieNadrzedne()
    {
        return $this->rozpoznanieNadrzedne;
    }

    /**
     * @param mixed $rozpoznanieNadrzedne
     */
    public function setRozpoznanieNadrzedne($rozpoznanieNadrzedne): void
    {
        $this->rozpoznanieNadrzedne = $rozpoznanieNadrzedne;
    }

    /**
     * @return Collection|Pacjent[]
     */
    public function getPacjenci(): Collection
    {
        return $this->pacjenci;
    }

    public function addPacjenci(Pacjent $pacjenci): self
    {
        if (!$this->pacjenci->contains($pacjenci)) {
            $this->pacjenci[] = $pacjenci;
            $pacjenci->setRozpoznanie($this);
        }

        return $this;
    }

    public function removePacjenci(Pacjent $pacjenci): self
    {
        if ($this->pacjenci->contains($pacjenci)) {
            $this->pacjenci->removeElement($pacjenci);
            // set the owning side to null (unless already changed)
            if ($pacjenci->getRozpoznanie() === $this) {
                $pacjenci->setRozpoznanie(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Plan[]
     */
    public function getPlany(): Collection
    {
        return $this->plany;
    }

    public function addPlany(Plan $plany): self
    {
        if (!$this->plany->contains($plany)) {
            $this->plany[] = $plany;
            $plany->setRozpoznanie($this);
        }

        return $this;
    }

    public function removePlany(Plan $plany): self
    {
        if ($this->plany->contains($plany)) {
            $this->plany->removeElement($plany);
            // set the owning side to null (unless already changed)
            if ($plany->getRozpoznanie() === $this) {
                $plany->setRozpoznanie(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Rozpoznanie[]
     */
    public function getRozpoznaniaPodrzedne(): Collection
    {
        return $this->rozpoznaniaPodrzedne;
    }

    public function addRozpoznaniePodrzedne(Rozpoznanie $rozpoznania): self
    {
        if (!$this->rozpoznaniaPodrzedne->contains($rozpoznania)) {
            $this->rozpoznaniaPodrzedne[] = $rozpoznania;
            $rozpoznania->setRozpoznanieNadrzedne($this);
        }

        return $this;
    }

    public function removeRozpoznaniePodrzedne(Rozpoznanie $rozpoznania): self
    {
        if ($this->rozpoznaniaPodrzedne->contains($rozpoznania)) {
            $this->rozpoznaniaPodrzedne->removeElement($rozpoznania);
            // set the owning side to null (unless already changed)
            if ($rozpoznania->getRozpoznanieNadrzedne() === $this) {
                $rozpoznania->setRozpoznanieNadrzedne(null);
            }
        }

        return $this;
    }
}
