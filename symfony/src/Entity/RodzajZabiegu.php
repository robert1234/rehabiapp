<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RodzajZabieguRepository")
 */
class RodzajZabiegu
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $nazwa;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Zabieg", mappedBy="rodzajZabiegu")
     */
    private $zabiegi;

    public function __construct()
    {
        $this->zabiegi = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNazwa(): ?string
    {
        return $this->nazwa;
    }

    public function setNazwa(string $nazwa): self
    {
        $this->nazwa = $nazwa;

        return $this;
    }

    /**
     * @return Collection|Zabieg[]
     */
    public function getZabiegi(): Collection
    {
        return $this->zabiegi;
    }

    public function addZabiegi(Zabieg $zabiegi): self
    {
        if (!$this->zabiegi->contains($zabiegi)) {
            $this->zabiegi[] = $zabiegi;
            $zabiegi->setRodzajZabiegu($this);
        }

        return $this;
    }

    public function removeZabiegi(Zabieg $zabiegi): self
    {
        if ($this->zabiegi->contains($zabiegi)) {
            $this->zabiegi->removeElement($zabiegi);
            // set the owning side to null (unless already changed)
            if ($zabiegi->getRodzajZabiegu() === $this) {
                $zabiegi->setRodzajZabiegu(null);
            }
        }

        return $this;
    }
}
