<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StatusWizytyRepository")
 */
class StatusWizyty
{
    const ZAREJESTROWANO = 1;
    const NIE_ODBYTO = 2;
    const ODBYTO = 3;
    const ZAPLANOWANO = 4;
    const ANULOWANO = 5;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $nazwa;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Etap", mappedBy="statusWizyty")
     */
    private $etapy;

    public function __construct()
    {
        $this->etapy = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNazwa(): ?string
    {
        return $this->nazwa;
    }

    public function setNazwa(string $nazwa): self
    {
        $this->nazwa = $nazwa;

        return $this;
    }

    /**
     * @return Collection|Etap[]
     */
    public function getEtapy(): Collection
    {
        return $this->etapy;
    }

    public function addEtapy(Etap $etapy): self
    {
        if (!$this->etapy->contains($etapy)) {
            $this->etapy[] = $etapy;
            $etapy->setStatusWizyty($this);
        }

        return $this;
    }

    public function removeEtapy(Etap $etapy): self
    {
        if ($this->etapy->contains($etapy)) {
            $this->etapy->removeElement($etapy);
            // set the owning side to null (unless already changed)
            if ($etapy->getStatusWizyty() === $this) {
                $etapy->setStatusWizyty(null);
            }
        }

        return $this;
    }
}
