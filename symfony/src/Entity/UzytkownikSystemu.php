<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UzytkownikSystemuRepository")
 */
class UzytkownikSystemu implements UserInterface, EquatableInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $login;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $haslo;

    /**
     * @ORM\Column(type="boolean")
     */
    private $aktywny;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Osoba", inversedBy="uzytkownicySystemu")
     * @ORM\JoinColumn(nullable=false)
     */
    private $osoba;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CzlonekGrupy", mappedBy="uzytkownikSystemu")
     */
    private $czlonkowieGrupy;

    private $roles;

    public function __construct()
    {
        $this->czlonkowieGrupy = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLogin(): ?string
    {
        return $this->login;
    }

    public function setLogin(string $login): self
    {
        $this->login = $login;

        return $this;
    }

    public function getHaslo(): ?string
    {
        return $this->haslo;
    }

    public function setHaslo(string $haslo): self
    {
        $this->haslo = $haslo;

        return $this;
    }

    public function getAktywny(): ?bool
    {
        return $this->aktywny;
    }

    public function setAktywny(bool $aktywny): self
    {
        $this->aktywny = $aktywny;

        return $this;
    }

    public function getOsoba(): ?Osoba
    {
        return $this->osoba;
    }

    public function setOsoba(?Osoba $osoba): self
    {
        $this->osoba = $osoba;

        return $this;
    }

    /**
     * @return Collection|CzlonekGrupy[]
     */
    public function getCzlonkowieGrupy(): Collection
    {
        return $this->czlonkowieGrupy;
    }

    public function addCzlonkowieGrupy(CzlonekGrupy $czlonkowieGrupy): self
    {
        if (!$this->czlonkowieGrupy->contains($czlonkowieGrupy)) {
            $this->czlonkowieGrupy[] = $czlonkowieGrupy;
            $czlonkowieGrupy->setUzytkownikSystemu($this);
        }

        return $this;
    }

    public function removeCzlonkowieGrupy(CzlonekGrupy $czlonkowieGrupy): self
    {
        if ($this->czlonkowieGrupy->contains($czlonkowieGrupy)) {
            $this->czlonkowieGrupy->removeElement($czlonkowieGrupy);
            // set the owning side to null (unless already changed)
            if ($czlonkowieGrupy->getUzytkownikSystemu() === $this) {
                $czlonkowieGrupy->setUzytkownikSystemu(null);
            }
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getRoles()
    {
        dump($this->roles);
        return $this->roles;
    }

    public function setRoles($array)
    {
        if(is_null($array)) {
            $array = ["ROLE_USER"];
        } else {
            array_push($array,"ROLE_USER");
        }
        $this->roles = $array;
    }

    /**
     * @inheritDoc
     */
    public function getPassword()
    {
        return $this->getHaslo();
    }

    /**
     * @inheritDoc
     */
    public function getSalt()
    {}

    /**
     * @inheritDoc
     */
    public function getUsername()
    {
        return $this->getLogin();
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials()
    {}

    /**
     * @inheritDoc
     */
    public function isEqualTo(UserInterface $user)
    {
        if ($this->login !== $user->getLogin()) {
            return false;
        }

        return true;
    }
}
