<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SpecjalistaRepository")
 */
class Specjalista
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Osoba", inversedBy="specjalisci")
     * @ORM\JoinColumn(nullable=false)
     */
    private $osoba;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Specjalizacja", inversedBy="specjalisci")
     * @ORM\JoinColumn(nullable=false)
     */
    private $specjalizacja;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Oddzial", inversedBy="specjalisci")
     * @ORM\JoinColumn(nullable=false)
     */
    private $oddzial;

    /**
     * @ORM\Column(type="date")
     */
    private $dataRozpoczecia;

    /**
     * @ORM\Column(type="date")
     */
    private $dataZakonczenia;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    private $opisDodatkowy;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Etap", mappedBy="specjalista")
     */
    private $etapy;

    public function __construct()
    {
        $this->etapy = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOsoba(): ?Osoba
    {
        return $this->osoba;
    }

    public function setOsoba(?Osoba $osoba): self
    {
        $this->osoba = $osoba;

        return $this;
    }

    public function getSpecjalizacja(): ?Specjalizacja
    {
        return $this->specjalizacja;
    }

    public function setSpecjalizacja(?Specjalizacja $specjalizacja): self
    {
        $this->specjalizacja = $specjalizacja;

        return $this;
    }

    public function getOddzial(): ?Oddzial
    {
        return $this->oddzial;
    }

    public function setOddzial(?Oddzial $oddzial): self
    {
        $this->oddzial = $oddzial;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDataRozpoczecia()
    {
        return $this->dataRozpoczecia;
    }

    /**
     * @param mixed $dataRozpoczecia
     */
    public function setDataRozpoczecia($dataRozpoczecia): void
    {
        $this->dataRozpoczecia = $dataRozpoczecia;
    }

    /**
     * @return mixed
     */
    public function getDataZakonczenia()
    {
        return $this->dataZakonczenia;
    }

    /**
     * @param mixed $dataZakonczenia
     */
    public function setDataZakonczenia($dataZakonczenia): void
    {
        $this->dataZakonczenia = $dataZakonczenia;
    }

    public function getOpisDodatkowy(): ?string
    {
        return $this->opisDodatkowy;
    }

    public function setOpisDodatkowy(?string $opisDodatkowy): self
    {
        $this->opisDodatkowy = $opisDodatkowy;

        return $this;
    }

    /**
     * @return Collection|Etap[]
     */
    public function getEtapy(): Collection
    {
        return $this->etapy;
    }

    public function addEtapy(Etap $etapy): self
    {
        if (!$this->etapy->contains($etapy)) {
            $this->etapy[] = $etapy;
            $etapy->setSpecjalista($this);
        }

        return $this;
    }

    public function removeEtapy(Etap $etapy): self
    {
        if ($this->etapy->contains($etapy)) {
            $this->etapy->removeElement($etapy);
            // set the owning side to null (unless already changed)
            if ($etapy->getSpecjalista() === $this) {
                $etapy->setSpecjalista(null);
            }
        }

        return $this;
    }
}
