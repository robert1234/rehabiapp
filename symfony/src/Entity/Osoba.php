<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OsobaRepository")
 */
class Osoba
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=48)
     */
    private $imie;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $nazwisko;

    /**
     * @ORM\Column(type="string", length=11)
     */
    private $pesel;

    /**
     * @ORM\Column(type="date")
     */
    private $dataUrodzenia;

    /**
     * @ORM\Column(type="string", length=1)
     */
    private $plec;

    /**
     * @ORM\Column(type="string", length=48)
     */
    private $imieOjca;

    /**
     * @ORM\Column(type="string", length=48)
     */
    private $imieMatki;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $ulica;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $nrDomu;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $miejscowosc;

    /**
     * @ORM\Column(type="string", length=6)
     */
    private $kodPocztowy;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dataRejestracji;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UzytkownikSystemu", mappedBy="osoba")
     */
    private $uzytkownicySystemu;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Pacjent", mappedBy="osoba")
     */
    private $pacjenci;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Specjalista", mappedBy="osoba")
     */
    private $specjalisci;

    public function __construct()
    {
        $this->uzytkownicySystemu = new ArrayCollection();
        $this->pacjenci = new ArrayCollection();
        $this->specjalisci = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId($id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getImie(): ?string
    {
        return $this->imie;
    }

    public function setImie(string $imie): self
    {
        $this->imie = $imie;

        return $this;
    }

    public function getNazwisko(): ?string
    {
        return $this->nazwisko;
    }

    public function setNazwisko(string $nazwisko): self
    {
        $this->nazwisko = $nazwisko;

        return $this;
    }

    public function getPesel(): ?string
    {
        return $this->pesel;
    }

    public function setPesel(string $pesel): self
    {
        $this->pesel = $pesel;

        return $this;
    }

    public function getDataUrodzenia(): ?\DateTimeInterface
    {
        return $this->dataUrodzenia;
    }

    public function setDataUrodzenia(\DateTimeInterface $dataUrodzenia): self
    {
        $this->dataUrodzenia = $dataUrodzenia;
        return $this;
    }

    public function getPlec(): ?string
    {
        return $this->plec;
    }

    public function setPlec(string $plec): self
    {
        $this->plec = $plec;

        return $this;
    }

    public function getImieOjca(): ?string
    {
        return $this->imieOjca;
    }

    public function setImieOjca(string $imieOjca): self
    {
        $this->imieOjca = $imieOjca;

        return $this;
    }

    public function getImieMatki(): ?string
    {
        return $this->imieMatki;
    }

    public function setImieMatki(string $imieMatki): self
    {
        $this->imieMatki = $imieMatki;

        return $this;
    }

    public function getUlica(): ?string
    {
        return $this->ulica;
    }

    public function setUlica(string $ulica): self
    {
        $this->ulica = $ulica;

        return $this;
    }

    public function getNrDomu(): ?string
    {
        return $this->nrDomu;
    }

    public function setNrDomu(string $nrDomu): self
    {
        $this->nrDomu = $nrDomu;

        return $this;
    }

    public function getMiejscowosc(): ?string
    {
        return $this->miejscowosc;
    }

    public function setMiejscowosc(string $miejscowosc): self
    {
        $this->miejscowosc = $miejscowosc;

        return $this;
    }

    public function getKodPocztowy(): ?string
    {
        return $this->kodPocztowy;
    }

    public function setKodPocztowy(string $kodPocztowy): self
    {
        $this->kodPocztowy = $kodPocztowy;

        return $this;
    }

    public function getDataRejestracji(): ?\DateTimeInterface
    {
        return $this->dataRejestracji;
    }

    public function setDataRejestracji(\DateTimeInterface $dataRejestracji): self
    {
        $this->dataRejestracji = $dataRejestracji;

        return $this;
    }

    /**
     * @return Collection|UzytkownikSystemu[]
     */
    public function getUzytkownicySystemu(): Collection
    {
        return $this->uzytkownicySystemu;
    }

    public function addUzytkownicySystemu(UzytkownikSystemu $uzytkownicySystemu): self
    {
        if (!$this->uzytkownicySystemu->contains($uzytkownicySystemu)) {
            $this->uzytkownicySystemu[] = $uzytkownicySystemu;
            $uzytkownicySystemu->setOsoba($this);
        }

        return $this;
    }

    public function removeUzytkownicySystemu(UzytkownikSystemu $uzytkownicySystemu): self
    {
        if ($this->uzytkownicySystemu->contains($uzytkownicySystemu)) {
            $this->uzytkownicySystemu->removeElement($uzytkownicySystemu);
            // set the owning side to null (unless already changed)
            if ($uzytkownicySystemu->getOsoba() === $this) {
                $uzytkownicySystemu->setOsoba(null);
            }
        }

        return $this;
    }

    public function getUzytkownikSystemu() : UzytkownikSystemu
    {
        if(!$this->uzytkownicySystemu->isEmpty()) {
            return $this->uzytkownicySystemu->first();
        }
    }

    /**
     * @return Collection|Pacjent[]
     */
    public function getPacjenci(): Collection
    {
        return $this->pacjenci;
    }

    public function addPacjenci(Pacjent $pacjenci): self
    {
        if (!$this->pacjenci->contains($pacjenci)) {
            $this->pacjenci[] = $pacjenci;
            $pacjenci->setOsoba($this);
        }

        return $this;
    }

    public function removePacjenci(Pacjent $pacjenci): self
    {
        if ($this->pacjenci->contains($pacjenci)) {
            $this->pacjenci->removeElement($pacjenci);
            // set the owning side to null (unless already changed)
            if ($pacjenci->getOsoba() === $this) {
                $pacjenci->setOsoba(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Specjalista[]
     */
    public function getSpecjalisci(): Collection
    {
        return $this->specjalisci;
    }

    public function addSpecjalisci(Specjalista $specjalisci): self
    {
        if (!$this->specjalisci->contains($specjalisci)) {
            $this->specjalisci[] = $specjalisci;
            $specjalisci->setOsoba($this);
        }

        return $this;
    }

    public function removeSpecjalisci(Specjalista $specjalisci): self
    {
        if ($this->specjalisci->contains($specjalisci)) {
            $this->specjalisci->removeElement($specjalisci);
            // set the owning side to null (unless already changed)
            if ($specjalisci->getOsoba() === $this) {
                $specjalisci->setOsoba(null);
            }
        }

        return $this;
    }
}
