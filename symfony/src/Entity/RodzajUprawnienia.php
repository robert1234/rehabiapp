<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RodzajUprawnieniaRepository")
 */
class RodzajUprawnienia
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $nazwa;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    private $opis;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Uprawnienie", mappedBy="rodzajUprawnienia")
     */
    private $uprawnienia;

    public function __construct()
    {
        $this->uprawnienia = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNazwa(): ?string
    {
        return $this->nazwa;
    }

    public function setNazwa(string $nazwa): self
    {
        $this->nazwa = $nazwa;

        return $this;
    }

    public function getOpis(): ?string
    {
        return $this->opis;
    }

    public function setOpis(?string $opis): self
    {
        $this->opis = $opis;

        return $this;
    }

    /**
     * @return Collection|Uprawnienie[]
     */
    public function getUprawnienia(): Collection
    {
        return $this->uprawnienia;
    }

    public function addUprawnienium(Uprawnienie $uprawnienium): self
    {
        if (!$this->uprawnienia->contains($uprawnienium)) {
            $this->uprawnienia[] = $uprawnienium;
            $uprawnienium->setRodzajUprawnienia($this);
        }

        return $this;
    }

    public function removeUprawnienium(Uprawnienie $uprawnienium): self
    {
        if ($this->uprawnienia->contains($uprawnienium)) {
            $this->uprawnienia->removeElement($uprawnienium);
            // set the owning side to null (unless already changed)
            if ($uprawnienium->getRodzajUprawnienia() === $this) {
                $uprawnienium->setRodzajUprawnienia(null);
            }
        }

        return $this;
    }
}
