<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EtapRepository")
 */
class Etap
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Specjalista", inversedBy="etapy")
     */
    private $specjalista;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Zabieg", inversedBy="etapy")
     * @ORM\JoinColumn(nullable=false)
     */
    private $zabieg;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Sala", inversedBy="etapy")
     */
    private $sala;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dataPrzeprowadzenia;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Turnus", inversedBy="etapy")
     * @ORM\JoinColumn(nullable=false)
     */
    private $turnus;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\StatusWizyty", inversedBy="etapy")
     */
    private $statusWizyty;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $kodWalidacyjny;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSpecjalista(): ?Specjalista
    {
        return $this->specjalista;
    }

    public function setSpecjalista(?Specjalista $specjalista): self
    {
        $this->specjalista = $specjalista;

        return $this;
    }

    public function getZabieg(): ?Zabieg
    {
        return $this->zabieg;
    }

    public function setZabieg(?Zabieg $zabieg): self
    {
        $this->zabieg = $zabieg;

        return $this;
    }

    public function getSala(): ?Sala
    {
        return $this->sala;
    }

    public function setSala(?Sala $sala): self
    {
        $this->sala = $sala;

        return $this;
    }

    public function getDataPrzeprowadzenia(): ?\DateTime
    {
        return $this->dataPrzeprowadzenia;
    }

    public function setDataPrzeprowadzenia(?\DateTime $dataPrzeprowadzenia): self
    {
        $this->dataPrzeprowadzenia = $dataPrzeprowadzenia;

        return $this;
    }

    public function getTurnus(): ?Turnus
    {
        return $this->turnus;
    }

    public function setTurnus(?Turnus $turnus): self
    {
        $this->turnus = $turnus;

        return $this;
    }

    public function getStatusWizyty(): ?StatusWizyty
    {
        return $this->statusWizyty;
    }

    public function setStatusWizyty(?StatusWizyty $statusWizyty): self
    {
        $this->statusWizyty = $statusWizyty;

        return $this;
    }

    public function getKodWalidacyjny(): ?string
    {
        return $this->kodWalidacyjny;
    }

    public function setKodWalidacyjny(?string $kodWalidacyjny): self
    {
        $this->kodWalidacyjny = $kodWalidacyjny;

        return $this;
    }
}
