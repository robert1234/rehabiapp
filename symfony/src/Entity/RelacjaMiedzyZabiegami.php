<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RelacjaMiedzyZabiegamiRepository")
 */
class RelacjaMiedzyZabiegami
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Zabieg", inversedBy="relacjeMiedzyZabiegamiX")
     * @ORM\JoinColumn(nullable=false)
     */
    private $zabiegX;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Zabieg", inversedBy="relacjeMiedzyZabiegamiY")
     * @ORM\JoinColumn(nullable=false)
     */
    private $zabiegY;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $minimalnyOdstep;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getZabiegX(): ?Zabieg
    {
        return $this->zabiegX;
    }

    public function setZabiegX(?Zabieg $zabiegX): self
    {
        $this->zabiegX = $zabiegX;

        return $this;
    }

    public function getZabiegY(): ?Zabieg
    {
        return $this->zabiegY;
    }

    public function setZabiegY(?Zabieg $zabiegY): self
    {
        $this->zabiegY = $zabiegY;

        return $this;
    }

    public function getMinimalnyOdstep(): ?int
    {
        return $this->minimalnyOdstep;
    }

    public function setMinimalnyOdstep(?int $minimalnyOdstep): self
    {
        $this->minimalnyOdstep = $minimalnyOdstep;

        return $this;
    }
}
