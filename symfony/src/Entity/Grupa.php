<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GrupaRepository")
 */
class Grupa
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $nazwa;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    private $opis;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Aplikacja", inversedBy="grupy")
     * @ORM\JoinColumn(nullable=false)
     */
    private $aplikacja;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Uprawnienie", mappedBy="grupa")
     */
    private $uprawnienia;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CzlonekGrupy", mappedBy="grupa")
     */
    private $czlonkowieGrupy;

    public function __construct()
    {
        $this->uprawnienia = new ArrayCollection();
        $this->czlonkowieGrupy = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNazwa(): ?string
    {
        return $this->nazwa;
    }

    public function setNazwa(string $nazwa): self
    {
        $this->nazwa = $nazwa;

        return $this;
    }

    public function getOpis(): ?string
    {
        return $this->opis;
    }

    public function setOpis(string $opis): self
    {
        $this->opis = $opis;

        return $this;
    }

    public function getAplikacja(): ?Aplikacja
    {
        return $this->aplikacja;
    }

    public function setAplikacja(?Aplikacja $aplikacja): self
    {
        $this->aplikacja = $aplikacja;

        return $this;
    }

    /**
     * @return Collection|Uprawnienie[]
     */
    public function getUprawnienia(): Collection
    {
        return $this->uprawnienia;
    }

    public function addUprawnienium(Uprawnienie $uprawnienium): self
    {
        if (!$this->uprawnienia->contains($uprawnienium)) {
            $this->uprawnienia[] = $uprawnienium;
            $uprawnienium->setGrupa($this);
        }

        return $this;
    }

    public function removeUprawnienium(Uprawnienie $uprawnienium): self
    {
        if ($this->uprawnienia->contains($uprawnienium)) {
            $this->uprawnienia->removeElement($uprawnienium);
            // set the owning side to null (unless already changed)
            if ($uprawnienium->getGrupa() === $this) {
                $uprawnienium->setGrupa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CzlonekGrupy[]
     */
    public function getCzlonkowieGrupy(): Collection
    {
        return $this->czlonkowieGrupy;
    }

    public function addCzlonkowieGrupy(CzlonekGrupy $czlonkowieGrupy): self
    {
        if (!$this->czlonkowieGrupy->contains($czlonkowieGrupy)) {
            $this->czlonkowieGrupy[] = $czlonkowieGrupy;
            $czlonkowieGrupy->setGrupa($this);
        }

        return $this;
    }

    public function removeCzlonkowieGrupy(CzlonekGrupy $czlonkowieGrupy): self
    {
        if ($this->czlonkowieGrupy->contains($czlonkowieGrupy)) {
            $this->czlonkowieGrupy->removeElement($czlonkowieGrupy);
            // set the owning side to null (unless already changed)
            if ($czlonkowieGrupy->getGrupa() === $this) {
                $czlonkowieGrupy->setGrupa(null);
            }
        }

        return $this;
    }
}
