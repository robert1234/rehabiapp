<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PozycjaPlanuRepository")
 */
class PozycjaPlanu
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Plan", inversedBy="pozycjePlanu")
     * @ORM\JoinColumn(nullable=false)
     */
    private $plan;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $liczbaPorzadkowa;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Zabieg", inversedBy="pozycjePlanu")
     * @ORM\JoinColumn(nullable=false)
     */
    private $zabieg;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPlan(): ?Plan
    {
        return $this->plan;
    }

    public function setPlan(?Plan $plan): self
    {
        $this->plan = $plan;

        return $this;
    }

    public function getLiczbaPorzadkowa(): ?int
    {
        return $this->liczbaPorzadkowa;
    }

    public function setLiczbaPorzadkowa(?int $liczbaPorzadkowa): self
    {
        $this->liczbaPorzadkowa = $liczbaPorzadkowa;

        return $this;
    }

    public function getZabieg(): ?Zabieg
    {
        return $this->zabieg;
    }

    public function setZabieg(?Zabieg $zabieg): self
    {
        $this->zabieg = $zabieg;

        return $this;
    }
}
