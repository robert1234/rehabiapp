<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AplikacjaRepository")
 */
class Aplikacja
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $nazwa;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Grupa", mappedBy="aplikacja")
     */
    private $grupy;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ElementAplikacji", mappedBy="aplikacja")
     */
    private $elementyAplikacji;

    public function __construct()
    {
        $this->grupy = new ArrayCollection();
        $this->elementyAplikacji = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNazwa(): ?string
    {
        return $this->nazwa;
    }

    public function setNazwa(string $nazwa): self
    {
        $this->nazwa = $nazwa;

        return $this;
    }

    /**
     * @return Collection|Grupa[]
     */
    public function getGrupy(): Collection
    {
        return $this->grupy;
    }

    public function addGrupy(Grupa $grupy): self
    {
        if (!$this->grupy->contains($grupy)) {
            $this->grupy[] = $grupy;
            $grupy->setAplikacja($this);
        }

        return $this;
    }

    public function removeGrupy(Grupa $grupy): self
    {
        if ($this->grupy->contains($grupy)) {
            $this->grupy->removeElement($grupy);
            // set the owning side to null (unless already changed)
            if ($grupy->getAplikacja() === $this) {
                $grupy->setAplikacja(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ElementAplikacji[]
     */
    public function getElementyAplikacji(): Collection
    {
        return $this->elementyAplikacji;
    }

    public function addElementyAplikacji(ElementAplikacji $elementyAplikacji): self
    {
        if (!$this->elementyAplikacji->contains($elementyAplikacji)) {
            $this->elementyAplikacji[] = $elementyAplikacji;
            $elementyAplikacji->setAplikacja($this);
        }

        return $this;
    }

    public function removeElementyAplikacji(ElementAplikacji $elementyAplikacji): self
    {
        if ($this->elementyAplikacji->contains($elementyAplikacji)) {
            $this->elementyAplikacji->removeElement($elementyAplikacji);
            // set the owning side to null (unless already changed)
            if ($elementyAplikacji->getAplikacja() === $this) {
                $elementyAplikacji->setAplikacja(null);
            }
        }

        return $this;
    }
}
