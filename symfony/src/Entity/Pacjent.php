<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PacjentRepository")
 */
class Pacjent
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Osoba", inversedBy="pacjenci")
     * @ORM\JoinColumn(nullable=false)
     */
    private $osoba;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $platnik;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Rozpoznanie", inversedBy="pacjenci")
     * @ORM\JoinColumn(nullable=false)
     */
    private $rozpoznanie;

    /**
     * @ORM\Column(type="date")
     */
    private $dataRozpoczecia;

    /**
     * @ORM\Column(type="date")
     */
    private $dataZakonczenia;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    private $opisDodatkowy;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Turnus", mappedBy="pacjent")
     */
    private $turnusy;

    public function __construct()
    {
        $this->turnusy = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOsoba(): ?Osoba
    {
        return $this->osoba;
    }

    public function setOsoba(?Osoba $osoba): self
    {
        $this->osoba = $osoba;

        return $this;
    }

    public function getPlatnik(): ?string
    {
        return $this->platnik;
    }

    public function setPlatnik(?string $platnik): self
    {
        $this->platnik = $platnik;

        return $this;
    }

    public function getRozpoznanie(): ?Rozpoznanie
    {
        return $this->rozpoznanie;
    }

    public function setRozpoznanie(?Rozpoznanie $rozpoznanie): self
    {
        $this->rozpoznanie = $rozpoznanie;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDataRozpoczecia()
    {
        return $this->dataRozpoczecia;
    }

    /**
     * @param mixed $dataRozpoczecia
     */
    public function setDataRozpoczecia($dataRozpoczecia): void
    {
        $this->dataRozpoczecia = $dataRozpoczecia;
    }

    /**
     * @return mixed
     */
    public function getDataZakonczenia()
    {
        return $this->dataZakonczenia;
    }

    /**
     * @param mixed $dataZakonczenia
     */
    public function setDataZakonczenia($dataZakonczenia): void
    {
        $this->dataZakonczenia = $dataZakonczenia;
    }

    public function getOpisDodatkowy(): ?string
    {
        return $this->opisDodatkowy;
    }

    public function setOpisDodatkowy(?string $opisDodatkowy): self
    {
        $this->opisDodatkowy = $opisDodatkowy;

        return $this;
    }

    /**
     * @return Collection|Turnus[]
     */
    public function getTurnusy(): Collection
    {
        return $this->turnusy;
    }

    public function addTurnusy(Turnus $turnusy): self
    {
        if (!$this->turnusy->contains($turnusy)) {
            $this->turnusy[] = $turnusy;
            $turnusy->setPacjent($this);
        }

        return $this;
    }

    public function removeTurnusy(Turnus $turnusy): self
    {
        if ($this->turnusy->contains($turnusy)) {
            $this->turnusy->removeElement($turnusy);
            // set the owning side to null (unless already changed)
            if ($turnusy->getPacjent() === $this) {
                $turnusy->setPacjent(null);
            }
        }

        return $this;
    }
}
