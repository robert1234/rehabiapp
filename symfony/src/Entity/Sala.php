<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SalaRepository")
 */
class Sala
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Oddzial", inversedBy="sale")
     * @ORM\JoinColumn(nullable=false)
     */
    private $oddzial;

    /**
     * @ORM\Column(type="string", length=6)
     */
    private $nrSali;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Etap", mappedBy="sala")
     */
    private $etapy;

    public function __construct()
    {
        $this->etapy = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOddzial(): ?Oddzial
    {
        return $this->oddzial;
    }

    public function setOddzial(?Oddzial $oddzial): self
    {
        $this->oddzial = $oddzial;

        return $this;
    }

    public function getNrSali(): ?string
    {
        return $this->nrSali;
    }

    public function setNrSali(string $nrSali): self
    {
        $this->nrSali = $nrSali;

        return $this;
    }

    /**
     * @return Collection|Etap[]
     */
    public function getEtapy(): Collection
    {
        return $this->etapy;
    }

    public function addEtapy(Etap $etapy): self
    {
        if (!$this->etapy->contains($etapy)) {
            $this->etapy[] = $etapy;
            $etapy->setSala($this);
        }

        return $this;
    }

    public function removeEtapy(Etap $etapy): self
    {
        if ($this->etapy->contains($etapy)) {
            $this->etapy->removeElement($etapy);
            // set the owning side to null (unless already changed)
            if ($etapy->getSala() === $this) {
                $etapy->setSala(null);
            }
        }

        return $this;
    }
}
