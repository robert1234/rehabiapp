<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UprawnienieRepository")
 */
class Uprawnienie
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\RodzajUprawnienia", inversedBy="uprawnienia")
     * @ORM\JoinColumn(nullable=false)
     */
    private $rodzajUprawnienia;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Grupa", inversedBy="uprawnienia")
     * @ORM\JoinColumn(nullable=false)
     */
    private $grupa;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ElementAplikacji", inversedBy="uprawnienia")
     * @ORM\JoinColumn(nullable=false)
     */
    private $elementAplikacji;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getRodzajUprawnienia(): ?RodzajUprawnienia
    {
        return $this->rodzajUprawnienia;
    }

    public function setRodzajUprawnienia(?RodzajUprawnienia $rodzajUprawnienia): self
    {
        $this->rodzajUprawnienia = $rodzajUprawnienia;

        return $this;
    }

    public function getGrupa(): ?Grupa
    {
        return $this->grupa;
    }

    public function setGrupa(?Grupa $grupa): self
    {
        $this->grupa = $grupa;

        return $this;
    }

    public function getElementAplikacji(): ?ElementAplikacji
    {
        return $this->elementAplikacji;
    }

    public function setElementAplikacji(?ElementAplikacji $elementAplikacji): self
    {
        $this->elementAplikacji = $elementAplikacji;

        return $this;
    }
}
