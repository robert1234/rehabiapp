<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ZabiegRepository")
 */
class Zabieg
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\RodzajZabiegu", inversedBy="zabiegi")
     * @ORM\JoinColumn(nullable=false)
     */
    private $rodzajZabiegu;

    /**
     * @ORM\Column(type="decimal", precision=13, scale=2)
     */
    private $cena;

    /**
     * @ORM\Column(type="integer")
     */
    private $czasTrwania;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $zalecenia;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    private $opis;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PozycjaPlanu", mappedBy="zabieg")
     */
    private $pozycjePlanu;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RelacjaMiedzyZabiegami", mappedBy="zabiegX")
     */
    private $relacjeMiedzyZabiegamiX;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RelacjaMiedzyZabiegami", mappedBy="zabiegY")
     */
    private $relacjeMiedzyZabiegamiY;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Etap", mappedBy="zabieg")
     */
    private $etapy;

    public function __construct()
    {
        $this->pozycjePlanu = new ArrayCollection();
        $this->relacjeMiedzyZabiegamiX = new ArrayCollection();
        $this->relacjeMiedzyZabiegamiY = new ArrayCollection();
        $this->etapy = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRodzajZabiegu(): ?RodzajZabiegu
    {
        return $this->rodzajZabiegu;
    }

    public function setRodzajZabiegu(?RodzajZabiegu $rodzajZabiegu): self
    {
        $this->rodzajZabiegu = $rodzajZabiegu;

        return $this;
    }

    public function getCena(): ?string
    {
        return $this->cena;
    }

    public function setCena(string $cena): self
    {
        $this->cena = $cena;

        return $this;
    }

    public function getCzasTrwania(): ?int
    {
        return $this->czasTrwania;
    }

    public function setCzasTrwania(int $czasTrwania): self
    {
        $this->czasTrwania = $czasTrwania;

        return $this;
    }

    public function getZalecenia(): ?string
    {
        return $this->zalecenia;
    }

    public function setZalecenia(?string $zalecenia): self
    {
        $this->zalecenia = $zalecenia;

        return $this;
    }

    public function getOpis(): ?string
    {
        return $this->opis;
    }

    public function setOpis(?string $opis): self
    {
        $this->opis = $opis;

        return $this;
    }

    /**
     * @return Collection|PozycjaPlanu[]
     */
    public function getPozycjePlanu(): Collection
    {
        return $this->pozycjePlanu;
    }

    public function addPozycjePlanu(PozycjaPlanu $pozycjePlanu): self
    {
        if (!$this->pozycjePlanu->contains($pozycjePlanu)) {
            $this->pozycjePlanu[] = $pozycjePlanu;
            $pozycjePlanu->setZabieg($this);
        }

        return $this;
    }

    public function removePozycjePlanu(PozycjaPlanu $pozycjePlanu): self
    {
        if ($this->pozycjePlanu->contains($pozycjePlanu)) {
            $this->pozycjePlanu->removeElement($pozycjePlanu);
            // set the owning side to null (unless already changed)
            if ($pozycjePlanu->getZabieg() === $this) {
                $pozycjePlanu->setZabieg(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|RelacjaMiedzyZabiegami[]
     */
    public function getRelacjeMiedzyZabiegamiX(): Collection
    {
        return $this->relacjeMiedzyZabiegamiX;
    }

    public function addRelacjeMiedzyZabiegamiX(RelacjaMiedzyZabiegami $relacjeMiedzyZabiegamiX): self
    {
        if (!$this->relacjeMiedzyZabiegamiX->contains($relacjeMiedzyZabiegamiX)) {
            $this->relacjeMiedzyZabiegamiX[] = $relacjeMiedzyZabiegamiX;
            $relacjeMiedzyZabiegamiX->setZabiegX($this);
        }

        return $this;
    }

    public function removeRelacjeMiedzyZabiegamiX(RelacjaMiedzyZabiegami $relacjeMiedzyZabiegamiX): self
    {
        if ($this->relacjeMiedzyZabiegamiX->contains($relacjeMiedzyZabiegamiX)) {
            $this->relacjeMiedzyZabiegamiX->removeElement($relacjeMiedzyZabiegamiX);
            // set the owning side to null (unless already changed)
            if ($relacjeMiedzyZabiegamiX->getZabiegX() === $this) {
                $relacjeMiedzyZabiegamiX->setZabiegX(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|RelacjaMiedzyZabiegami[]
     */
    public function getRelacjeMiedzyZabiegamiY(): Collection
    {
        return $this->relacjeMiedzyZabiegamiY;
    }

    public function addRelacjeMiedzyZabiegamiY(RelacjaMiedzyZabiegami $relacjeMiedzyZabiegamiY): self
    {
        if (!$this->relacjeMiedzyZabiegamiY->contains($relacjeMiedzyZabiegamiY)) {
            $this->relacjeMiedzyZabiegamiY[] = $relacjeMiedzyZabiegamiY;
            $relacjeMiedzyZabiegamiY->setZabiegY($this);
        }

        return $this;
    }

    public function removeRelacjeMiedzyZabiegamiY(RelacjaMiedzyZabiegami $relacjeMiedzyZabiegamiY): self
    {
        if ($this->relacjeMiedzyZabiegamiY->contains($relacjeMiedzyZabiegamiY)) {
            $this->relacjeMiedzyZabiegamiY->removeElement($relacjeMiedzyZabiegamiY);
            // set the owning side to null (unless already changed)
            if ($relacjeMiedzyZabiegamiY->getZabiegY() === $this) {
                $relacjeMiedzyZabiegamiY->setZabiegY(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Etap[]
     */
    public function getEtapy(): Collection
    {
        return $this->etapy;
    }

    public function addEtapy(Etap $etapy): self
    {
        if (!$this->etapy->contains($etapy)) {
            $this->etapy[] = $etapy;
            $etapy->setZabieg($this);
        }

        return $this;
    }

    public function removeEtapy(Etap $etapy): self
    {
        if ($this->etapy->contains($etapy)) {
            $this->etapy->removeElement($etapy);
            // set the owning side to null (unless already changed)
            if ($etapy->getZabieg() === $this) {
                $etapy->setZabieg(null);
            }
        }

        return $this;
    }
}
