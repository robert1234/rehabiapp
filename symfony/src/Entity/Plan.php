<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlanRepository")
 */
class Plan
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Rozpoznanie", inversedBy="plany")
     * @ORM\JoinColumn(nullable=false)
     */
    private $rozpoznanie;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    private $opis;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Turnus", mappedBy="plan")
     */
    private $turnusy;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PozycjaPlanu", mappedBy="plan")
     */
    private $pozycjePlanu;

    public function __construct()
    {
        $this->turnusy = new ArrayCollection();
        $this->pozycjePlanu = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRozpoznanie(): ?Rozpoznanie
    {
        return $this->rozpoznanie;
    }

    public function setRozpoznanie(?Rozpoznanie $rozpoznanie): self
    {
        $this->rozpoznanie = $rozpoznanie;

        return $this;
    }

    public function getOpis(): ?string
    {
        return $this->opis;
    }

    public function setOpis(?string $opis): self
    {
        $this->opis = $opis;

        return $this;
    }

    /**
     * @return Collection|Turnus[]
     */
    public function getTurnusy(): Collection
    {
        return $this->turnusy;
    }

    public function addTurnusy(Turnus $turnusy): self
    {
        if (!$this->turnusy->contains($turnusy)) {
            $this->turnusy[] = $turnusy;
            $turnusy->setPlan($this);
        }

        return $this;
    }

    public function removeTurnusy(Turnus $turnusy): self
    {
        if ($this->turnusy->contains($turnusy)) {
            $this->turnusy->removeElement($turnusy);
            // set the owning side to null (unless already changed)
            if ($turnusy->getPlan() === $this) {
                $turnusy->setPlan(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PozycjaPlanu[]
     */
    public function getPozycjePlanu(): Collection
    {
        return $this->pozycjePlanu;
    }

    public function addPozycjePlanu(PozycjaPlanu $pozycjePlanu): self
    {
        if (!$this->pozycjePlanu->contains($pozycjePlanu)) {
            $this->pozycjePlanu[] = $pozycjePlanu;
            $pozycjePlanu->setPlan($this);
        }

        return $this;
    }

    public function removePozycjePlanu(PozycjaPlanu $pozycjePlanu): self
    {
        if ($this->pozycjePlanu->contains($pozycjePlanu)) {
            $this->pozycjePlanu->removeElement($pozycjePlanu);
            // set the owning side to null (unless already changed)
            if ($pozycjePlanu->getPlan() === $this) {
                $pozycjePlanu->setPlan(null);
            }
        }

        return $this;
    }
}
