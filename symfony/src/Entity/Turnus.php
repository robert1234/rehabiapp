<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TurnusRepository")
 */
class Turnus
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Pacjent", inversedBy="turnusy")
     * @ORM\JoinColumn(nullable=false)
     */
    private $pacjent;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    private $opis;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Plan", inversedBy="turnusy")
     * @ORM\JoinColumn(nullable=false)
     */
    private $plan;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Etap", mappedBy="turnus")
     */
    private $etapy;

    public function __construct()
    {
        $this->etapy = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPacjent(): ?Pacjent
    {
        return $this->pacjent;
    }

    public function setPacjent(?Pacjent $pacjent): self
    {
        $this->pacjent = $pacjent;

        return $this;
    }

    public function getOpis(): ?string
    {
        return $this->opis;
    }

    public function setOpis(?string $opis): self
    {
        $this->opis = $opis;

        return $this;
    }

    public function getPlan(): ?Plan
    {
        return $this->plan;
    }

    public function setPlan(?Plan $plan): self
    {
        $this->plan = $plan;

        return $this;
    }

    /**
     * @return Collection|Etap[]
     */
    public function getEtapy(): Collection
    {
        return $this->etapy;
    }

    public function addEtapy(Etap $etapy): self
    {
        if (!$this->etapy->contains($etapy)) {
            $this->etapy[] = $etapy;
            $etapy->setTurnus($this);
        }

        return $this;
    }

    public function removeEtapy(Etap $etapy): self
    {
        if ($this->etapy->contains($etapy)) {
            $this->etapy->removeElement($etapy);
            // set the owning side to null (unless already changed)
            if ($etapy->getTurnus() === $this) {
                $etapy->setTurnus(null);
            }
        }

        return $this;
    }
}
