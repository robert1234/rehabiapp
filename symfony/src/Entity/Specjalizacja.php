<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SpecjalizacjaRepository")
 */
class Specjalizacja
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $nazwa;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Specjalista", mappedBy="specjalizacja")
     */
    private $specjalisci;

    public function __construct()
    {
        $this->specjalisci = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNazwa(): ?string
    {
        return $this->nazwa;
    }

    public function setNazwa(string $nazwa): self
    {
        $this->nazwa = $nazwa;

        return $this;
    }

    /**
     * @return Collection|Specjalista[]
     */
    public function getSpecjalisci(): Collection
    {
        return $this->specjalisci;
    }

    public function addSpecjalisci(Specjalista $specjalisci): self
    {
        if (!$this->specjalisci->contains($specjalisci)) {
            $this->specjalisci[] = $specjalisci;
            $specjalisci->setSpecjalizacja($this);
        }

        return $this;
    }

    public function removeSpecjalisci(Specjalista $specjalisci): self
    {
        if ($this->specjalisci->contains($specjalisci)) {
            $this->specjalisci->removeElement($specjalisci);
            // set the owning side to null (unless already changed)
            if ($specjalisci->getSpecjalizacja() === $this) {
                $specjalisci->setSpecjalizacja(null);
            }
        }

        return $this;
    }
}
