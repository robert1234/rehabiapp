# GIT REPO:
> git clone https://gitlab.com/robert1234/rehabiapp.git

# PREVIEW
![Alpha 0](alpha-preview-0.png)

# APPLICATION INFO:
Design and implementation of a software application supporting the functioning the rehabilitation clinic

# COPYRIGHT
robert.radziszewski.pierwszy@gmail.com &copy; 2019-2020