INSERT INTO aplikacja(nazwa) VALUES ('rehabi_app_web'),('rehabi_app_apk');

INSERT INTO rodzaj_uprawnienia(nazwa) VALUES ('odczyt'),('zapis'),('grupa');

INSERT INTO element_aplikacji(nazwa, aplikacja_id) VALUES
    (
        'uprawnienia', (SELECT id FROM aplikacja WHERE nazwa = 'rehabi_app_web')
    ),
    (
        'osoby', (SELECT id FROM aplikacja WHERE nazwa = 'rehabi_app_web')
    ),
    (
        'pacjenci', (SELECT id FROM aplikacja WHERE nazwa = 'rehabi_app_web')
    ),
    (
        'uzytkownicy_systemu', (SELECT id FROM aplikacja WHERE nazwa = 'rehabi_app_web')
    ),
    (
        'grupy', (SELECT id FROM aplikacja WHERE nazwa = 'rehabi_app_web')
    ),
    (
        'plany', (SELECT id FROM aplikacja WHERE nazwa = 'rehabi_app_web')
    ),
    (
        'turnusy', (SELECT id FROM aplikacja WHERE nazwa = 'rehabi_app_web')
    ),
    (
        'zabiegi', (SELECT id FROM aplikacja WHERE nazwa = 'rehabi_app_web')
    ),
    (
        'etapy', (SELECT id FROM aplikacja WHERE nazwa = 'rehabi_app_web')
    ),
    (
        'rozpoznania', (SELECT id FROM aplikacja WHERE nazwa = 'rehabi_app_web')
    ),
    (
        'specjalisci', (SELECT id FROM aplikacja WHERE nazwa = 'rehabi_app_web')
    ),
    (
        'specjalizacje', (SELECT id FROM aplikacja WHERE nazwa = 'rehabi_app_web')
    ),
    (
        'sale', (SELECT id FROM aplikacja WHERE nazwa = 'rehabi_app_web')
    ),
    (
        'oddzialy', (SELECT id FROM aplikacja WHERE nazwa = 'rehabi_app_web')
    ),
    (
        'zabiegi', (SELECT id FROM aplikacja WHERE nazwa = 'rehabi_app_web')
    );

INSERT INTO status_wizyty(nazwa) VALUES ('zarejestrowano'),('nie_odbyto'),('odbyto'),('zaplanowano'),('anulowano');

INSERT INTO grupa(nazwa,opis,aplikacja_id) VALUES
    (
        'super_admin',
        'Bogowie Wszystkiego',
        (SELECT id FROM aplikacja WHERE nazwa = 'rehabi_app_web')
    );

INSERT INTO uprawnienie(status, rodzaj_uprawnienia_id, grupa_id, element_aplikacji_id)
    (
        SELECT 'przyznano', rodzaj_uprawnienia.id, grupa.id, element_aplikacji.id
        FROM rodzaj_uprawnienia CROSS JOIN grupa CROSS JOIN element_aplikacji
        WHERE grupa.nazwa = 'super_admin'
    );

INSERT INTO osoba(imie, nazwisko, pesel, data_urodzenia, plec, imie_ojca, imie_matki, ulica, nr_domu, miejscowosc, kod_pocztowy, data_rejestracji) VALUES
    ('Jan','Nowak','44051401458', '1944-05-14 00:00:00','M','Janusz','Grazyna','Glowna','1','Otwock','01-234',NOW());

INSERT INTO uzytkownik_systemu(login, haslo, aktywny, osoba_id) VALUES
    ('jan.nowak', '$2y$12$wbsaXjX7hFPiA8ENtScxPemzQBy1OG7HEOA1/bh1B1Itt6FysIj0i', true, (SELECT id FROM osoba WHERE pesel = '44051401458'));

INSERT INTO czlonek_grupy(uzytkownik_systemu_id, grupa_id) VALUES
    (
        (SELECT id FROM uzytkownik_systemu WHERE login = 'jan.nowak'),
        (SELECT id FROM grupa WHERE nazwa = 'super_admin')
    );