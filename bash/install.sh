#!/bin/bash

docker-compose exec php composer install
docker-compose exec php yarn install
docker-compose exec php yarn encore dev
chmod -R 777 ./logs
chmod -R 777 ./symfony/var